package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 16/7/2560.
 */

public class PostItemResultData implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("image")
    private String image;

    @SerializedName("postponed_status")
    private boolean postponed_status;

    @SerializedName("postponed_number")
    private int postponed_number;

    public PostItemResultData(){}

    public int getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public boolean isPostponedStatus() {
        return postponed_status;
    }

    public int getPostponedNumber() {
        return postponed_number;
    }

    protected PostItemResultData(Parcel in) {
        id = in.readInt();
        image = in.readString();
        postponed_status = in.readByte() != 0;
        postponed_number =  in.readInt();

    }

    public static final Parcelable.Creator<PostItemResultData> CREATOR = new Parcelable.Creator<PostItemResultData>() {
        @Override
        public PostItemResultData createFromParcel(Parcel in) {
            return new PostItemResultData(in);
        }

        @Override
        public PostItemResultData[] newArray(int size) {
            return new PostItemResultData[size];
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(image);
        dest.writeByte((byte) (postponed_status ? 1 : 0));
        dest.writeInt(postponed_number);
    }
}
