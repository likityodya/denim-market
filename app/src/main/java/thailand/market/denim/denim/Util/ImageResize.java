package thailand.market.denim.denim.Util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

/**
 * Created by com_s on 14-Jan-17.
 */

public class ImageResize {

    private String TAG = "ImageResize";

    public ImageResize() {

    }

    public String resize(String path) throws Exception {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = BitmapFactory.decodeFile(path, options);
        return scaleDown(bitmap, true);
    }

    private String scaleDown(Bitmap realImage, boolean filter) throws Exception {
        float ratio = Math.min(
                Constant.MAX_IMAGE_SIZE / realImage.getWidth(),
                Constant.MAX_IMAGE_SIZE / realImage.getHeight());
        int width = Math.round(ratio * realImage.getWidth());
        int height = Math.round(ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height, filter);
        return storeImage(newBitmap);
    }

    private String storeImage(Bitmap image) throws Exception {
        try {
            String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/req_images");
            myDir.deleteOnExit();
            myDir.mkdirs();
            Random generator = new Random();
            int n = 10000;
            n = generator.nextInt(n);
            String fname = "Image-" + n + ".jpg";
            File file = new File(myDir, fname);
            if (file.exists())
                file.delete();

            FileOutputStream out = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            String filePath =  file.getPath();
            return filePath;
        } catch (Exception e) {
            throw e;
        }

    }
    public static void deleteTemp(){
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/req_images");
        myDir.deleteOnExit();
    }


}
