package thailand.market.denim.denim.Util;

/**
 * Created by chawdee on 09/04/2015.
 */
public class Constant {

    public static String BASE_URL = "http://denim-markets.com/api/";
    public static String TERM_URL = "http://www.denim-markets.com/terms";

    public static String DEVICE_TYPE = "android";

    public static String JSON_DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static String EMAIL_STAFF = "denim_market@outlook.com";

    public static final int REQUEST_CODE_PICKER_PROFILE = 1000;
    public static final int REQUEST_CODE_PICKER_REGISTER_MEMBER = 1002;
    public static final int REQUEST_CODE_PICKER_SELL_IMAGE = 1003;
    public static final int REQUEST_CODE_ADVANCE_SEARCH_POST = 1004;
    public static final int REQUEST_CODE_VOTE_USER = 1005;

    public static float MAX_IMAGE_SIZE = 300;

    public static final String[] SHIRT_SIZE = new String[]{"XS", "S", "M", "L", "XL", "XXL", "34", "36", "38", "40", "42", "44", "46", "48"};

    public static final String[] PANT_WAIST = new String[]{"XS", "S", "M", "L", "XL", "XXL", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40"};
    public static final String[] PANT_LENGTH = new String[]{"28", "30", "32", "34", "36", "38", "40"};

    public static final String[] SHOES_EU = new String[]{"36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48"};
    public static final String[] SHOES_US = new String[]{"4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12", "12.5", "13", "13.5", "14", "14.5", "15"};
    public static final String[] SHOES_UK = new String[]{"3.5", "4", "4.5", "5", "5.5", "6", "6.5", "7", "7.5", "8", "8.5", "9", "9.5", "10", "10.5", "11", "11.5", "12", "12.5", "13", "13.5", "14", "14.5", "15"};

}

