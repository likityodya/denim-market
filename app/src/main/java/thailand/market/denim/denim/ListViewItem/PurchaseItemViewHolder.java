package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerPurchaseItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class PurchaseItemViewHolder extends RecyclerView.ViewHolder {
    public RecyclerPurchaseItemBinding binding;

    public PurchaseItemViewHolder(RecyclerPurchaseItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void displayImage(String imageUrl) {
        Util.loadImage(binding.getRoot().getContext(), imageUrl, binding.imageView, binding.progressBar);
    }

    public void setStatus(String status) {
        switch (status){
            case "pending":
                binding.tvStatus.setText("รอตรวจสอบ/ค้างชำระ");
                break;
            case "ready_shipping":
                binding.tvStatus.setText("รอจัดส่ง");
                break;
            case "completed":
                binding.tvStatus.setText("จัดส่งสินค้าแล้ว");
                break;
        }

    }

    public void setBrand(String brand) {
        binding.tvBrand.setText(brand);
    }

    public void setGeneration(String generation) {
        binding.tvGeneration.setText(generation);
    }

    public void setDate(String date) {
        binding.tvDate.setText(date);
    }

    public void setPrice(String price) {
        binding.tvPrice.setText(price);
    }

    public View getLayoutItem() {
        return binding.layout;
    }
}
