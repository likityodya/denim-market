package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 15/7/2560.
 */

public class ProductTypeResultData implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("brand_id")
    private int brand_id;

    @SerializedName("type_id")
    private int type_id;

    @SerializedName("size_option")
    private String size_option;

    @SerializedName("type_name")
    private String type_name;

    public int getId() {
        return id;
    }

    public int getBrandId() {
        return brand_id;
    }

    public int getTypeId() {
        return type_id;
    }

    public String getSizeOption() {
        return size_option;
    }

    public String getTypeName() {
        return type_name;
    }

    public ProductTypeResultData() {
    }

    protected ProductTypeResultData(Parcel in) {
        id = in.readInt();
        brand_id = in.readInt();
        type_id = in.readInt();
        size_option = in.readString();
        type_name = in.readString();
    }

    public static final Parcelable.Creator<ProductTypeResultData> CREATOR = new Parcelable.Creator<ProductTypeResultData>() {
        @Override
        public ProductTypeResultData createFromParcel(Parcel in) {
            return new ProductTypeResultData(in);
        }

        @Override
        public ProductTypeResultData[] newArray(int size) {
            return new ProductTypeResultData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(brand_id);
        dest.writeInt(type_id);
        dest.writeString(size_option);
        dest.writeString(type_name);
    }
}
