package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class ProductDetailResponse extends BaseResponse {

    @SerializedName("data")
    private ProductDetailResultData productDetailResultData;

    public ProductDetailResultData getProductDetailResultData() {
        return productDetailResultData;
    }
}
