package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import thailand.market.denim.denim.Adaptor.BrandRecyclerViewAdapter;
import thailand.market.denim.denim.Adaptor.NewsRecyclerViewAdapter;
import thailand.market.denim.denim.Adaptor.PromotionRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.IFragmentControlListener;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.ForumResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.ForumRepository;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentForumBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ForumFragment extends BaseFragment {


    private FragmentForumBinding binding;
    private ForumRepository forumRepository;
    private NewsRecyclerViewAdapter newsRecyclerViewAdapter;
    private PromotionRecyclerViewAdapter promotionRecyclerViewAdapter;
    private BrandRecyclerViewAdapter brandRecyclerViewAdapter;

    public ForumFragment() {
        super();
    }

    public static ForumFragment newInstance() {
        ForumFragment fragment = new ForumFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentForumBinding.inflate(inflater);

        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        forumRepository = new ForumRepository();

    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        binding.recyclerNews.setLayoutManager(linearLayoutManager);

        linearLayoutManager = new LinearLayoutManager(getContext());
        binding.recyclerPromo.setLayoutManager(linearLayoutManager);

        linearLayoutManager = new LinearLayoutManager(getContext());
        binding.recyclerBrand.setLayoutManager(linearLayoutManager);

        loadData();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    private void loadData() {
        showProgressDialog(getString(R.string.loading));
        forumRepository.getForum(forumCallback);
    }

    private HTTPCallback forumCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {

            ForumResultData forumResultData = (ForumResultData) response;

            if(forumResultData.getNews() != null){
                newsRecyclerViewAdapter = new NewsRecyclerViewAdapter(forumResultData.getNews());
                binding.recyclerNews.setAdapter(newsRecyclerViewAdapter);
            }else {
                binding.layoutNews.setVisibility(View.GONE);
            }

            if(forumResultData.getPromotions() != null){
                promotionRecyclerViewAdapter = new PromotionRecyclerViewAdapter(forumResultData.getPromotions());
                binding.recyclerPromo.setAdapter(promotionRecyclerViewAdapter);
            }else {
                binding.layoutPromotion.setVisibility(View.GONE);
            }

            if(forumResultData.getBrands() != null){
                brandRecyclerViewAdapter = new BrandRecyclerViewAdapter(forumResultData.getBrands());
                brandRecyclerViewAdapter.setOnClickListener(brandClickListener);
                binding.recyclerBrand.setAdapter(brandRecyclerViewAdapter);
            }else {
                binding.layoutBrand.setVisibility(View.GONE);
            }

            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private IRecyclerViewItemClickListener brandClickListener = new IRecyclerViewItemClickListener() {
        @Override
        public void OnItemClick(Object item) {
            BrandResultData data = (BrandResultData)item;
            IFragmentControlListener controlListener = (IFragmentControlListener) getParentFragment();
            controlListener.onGoNextFragment(ProductBrandFragment.newInstance(data));
        }

        @Override
        public void OnItemLongClick(View view, Object item) {

        }
    };

}
