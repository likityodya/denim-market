package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.FavoritePostItemViewHolder;
import thailand.market.denim.denim.ListViewItem.HistoryPostItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.FavoritePostData;
import thailand.market.denim.denim.Model.HTTP.HistoryPostData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerFavoritePostItemBinding;
import thailand.market.denim.denim.databinding.RecyclerHistoryPostItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class HistoryRecyclerViewAdapter extends RecyclerView.Adapter<HistoryPostItemViewHolder> {

    private List<HistoryPostData> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener itemClickListener;

    public HistoryRecyclerViewAdapter(List<HistoryPostData> items) {
        this.items = items;
    }


    @Override
    public HistoryPostItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerHistoryPostItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_history_post_item, parent, false);
        return new HistoryPostItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(HistoryPostItemViewHolder postItemViewHolder, final int position) {
        final HistoryPostData data = items.get(position);
        postItemViewHolder.displayImage(data.getImages());
        postItemViewHolder.setBrand(data.getBrand());
        postItemViewHolder.setGeneration(data.getGeneration());
        postItemViewHolder.setPrice(data.getPrice());
        postItemViewHolder.setMemberType(data.getUserType());
        postItemViewHolder.setDistrict(data.getDistrict().getNameTh());
        postItemViewHolder.setProvince(data.getProvince().getNameTh());
        postItemViewHolder.setProductType(data.getProductType());
        postItemViewHolder.setSize(data.getSize());
        postItemViewHolder.setType(data.getType());
        postItemViewHolder.getLayoutItem().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.OnItemClick(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItemClickListener(IRecyclerViewItemClickListener onClickListener) {
        this.itemClickListener = onClickListener;
    }
}