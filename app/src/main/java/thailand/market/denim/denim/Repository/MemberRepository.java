package thailand.market.denim.denim.Repository;

import java.io.File;
import java.util.HashMap;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.HTTP.MemberService;
import thailand.market.denim.denim.Manager.HttpManager;
import thailand.market.denim.denim.Model.DTO.ForgotPasswordDto;
import thailand.market.denim.denim.Model.DTO.RegisterDto;
import thailand.market.denim.denim.Model.HTTP.BaseResponse;
import thailand.market.denim.denim.Model.HTTP.LoginResponse;
import thailand.market.denim.denim.Model.HTTP.LookForResponse;
import thailand.market.denim.denim.Model.HTTP.MyOrderListResponse;
import thailand.market.denim.denim.Model.HTTP.OrderDetailResponse;
import thailand.market.denim.denim.Model.HTTP.ProfileResponse;
import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.Model.HTTP.RegisterResponse;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class MemberRepository {
    MemberService memberService;

    public MemberRepository() {
        memberService = HttpManager.getInstance().getMemberService();
    }

    public void register(RegisterDto registerDto, final HTTPCallback httpCallback) {
        Flowable<Response<RegisterResponse>> register = memberService.register(
                registerDto.getName(),
                registerDto.getSurname(),
                registerDto.getEmail(),
                registerDto.getPassword(),
                registerDto.getConfirmPassword(),
                Constant.DEVICE_TYPE
        );
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<RegisterResponse>>() {
                    @Override
                    public void onNext(Response<RegisterResponse> response) {
                        if (response.isSuccessful()) {
                            RegisterResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void login(String email, String password, String fcmToken, final HTTPCallback httpCallback) {
        Flowable<Response<LoginResponse>> login = memberService.login(email, password, fcmToken, Constant.DEVICE_TYPE);
        login.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<LoginResponse>>() {
                    @Override
                    public void onNext(Response<LoginResponse> response) {
                        if (response.isSuccessful()) {
                            LoginResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getLoginResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getProfile(int userID, final HTTPCallback httpCallback) {
        Flowable<Response<ProfileResponse>> profile = memberService.getProfile(userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<ProfileResponse>>() {
                    @Override
                    public void onNext(Response<ProfileResponse> response) {
                        if (response.isSuccessful()) {
                            ProfileResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getProfileResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void updateProfile(ProfileResultData profileResultData, File file, final HTTPCallback httpCallback) {

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getName());
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getPhone());
        RequestBody facebook = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getFacebook());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getEmail());
        RequestBody first_name = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getFirstName());
        RequestBody last_name = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getLastName());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getAddress());
        RequestBody province_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(profileResultData.getProvince().getId()));
        RequestBody district_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(profileResultData.getDistrict().getId()));
        RequestBody line_id = RequestBody.create(MediaType.parse("text/plain"), profileResultData.getLineId());
        MultipartBody.Part body = null;
        if (file != null) {
            RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
            body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        }

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("name", name);
        map.put("phone", phone);
        map.put("facebook", facebook);
        map.put("email", email);
        map.put("first_name", first_name);
        map.put("last_name", last_name);
        map.put("address", address);
        map.put("province_id", province_id);
        map.put("district_id", district_id);
        map.put("line_id", line_id);

        Flowable<Response<BaseResponse>> profile = memberService.updateProfile(
                profileResultData.getId(),
                map,
                body
        );
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(null);
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void forgotPassword(String email, final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> profile = memberService.forgetPassword(email);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void changePassword(ForgotPasswordDto forgotPasswordDto, final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> profile = memberService.changePassword(
                forgotPasswordDto.getUserID(),
                forgotPasswordDto.getPassword(),
                forgotPasswordDto.getNewPassword(),
                forgotPasswordDto.getConfirmPassword()
        );
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void registerVerify(int userID, File file, final HTTPCallback httpCallback) {

        RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestFile);

        Flowable<Response<BaseResponse>> profile = memberService.registerVerify(
                userID,
                body
        );
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void facebookLogin(String token, String fcmToken, final HTTPCallback httpCallback) {

        Flowable<Response<LoginResponse>> profile = memberService.facebookLogin(token, fcmToken, Constant.DEVICE_TYPE);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<LoginResponse>>() {
                    @Override
                    public void onNext(Response<LoginResponse> response) {
                        if (response.isSuccessful()) {
                            LoginResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getLoginResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void logout(int userID, final HTTPCallback httpCallback) {

        Flowable<Response<BaseResponse>> profile = memberService.logout(userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void voteUser(int fromUserID, int toUserID, int score, final HTTPCallback httpCallback) {

        Flowable<Response<BaseResponse>> profile = memberService.vote(fromUserID, toUserID, score);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getMyOrder(int userID, final HTTPCallback httpCallback) {

        Flowable<Response<MyOrderListResponse>> profile = memberService.getMyOrder(userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<MyOrderListResponse>>() {
                    @Override
                    public void onNext(Response<MyOrderListResponse> response) {
                        if (response.isSuccessful()) {
                            MyOrderListResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getOrderListData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void lookForItem(int userId,
                            int brandId,
                            int typeId,
                            String productType,
                            int minPrice,
                            int maxPrice,
                            int provinceId,
                            int districtId,
                            String shirts_size_1,
                            String pants_size_1,
                            String pants_size_2,
                            String shoes_size_eu,
                            String shoes_size_us,
                            String shoes_size_uk,
                            final HTTPCallback httpCallback) {

        Flowable<Response<BaseResponse>> flowable = memberService.lookFor(
                userId,
                brandId,
                typeId,
                productType,
                minPrice,
                maxPrice,
                provinceId,
                districtId,
                shirts_size_1,
                pants_size_1,
                pants_size_2,
                shoes_size_eu,
                shoes_size_us,
                shoes_size_uk
        );
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void myLookFor(int userId, final HTTPCallback httpCallback) {

        Flowable<Response<LookForResponse>> flowable = memberService.myLookFor(userId);
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<LookForResponse>>() {
                    @Override
                    public void onNext(Response<LookForResponse> response) {
                        if (response.isSuccessful()) {
                            LookForResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getLookForData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteLookFor(int id, int userId, final HTTPCallback httpCallback) {

        Flowable<Response<BaseResponse>> flowable = memberService.deleteLookFor(id, userId);
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deleteOrder(int id, int userId, final HTTPCallback httpCallback) {

        Flowable<Response<BaseResponse>> flowable = memberService.deleteOrder(id, userId);
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getOrderDetail(int id, final HTTPCallback httpCallback) {

        Flowable<Response<OrderDetailResponse>> flowable = memberService.getOrderDetail(id);
        flowable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<OrderDetailResponse>>() {
                    @Override
                    public void onNext(Response<OrderDetailResponse> response) {
                        if (response.isSuccessful()) {
                            OrderDetailResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getOrderDetailResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
