package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class PostListResultData extends BaseResponse {

    @SerializedName("online")
    private List<PostItemResultData> onlones;

    @SerializedName("pending")
    private List<PostItemResultData> pendings;

    @SerializedName("close")
    private List<PostItemResultData> closes;

    public List<PostItemResultData> getOnlones() {
        if(onlones == null){
            return new ArrayList<>();
        }
        return onlones;
    }

    public List<PostItemResultData> getPendings() {
        if(pendings == null){
            return new ArrayList<>();
        }
        return pendings;
    }

    public List<PostItemResultData> getCloses() {
        if(closes == null){
            return new ArrayList<>();
        }
        return closes;
    }


}
