package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 3/7/2560.
 */

public class ProfileResultData implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("user_type")
    private String user_type;

    @SerializedName("type")
    private String type;

    @SerializedName("rating")
    private String rating;

    @SerializedName("phone")
    private String phone;

    @SerializedName("facebook")
    private String facebook;

    @SerializedName("email")
    private String email;

    @SerializedName("line_id")
    private String line_id;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("nationality_id")
    private String nationality_id;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("address")
    private String address;

    @SerializedName("district")
    private DistrictData district;

    @SerializedName("province")
    private ProvinceResultData province;

    @SerializedName("image")
    private String image;

//    private int provinceId;
//    private int districtId;

    public ProfileResultData() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUserType() {
        return user_type;
    }

    public void setUserType(String user_type) {
        this.user_type = user_type;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCreatedAt() {
        return created_at;
    }

    public void setCreatedAt(String created_at) {
        this.created_at = created_at;
    }

    public String getNationalityId() {
        return nationality_id;
    }

    public void setNationalityId(String nationality_id) {
        this.nationality_id = nationality_id;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String first_name) {
        this.first_name = first_name;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String last_name) {
        this.last_name = last_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public DistrictData getDistrict() {
        return district;
    }

    public void setDistrict(DistrictData district) {
        this.district = district;
    }

    public ProvinceResultData getProvince() {
        return province;
    }

    public void setProvince(ProvinceResultData province) {
        this.province = province;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getLineId() {
        return line_id;
    }

    public void setLineId(String line_id) {
        this.line_id = line_id;
    }

    public String getType() {
        return type;
    }

    //    public int getProvinceId() {
//        return provinceId;
//    }
//
//    public void setProvinceId(int provinceId) {
//        this.provinceId = provinceId;
//    }
//
//    public int getDistrictId() {
//        return districtId;
//    }
//
//    public void setDistrictId(int districtId) {
//        this.districtId = districtId;
//    }

    protected ProfileResultData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        user_type = in.readString();
        rating = in.readString();
        phone = in.readString();
        facebook = in.readString();
        email = in.readString();
        line_id = in.readString();
        created_at = in.readString();
        nationality_id = in.readString();
        first_name = in.readString();
        last_name = in.readString();
        address = in.readString();
        district = in.readParcelable(DistrictData.class.getClassLoader());
        province = in.readParcelable(ProvinceResultData.class.getClassLoader());
        image = in.readString();
    }

    public static final Parcelable.Creator<ProfileResultData> CREATOR = new Parcelable.Creator<ProfileResultData>() {
        @Override
        public ProfileResultData createFromParcel(Parcel in) {
            return new ProfileResultData(in);
        }

        @Override
        public ProfileResultData[] newArray(int size) {
            return new ProfileResultData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(user_type);
        dest.writeString(rating);
        dest.writeString(phone);
        dest.writeString(facebook);
        dest.writeString(email);
        dest.writeString(line_id);
        dest.writeString(created_at);
        dest.writeString(nationality_id);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(address);
        dest.writeParcelable(district, flags);
        dest.writeParcelable(province, flags);
        dest.writeString(image);
    }
}
