package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 30/5/2560.
 */

public class HomeResultData {
    @SerializedName("post_new_total")
    private int postNewTotal;

    @SerializedName("post_all_total")
    private int postAllTotal;

    @SerializedName("user_online")
    private int userOnline;

    @SerializedName("user_login_today_total")
    private int userLoginTodayTotal;

    @SerializedName("user_total")
    private int userTotal;

    @SerializedName("slider")
    private String[] slider;

    public int getPostNewTotal() {
        return postNewTotal;
    }

    public int getPostAllTotal() {
        return postAllTotal;
    }

    public int getUserOnline() {
        return userOnline;
    }

    public int getUserLoginTodayTotal() {
        return userLoginTodayTotal;
    }

    public int getUserTotal() {
        return userTotal;
    }

    public String[] getSlider() {
        return slider;
    }
}
