package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Adaptor.FavoritePostRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.FavoritePostData;
import thailand.market.denim.denim.Model.HTTP.HistoryPostData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Repository.PostRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentFavouriteBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class FavoriteFragment extends BaseFragment {

    private FragmentFavouriteBinding binding;
    private PostRepository postRepository;

    public FavoriteFragment() {
        super();
    }

    public static FavoriteFragment newInstance() {
        FavoriteFragment fragment = new FavoriteFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentFavouriteBinding.inflate(inflater);
        View rootView = binding.getRoot();
        super.init(rootView, inflater);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        postRepository = new PostRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        loadList();
    }

    private void loadList() {
        showProgress();
        postRepository.getFavorite(PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                List<FavoritePostData> dataList = (List<FavoritePostData>) response;
                if (dataList != null) {
                    FavoritePostRecyclerViewAdapter adapter = new FavoritePostRecyclerViewAdapter(dataList);
                    adapter.setItemClickListener(new IRecyclerViewItemClickListener() {
                        @Override
                        public void OnItemClick(Object item) {
                            FavoritePostData data = (FavoritePostData) item;
                            IFragmentListener listener = (IFragmentListener) getActivity();
                            listener.openPostItem(data.getPostID());
                        }

                        @Override
                        public void OnItemLongClick(View view, final Object item) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext())
                                    .setMessage("ลบออกจากรายการโปรด")
                                    .setCancelable(true)
                                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int whichButton) {

                                            FavoritePostData data = (FavoritePostData) item;
                                            postRepository.removeFavorite(data.getId(), new HTTPCallback() {
                                                @Override
                                                public void OnSuccess(Object response) {
                                                    loadList();
                                                }

                                                @Override
                                                public void OnError(String message) {
                                                    Util.showToast(message);
                                                }
                                            });
                                        }
                                    });
                            alertDialog.show();
                        }
                    });
                    binding.recyclerView.setAdapter(adapter);
                }
                hideProgress();
            }

            @Override
            public void OnError(String message) {
                hideProgress();
                Util.showToast(message);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }


}
