package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Adaptor.PostRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Model.HTTP.PostItemResultData;
import thailand.market.denim.denim.databinding.FragmentPostBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PostPendingFragment extends BaseFragment {

    private FragmentPostBinding binding;
    private List<PostItemResultData> posts;
    private PostRecyclerViewAdapter adapter;
    private MyNoticeFragment noticeFragment;
    private RecyclerView recyclerView;

    public PostPendingFragment() {
        super();
    }

    private void setData(List<PostItemResultData> posts){
        this.posts = posts;
    }

    public static PostPendingFragment newInstance(List<PostItemResultData> posts) {
        PostPendingFragment fragment = new PostPendingFragment();
        fragment.setData(posts);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPostBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        noticeFragment = (MyNoticeFragment) getParentFragment();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView = binding.recyclerView;
        setPosts(posts);
    }

    public void setPosts(List<PostItemResultData> posts) {
        this.posts = posts;
        adapter = new PostRecyclerViewAdapter(posts);
        initRecyclerViewEvent();
        if (recyclerView != null) {
            recyclerView.setAdapter(adapter);
        }
    }

    private void initRecyclerViewEvent() {
        adapter.setMarkedSellClickListener(new IRecyclerViewItemClickListener() {
            @Override
            public void OnItemClick(Object item) {
                noticeFragment.markedSell((PostItemResultData) item);
            }

            @Override
            public void OnItemLongClick(View view, Object item) {

            }
        });
        adapter.setEditClickListener(new IRecyclerViewItemClickListener() {
            @Override
            public void OnItemClick(Object item) {
                noticeFragment.editPost((PostItemResultData)item);
            }

            @Override
            public void OnItemLongClick(View view, Object item) {

            }
        });
        adapter.setDeleteClickListener(new IRecyclerViewItemClickListener() {
            @Override
            public void OnItemClick(Object item) {
                noticeFragment.deletePost((PostItemResultData) item);
            }

            @Override
            public void OnItemLongClick(View view, Object item) {

            }
        });
        adapter.setRenewClickListener(new IRecyclerViewItemClickListener() {
            @Override
            public void OnItemClick(Object item) {
                noticeFragment.renewPost((PostItemResultData) item);
            }

            @Override
            public void OnItemLongClick(View view, Object item) {

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }


}
