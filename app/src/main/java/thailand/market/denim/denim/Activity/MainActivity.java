package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.widget.TextView;

import com.esafirm.imagepicker.model.Image;

import java.util.ArrayList;

import thailand.market.denim.denim.CustomImagePicker.CustomImagePicker;
import thailand.market.denim.denim.Fragment.ChatFragment;
import thailand.market.denim.denim.Fragment.ForumFragment;
import thailand.market.denim.denim.Fragment.HomeFragment;
import thailand.market.denim.denim.Fragment.ManageProfileFragment;
import thailand.market.denim.denim.Fragment.MyNoticeFragment;
import thailand.market.denim.denim.Fragment.NeedBuyListFragment;
import thailand.market.denim.denim.Fragment.NoLoginFragment;
import thailand.market.denim.denim.Fragment.ProfileFragment;
import thailand.market.denim.denim.Fragment.RegisterMemberFragment;
import thailand.market.denim.denim.Fragment.UploadImageFragment;
import thailand.market.denim.denim.Fragment.WrapperFragment;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.DistrictData;
import thailand.market.denim.denim.Model.HTTP.ProductTypeResultData;
import thailand.market.denim.denim.Model.HTTP.ProvinceResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivityMainBinding;

public class MainActivity
        extends BaseActivity
        implements IFragmentListener {

    private ActivityMainBinding binding;
    private boolean doubleBackToExitPressedOnce;
    private int currentTabPosition = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        Util.verifyStoragePermissions(this);
        initInstance();
    }

    private void initInstance() {

        TabLayout.Tab homeTab = binding.tabs.newTab();
        homeTab.setCustomView(R.layout.tab_home);
        binding.tabs.addTab(homeTab, true);

        TabLayout.Tab noticeTab = binding.tabs.newTab();
        noticeTab.setCustomView(R.layout.tab_my_notice);
        binding.tabs.addTab(noticeTab);

        TabLayout.Tab chatTab = binding.tabs.newTab();
        chatTab.setCustomView(R.layout.tab_chat);
        binding.tabs.addTab(chatTab);

        TabLayout.Tab forumTab = binding.tabs.newTab();
        forumTab.setCustomView(R.layout.tab_forum);
        binding.tabs.addTab(forumTab);

        TabLayout.Tab profileTab = binding.tabs.newTab();
        profileTab.setCustomView(R.layout.tab_profile);
        binding.tabs.addTab(profileTab);

        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_layout, WrapperFragment.newInstance(HomeFragment.newInstance()));
        ft.commit();

        binding.tabs.addOnTabSelectedListener(onTabSelectedListener);

    }


    private TabLayout.OnTabSelectedListener onTabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            Fragment fragment = null;
            switch (tab.getPosition()) {
                case 0:
                    fragment = WrapperFragment.newInstance(HomeFragment.newInstance());
                    break;
                case 1:
                    if (isLogIn()) {
                        fragment = WrapperFragment.newInstance(MyNoticeFragment.newInstance());
                    } else {
                        fragment = NoLoginFragment.newInstance();
                    }
                    break;
                case 2:
                    fragment = ChatFragment.newInstance();
                    break;
                case 3:
                    if (isLogIn()) {
                        fragment = WrapperFragment.newInstance(ForumFragment.newInstance());
                    } else {
                        fragment = NoLoginFragment.newInstance();
                    }

                    break;
                case 4:
                    if (isLogIn()) {
                        fragment = WrapperFragment.newInstance(ProfileFragment.newInstance());
                    } else {
                        fragment = NoLoginFragment.newInstance();
                    }
                    break;
            }

            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            if (currentTabPosition < tab.getPosition()) {
                ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            } else {
                ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
            }

            ft.replace(R.id.fragment_layout, fragment);
            ft.commit();

            TextView tabText = (TextView) tab.getCustomView().findViewById(R.id.tabText);
            tabText.setTextColor(ContextCompat.getColor(MainActivity.this, android.R.color.white));

            TextView tabIcon = (TextView) tab.getCustomView().findViewById(R.id.tabIcon);
            tabIcon.setTextColor(ContextCompat.getColor(MainActivity.this, android.R.color.white));

            currentTabPosition = tab.getPosition();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
            TextView tabText = (TextView) tab.getCustomView().findViewById(R.id.tabText);
            tabText.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.dark_gray));

            TextView tabIcon = (TextView) tab.getCustomView().findViewById(R.id.tabIcon);
            tabIcon.setTextColor(ContextCompat.getColor(MainActivity.this, R.color.dark_gray));

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    private void goLoginPage() {
        Intent intent = new Intent(this, SelectLoginActivity.class);
        startActivityForResult(intent, SelectLoginActivity.REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            WrapperFragment wrapperFragment;
            Fragment fragment;
            ArrayList<Image> images;
            switch (requestCode) {
                case SelectLoginActivity.REQUEST_CODE:
                    reload();
                    break;
                case Constant.REQUEST_CODE_PICKER_PROFILE:
                    images = (ArrayList<Image>) CustomImagePicker.getImages(data);
                    wrapperFragment = (WrapperFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
                    fragment = wrapperFragment.getCurrentFragment();
                    if (fragment instanceof ManageProfileFragment) {
                        ((ManageProfileFragment) fragment).setSelectedImage(images.get(0));
                    }
                    break;
                case Constant.REQUEST_CODE_PICKER_REGISTER_MEMBER:
                    images = (ArrayList<Image>) CustomImagePicker.getImages(data);
                    wrapperFragment = (WrapperFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
                    fragment = wrapperFragment.getCurrentFragment();
                    if (fragment instanceof RegisterMemberFragment) {
                        ((RegisterMemberFragment) fragment).setSelectedImage(images.get(0));
                    }
                    break;
                case Constant.REQUEST_CODE_PICKER_SELL_IMAGE:
                    images = (ArrayList<Image>) CustomImagePicker.getImages(data);
                    wrapperFragment = (WrapperFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
                    fragment = wrapperFragment.getCurrentFragment();
                    if (fragment instanceof UploadImageFragment) {
                        ((UploadImageFragment) fragment).addImages(images);
                    }
                    break;
                case Constant.REQUEST_CODE_ADVANCE_SEARCH_POST:

                    wrapperFragment = (WrapperFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
                    fragment = wrapperFragment.getCurrentFragment();
                    if (fragment instanceof NeedBuyListFragment) {
                        BrandResultData brand = data.getParcelableExtra("Brand");
                        ProductTypeResultData type = data.getParcelableExtra("Type");
                        ProvinceResultData province = data.getParcelableExtra("Province");
                        DistrictData district = data.getParcelableExtra("District");
                        int max = data.getIntExtra("MaxPrice", 0);
                        int min = data.getIntExtra("MinPrice", 0);
                        ((NeedBuyListFragment) fragment).advanceSearch(brand, type, province, district, max, min);
                    }
                    break;
            }
        }
    }

    private void reload() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onLoginRequire() {
        goLoginPage();
    }

    @Override
    public void onBackPressed() {

        WrapperFragment wrapperFragment = (WrapperFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_layout);
        boolean onGoBackFragment = wrapperFragment.onGoBackFragment();
        if (!onGoBackFragment) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            doubleBackToExitPressedOnce = true;
            Util.showToast("กดอีกครั้งเพื่ออก");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        }

    }

    @Override
    public void onLogout() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this)
                .setMessage("ยืนยันการออกจากระบบ")
                .setCancelable(true)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        showProgressDialog(getString(R.string.loading));
                        MemberRepository memberRepository = new MemberRepository();
                        memberRepository.logout(PreferenceManager.getInstance().getUserID(), logoutCallback);
                    }
                });
        alertDialog.show();
    }

    private HTTPCallback logoutCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            PreferenceManager.getInstance().setToken(null);
            PreferenceManager.getInstance().setUserID(0);
            hideProgressDialog();
            Intent intent = new Intent(MainActivity.this, SplashActivity.class);
            startActivity(intent);
            finish();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    @Override
    public void onStartEditSellItem(int postID) {
        Intent intent = new Intent(this, EditSellItemActivity.class);
        intent.putExtra("PostID", postID);
        startActivity(intent);
    }

    @Override
    public void openSelectImage(int requestCode) {
        CustomImagePicker customImagePicker = CustomImagePicker.create(this)
                .returnAfterFirst(true)
                .folderMode(true)
                .folderTitle("Photo")
                .showCamera(true);
        switch (requestCode) {
            case Constant.REQUEST_CODE_PICKER_SELL_IMAGE:
                customImagePicker.multi();
                break;
            default:
                customImagePicker.single();
                break;
        }
        customImagePicker.start(requestCode);
    }

    @Override
    public void openAdvanceSearchPost() {
        Intent intent = new Intent(MainActivity.this, SearchPostActivity.class);
        startActivityForResult(intent, Constant.REQUEST_CODE_ADVANCE_SEARCH_POST);
    }

    @Override
    public void openPostItem(int postID) {
        Intent intent = new Intent(MainActivity.this, ViewSellItemActivity.class);
        intent.putExtra("PostID", postID);
        startActivity(intent);
    }

    @Override
    public void openTerm() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.TERM_URL));
        startActivity(browserIntent);
    }

    @Override
    public void openContactStaff() {
        try {
            String email = Constant.EMAIL_STAFF;
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("mailto:" + email));
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            Util.showToast("ไม่พบ Email App.");
        }

    }
}
