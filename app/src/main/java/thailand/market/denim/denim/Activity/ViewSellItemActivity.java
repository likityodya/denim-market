package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.PostDetailData;
import thailand.market.denim.denim.Model.HTTP.PostDetailResultData;
import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.PostRepository;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivityViewSellItemBinding;

public class ViewSellItemActivity extends BaseActivity {

    private ActivityViewSellItemBinding binding;
    private PostRepository postRepository;
    private PostDetailResultData resultData;
    private ProfileResultData profile;
    private PostDetailData detail;
    private int postID;
    private String[] images;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_sell_item);

        postID = getIntent().getIntExtra("PostID", 0);
        postRepository = new PostRepository();
        showProgressDialog(getString(R.string.loading));
        postRepository.getPostDetail(postID, PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                resultData = (PostDetailResultData) response;
                initData();
                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        binding.tvCheck.setPaintFlags(binding.tvCheck.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        binding.vFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postRepository.addFavorite(PreferenceManager.getInstance().getUserID(), postID, new HTTPCallback() {
                    @Override
                    public void OnSuccess(Object response) {
                        Util.showToast(response.toString());
                        hideProgressDialog();
                    }

                    @Override
                    public void OnError(String message) {
                        Util.showToast(message);
                        hideProgressDialog();
                    }
                });
            }
        });

        binding.postID.setText(binding.postID.getText() + String.valueOf(postID));

    }

    private void initData() {
        profile = resultData.getProfile();
        detail = resultData.getPost();

        if (PreferenceManager.getInstance().getUserID() != profile.getId()) {
            binding.userRating.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ViewSellItemActivity.this, RatingUserActivity.class);
                    intent.putExtra("UserID", profile.getId());
                    intent.putExtra("Rating", profile.getRating());
                    startActivityForResult(intent, Constant.REQUEST_CODE_VOTE_USER);
                }
            });
        }


        Util.loadImage(this, profile.getImage(), binding.profileImage, binding.imageProgress);
        binding.tvFullName.setText(profile.getName());
        binding.tvProvince.setText(profile.getProvince().getNameTh());
        binding.tvDistrict.setText(profile.getDistrict().getNameTh());
        binding.tvPhone.setText(profile.getPhone());
        binding.userRating.setRating(Integer.parseInt(profile.getRating()));
        if (detail.getPostDate() != null) {
            binding.tvDate.setText(detail.getPostDate());
        }

        if (detail.getProductType().toUpperCase().equals("NEW")) {
            binding.tvProductType.setText("สินค้าใหม่");
        } else {
            binding.tvProductType.setText("สินค้ามือสอง");
        }
        List<String> tempImage = new ArrayList<>();
        for (String img : detail.getImages()) {
            tempImage.add(detail.getImagePath() + img);
        }
        images = new String[tempImage.size()];
        images = tempImage.toArray(images);
        binding.carouselView.setImageListener(imageListener);
        binding.carouselView.setPageCount(images.length);

        Util.displayMemberType(binding.tvMemberType, profile.getType(), this);

        binding.tvPrice.setText(detail.getPrice());
        binding.tvDesc.setText(binding.tvDesc.getText() + detail.getDescription());

        binding.tvBrand.setText(detail.getBrand());
        binding.tvGeneration.setText(detail.getGeneration());
        binding.tvSize.setText(detail.getSize());
    }

    private ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Util.loadImage(ViewSellItemActivity.this, images[position], imageView);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.REQUEST_CODE_VOTE_USER && resultCode == Activity.RESULT_OK) {
            int ratting = data.getIntExtra("Rating", 0);
            binding.userRating.setRating(ratting);
        }
    }
}
