package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResultData;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerProductItemBinding;
import thailand.market.denim.denim.databinding.RecyclerSearchPostItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class SearchPostItemViewHolder extends RecyclerView.ViewHolder {
    public RecyclerSearchPostItemBinding binding;

    public SearchPostItemViewHolder(RecyclerSearchPostItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void displayImage(String imageUrl) {
        Util.loadImage(binding.getRoot().getContext(), imageUrl, binding.imageView, binding.progressBar);
    }

    public void setProductType(String productType) {
        if (productType.equals("new")) {
            binding.tvProductType.setText("สินค้าใหม่");
        } else {
            binding.tvProductType.setText("สินค้ามือสอง");
        }
    }

    public void setBrand(String brand) {
        binding.tvBrand.setText(brand);
    }

    public void setGeneration(String generation) {
        binding.tvGeneration.setText(generation);
    }

    public void setSize(String size) {
        binding.tvSize.setText(size);
    }

    public void setType(String type) {
        binding.tvType.setText(type);
    }

    public void setPrice(String price) {
        binding.tvPrice.setText(price);
    }

    public void setDistrict(String district) {
        binding.tvDistrict.setText(district);
    }

    public void setProvince(String province) {
        binding.tvProvince.setText(province);
    }

    public void setMemberType(String memberType) {
        Util.displayMemberType(binding.tvMemberType, memberType, binding.getRoot().getContext());
    }

    public void isTopAd(boolean topAd) {
        binding.tvTopAd.setVisibility(topAd ? View.VISIBLE : View.GONE);
    }

    public View getLayoutItem() {
        return binding.layout;
    }
}
