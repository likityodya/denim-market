package thailand.market.denim.denim.Activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Util.DialogUtil;

/**
 * Created by Phitakphong on 1/6/2560.
 */

public class BaseActivity extends AppCompatActivity {
    private DialogUtil dialogUtil;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);
        dialogUtil = new DialogUtil(this);
    }

    public void showProgressDialog(String message){
        dialogUtil.showProgressDialog(message);
    }
    public void hideProgressDialog(){
        dialogUtil.hideProgressDialog();
    }


    public boolean isLogIn() {
        return PreferenceManager.getInstance().getUserID() != 0;
    }
}
