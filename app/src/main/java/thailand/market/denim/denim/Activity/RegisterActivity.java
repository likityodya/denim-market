package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;

import thailand.market.denim.denim.Model.DTO.RegisterDto;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivityRegisterBinding;

public class RegisterActivity extends BaseActivity {

    private ActivityRegisterBinding binding;
    private MemberRepository memberRepository;
    public static final int REQUEST_CODE = 102;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);

        initInstances();
    }

    private void initInstances() {

        memberRepository = new MemberRepository();

        this.setSupportActionBar(binding.toolbar);
        this.getSupportActionBar().setHomeButtonEnabled(true);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        binding.toolbar.setNavigationOnClickListener(onClickListener);
        binding.toolbar.setTitle(getString(R.string.register));
        binding.toolbar.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        binding.tvTerm.setPaintFlags(binding.tvTerm.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.btnRegister.setOnClickListener(onClickListener);
    }

    private void register() {
        if (validate()) {
            showProgressDialog(getString(R.string.registering));
            RegisterDto dto = new RegisterDto();
            dto.setName(binding.etName.getText().toString().trim());
            dto.setSurname(binding.etSurname.getText().toString().trim());
            dto.setEmail(binding.etEmail.getText().toString().trim());
            dto.setPassword(binding.etPassword.getText().toString().trim());
            dto.setConfirmPassword(binding.etConfirmPassword.getText().toString().trim());
            memberRepository.register(dto, registerCallback);
        }
    }

    private HTTPCallback registerCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            hideProgressDialog();
            AlertDialog.Builder dlgAlert = new AlertDialog.Builder(RegisterActivity.this);
            dlgAlert.setMessage(response.toString());
            dlgAlert.setTitle(R.string.register_result);
            dlgAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            });
            dlgAlert.setCancelable(false);
            dlgAlert.create().show();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private boolean validate() {
        boolean valid = true;
        if (binding.etName.getText().toString().trim().length() == 0) {
            binding.etName.setError(getString(R.string.name));
            valid = false;
        } else {
            binding.etName.setError(null);
        }
        if (binding.etSurname.getText().toString().trim().length() == 0) {
            binding.etSurname.setError(getString(R.string.surname));
            valid = false;
        } else {
            binding.etSurname.setError(null);
        }
        if (binding.etEmail.getText().toString().trim().length() == 0) {
            binding.etEmail.setError(getString(R.string.email));
        } else {
            valid = Util.isValidEmail(binding.etEmail.getText().toString());
            if (!valid) {
                binding.etEmail.setError(getString(R.string.invalid_email));
            } else {
                binding.etEmail.setError(null);
            }
        }
        if (binding.etPassword.getText().toString().trim().length() == 0) {
            binding.etPassword.setError(getString(R.string.password));
            valid = false;
        } else {
            binding.etPassword.setError(null);
        }
        if (binding.etConfirmPassword.getText().toString().trim().length() == 0) {
            binding.etConfirmPassword.setError(getString(R.string.confirm_pass));
            valid = false;
        } else {
            binding.etConfirmPassword.setError(null);
        }
        if (valid) {
            valid = binding.etPassword.getText().toString().equals(binding.etConfirmPassword.getText().toString());
            if (!valid) {
                binding.etPassword.setError(getString(R.string.password_not_match));
                binding.etConfirmPassword.setError(getString(R.string.password_not_match));
            } else {
                binding.etPassword.setError(null);
                binding.etConfirmPassword.setError(null);
            }
        }

        return valid;
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnRegister:
                    register();
                    break;
                default:
                    onBackPressed();
                    break;
            }
        }
    };
}
