package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.ImageView;

import com.esafirm.imagepicker.model.Image;
import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

import thailand.market.denim.denim.Adaptor.ImageRecyclerViewAdapter2;
import thailand.market.denim.denim.CustomImagePicker.CustomImagePicker;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.ImageItemViewHolder2;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.PostDetailData;
import thailand.market.denim.denim.Model.HTTP.PostDetailResultData;
import thailand.market.denim.denim.Model.HTTP.ProductTypeResultData;
import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.CommonRepository;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Repository.PostRepository;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivityEditSellItemBinding;

public class EditSellItemActivity extends BaseActivity {

    private ActivityEditSellItemBinding binding;
    private PostRepository postRepository;
    private PostDetailResultData resultData;
    private CommonRepository commonRepository;
    private String[] images;
    private List<BrandResultData> brands;
    private BrandResultData selectedBrand;

    private String selectedShirts = null;

    private String selectedShoesEU = null;
    private String selectedShoesUK = null;
    private String selectedShoesUS = null;

    private String selectedPants1 = null;
    private String selectedPants2 = null;

    private List<ProductTypeResultData> productTypes;
    private ProductTypeResultData selectedProductType;
    ProfileResultData profile;
    PostDetailData detail;
    int postID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_sell_item);
        postID = getIntent().getIntExtra("PostID", 0);

        postRepository = new PostRepository();
        commonRepository = new CommonRepository();

        binding.recyclerView.setLayoutManager(new GridLayoutManager(this, 5));

        loadMaster();
        initEvents();
    }

    private void loadMaster() {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getBands(brandHttpCallback);
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        binding.selectBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("แบรนด์");

                String[] items = new String[brands.size()];
                for (int i = 0; i < brands.size(); i++) {
                    items[i] = brands.get(i).getName();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedBrand = brands.get(which);
                        binding.selectBrand.setTextValue("แบรนด์");
                        binding.selectBrand.setTextValue(selectedBrand.getName());
                        clearType();
                        loadType(selectedBrand.getId());
                    }
                });
                builder.show();
            }
        });
        binding.selectType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (productTypes == null) {
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("ประเภท");

                String[] items = new String[productTypes.size()];
                for (int i = 0; i < productTypes.size(); i++) {
                    items[i] = productTypes.get(i).getTypeName();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedProductType = productTypes.get(which);
                        binding.selectType.setTextValue(selectedProductType.getTypeName());
                        clearType();
                        checkType();
                    }
                });
                builder.show();
            }
        });

        binding.selectSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("รอบอก CHEST");

                final String[] items = Constant.SHIRT_SIZE;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShirts = items[which];
                        binding.selectSize.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });

        binding.selectUkSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("ไซค์ UK");

                final String[] items = Constant.SHOES_UK;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShoesUK = items[which];
                        binding.selectUkSize.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });
        binding.selectSizeUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("ไซค์ US");

                final String[] items = Constant.SHOES_US;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShoesUS = items[which];
                        binding.selectSizeUs.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });

        binding.selectSizeEu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("ไซค์ EU");

                final String[] items = Constant.SHOES_EU;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShoesEU = items[which];
                        binding.selectSizeEu.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });
        binding.selectWaist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("รอบเอว WAIST");

                final String[] items = Constant.PANT_WAIST;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedPants1 = items[which];
                        binding.selectWaist.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });
        binding.selectLength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditSellItemActivity.this);
                builder.setTitle("ความยาว LENGTH");

                final String[] items = Constant.PANT_LENGTH;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedPants2 = items[which];
                        binding.selectLength.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    showProgressDialog(getString(R.string.loading));
                    postRepository.updatePost(
                            detail.getId(),
                            PreferenceManager.getInstance().getUserID(),
                            selectedBrand.getId(),
                            selectedProductType.getTypeId(),
                            binding.etGeneration.getText().toString(),
                            detail.getProductType(),
                            Integer.parseInt(binding.etPrice.getText().toString()),
                            binding.etOtherSize.getText() != null ? binding.etOtherSize.getText().toString() : "",
                            selectedShirts,
                            selectedPants1,
                            selectedPants2,
                            selectedShoesEU,
                            selectedShoesUK,
                            selectedShoesUS,
                            binding.etDetail.getText().toString(),
                            detail.getProvinceId(),
                            detail.getDistrictId(),
                            detail.getPhone(),
                            new HTTPCallback() {
                                @Override
                                public void OnSuccess(Object response) {
                                    Util.showToast(response.toString());
                                    hideProgressDialog();
                                    Intent intent = new Intent(EditSellItemActivity.this, ViewSellItemActivity.class);
                                    intent.putExtra("PostID", postID);
                                    startActivity(intent);
                                    finish();
                                }

                                @Override
                                public void OnError(String message) {
                                    hideProgressDialog();
                                    Util.showToast(message);
                                }
                            }
                    );
                }
            }
        });

        binding.ivUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSelectImage();
            }
        });
    }

    private HTTPCallback brandHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            brands = (List<BrandResultData>) response;
            postRepository.getPostDetail(postID, PreferenceManager.getInstance().getUserID(), httpCallback);

        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };
    private HTTPCallback httpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            resultData = (PostDetailResultData) response;
            initData();
            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private HTTPCallback productTypeResponse = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            productTypes = (List<ProductTypeResultData>) response;
            if (selectedProductType == null) {
                for (ProductTypeResultData productType : productTypes) {
                    if (productType.getTypeId() == detail.getTypeId()) {
                        selectedProductType = productType;
                        binding.selectType.setTextValue(selectedProductType.getTypeName());
                        checkType();
                        break;
                    }

                }
            }
            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private void initData() {
        profile = resultData.getProfile();
        detail = resultData.getPost();

        Util.loadImage(this, profile.getImage(), binding.profileImage, binding.imageProgress);
        binding.tvFullName.setText(profile.getName());
        binding.tvProvince.setText(profile.getProvince().getNameTh());
        binding.tvDistrict.setText(profile.getDistrict().getNameTh());
        binding.tvPhone.setText(profile.getPhone());
        binding.userRating.setRating(Integer.parseInt(profile.getRating()));
        binding.tvDate.setText(detail.getPostDate());
        if (detail.getProductType().toUpperCase().equals("NEW")) {
            binding.tvProductType.setText("สินค้าใหม่");
        } else {
            binding.tvProductType.setText("สินค้ามือสอง");
        }
        List<String> tempImage = new ArrayList<>();
        for (String img : detail.getImages()) {
            tempImage.add(detail.getImagePath() + img);
        }
        ImageRecyclerViewAdapter2 adapter2 = new ImageRecyclerViewAdapter2(tempImage);
        adapter2.setClickListener(recyclerViewItemClickListener);
        binding.recyclerView.setAdapter(adapter2);
        Util.displayMemberType(binding.tvMemberType, profile.getType(), this);

        for (BrandResultData brand : brands) {
            if (brand.getId() == detail.getBrandId()) {
                selectedBrand = brand;
                break;
            }
        }
        binding.selectBrand.setTextValue(selectedBrand.getName());
        loadType(selectedBrand.getId());


        if (detail.getShirtsSize() != null) {
            selectedShirts = detail.getShirtsSize();
            binding.selectSize.setTextValue(selectedShirts);
        }


        if (detail.getShoesSizeEu() != null) {
            selectedShoesEU = detail.getShoesSizeEu();
            binding.selectSizeEu.setTextValue(selectedShoesEU);
        }
        if (detail.getShoesSizeUk() != null) {
            selectedShoesUK = detail.getShoesSizeUk();
            binding.selectUkSize.setTextValue(selectedShoesUK);
        }
        if (detail.getShoesSizeUS() != null) {
            selectedShoesUS = detail.getShoesSizeUS();
            binding.selectSizeUs.setTextValue(selectedShoesUS);
        }


        if (detail.getPants_size_1() != null) {
            selectedPants1 = detail.getPants_size_1();
            binding.selectWaist.setTextValue(detail.getPants_size_1());
        }
        if (detail.getPants_size_2() != null) {
            selectedPants2 = detail.getPants_size_2();
            binding.selectLength.setTextValue(detail.getPants_size_2());
        }

        if (detail.getSize() != null) {
            binding.etOtherSize.setText(detail.getSize());
        }

        binding.etPrice.setText(detail.getPrice().replace(",", ""));
        binding.etDetail.setText(detail.getDescription());
        binding.etGeneration.setText(detail.getGeneration());
    }

    private void loadType(int brandUD) {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getProductType(brandUD, productTypeResponse);
    }

    private void clearType() {
        binding.layoutPants.setVisibility(View.GONE);
        binding.layoutShirts.setVisibility(View.GONE);
        binding.layoutShoes.setVisibility(View.GONE);
        binding.layoutOther.setVisibility(View.GONE);

        binding.selectSize.setTextValue("เลือกรอบอก CHEST");

        binding.selectSizeEu.setTextValue("เลือกไซต์ EU");
        binding.selectUkSize.setTextValue("เลือกไซต์ UK");
        binding.selectSizeUs.setTextValue("เลือกไซต์ US");

        binding.selectWaist.setTextValue("เลือกรอบเอว WAIST");
        binding.selectLength.setTextValue("เลือกความยาว LENGTH");

        binding.etOtherSize.setText(null);

        selectedShirts = null;

        selectedShoesEU = null;
        selectedShoesUK = null;
        selectedShoesUS = null;

        selectedPants1 = null;
        selectedPants2 = null;
    }

    private void checkType() {
        switch (selectedProductType.getSizeOption()) {
            case "pants":
                binding.layoutPants.setVisibility(View.VISIBLE);
                break;
            case "shirts":
                binding.layoutShirts.setVisibility(View.VISIBLE);
                break;
            case "shoes":
                binding.layoutShoes.setVisibility(View.VISIBLE);
                break;
            default:
                binding.layoutOther.setVisibility(View.VISIBLE);
                break;
        }
    }

    private IRecyclerViewItemClickListener recyclerViewItemClickListener = new IRecyclerViewItemClickListener() {
        @Override
        public void OnItemClick(Object item) {

        }

        @Override
        public void OnItemLongClick(View view,final Object item) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(EditSellItemActivity.this)
                    .setMessage("ยืนยันการลบ")
                    .setCancelable(true)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            showProgressDialog(getString(R.string.loading));
                            postRepository.deletePostImage(postID, detail.getImages().get((int)item), new HTTPCallback() {
                                @Override
                                public void OnSuccess(Object response) {
                                    postRepository.getPostDetail(postID, PreferenceManager.getInstance().getUserID(), httpCallback);
                                }

                                @Override
                                public void OnError(String message) {
                                    hideProgressDialog();
                                    Util.showToast(message);
                                }
                            });
                        }
                    });
            alertDialog.show();
        }
    };

    private boolean validate() {
        boolean valid = true;

        if (selectedBrand == null) {
            binding.selectBrand.setError("แบรนด์");
            valid = false;
        } else {
            binding.selectBrand.setError(null);
        }

        if (binding.etGeneration.getText().length() == 0) {
            binding.etGeneration.setError("รุ่นสินค้า");
            valid = false;
        } else {
            binding.etGeneration.setError(null);
        }

        if (selectedProductType == null) {
            binding.selectType.setError("ประเภท");
            valid = false;
        } else {
            binding.selectType.setError(null);
        }

        if (selectedProductType != null) {

            binding.selectWaist.setError(null);
            binding.selectLength.setError(null);
            binding.selectSize.setError(null);
            binding.selectUkSize.setError(null);
            binding.selectSizeUs.setError(null);
            binding.etOtherSize.setError(null);

            switch (selectedProductType.getSizeOption()) {
                case "pants":

                    if (selectedPants1 == null) {
                        binding.selectWaist.setError("เลือกรอบเอว WAIST");
                        valid = false;
                    }

                    if (selectedPants2 == null) {
                        binding.selectLength.setError("เลือกความยาว LENGTH");
                        valid = false;
                    }

                    break;
                case "shirts":
                    if (selectedShirts == null) {
                        binding.selectSize.setError("เลือกรอบอก CHEST");
                        valid = false;
                    }
                    break;
                case "shoes":
                    if (selectedShoesEU == null) {
                        binding.selectSizeEu.setError("เลือกไซต์ EU");
                        valid = false;
                    }

                    if (selectedShoesUK == null) {
                        binding.selectUkSize.setError("เลือกไซต์ UK");
                        valid = false;
                    }

                    if (selectedShoesUS == null) {
                        binding.selectSizeUs.setError("เลือกไซต์ Inter");
                        valid = false;
                    }
                    break;
                default:
                    if (binding.etOtherSize.getText().length() == 0) {
                        binding.etOtherSize.setError("Size");
                        valid = false;
                    }
                    break;
            }
        }

        if (binding.etPrice.getText().length() == 0) {
            binding.etPrice.setError("ราคา");
            valid = false;
        } else {
            binding.etPrice.setError(null);
        }

        if (binding.etDetail.getText().length() == 0) {
            binding.etDetail.setError("รายละเอียด");
            valid = false;
        } else {
            binding.etDetail.setError(null);
        }


        return valid;
    }

    public void openSelectImage() {
        CustomImagePicker customImagePicker = CustomImagePicker.create(this)
                .returnAfterFirst(true)
                .folderMode(true)
                .folderTitle("Photo")
                .showCamera(true)
                .multi();
        customImagePicker.start(Constant.REQUEST_CODE_ADVANCE_SEARCH_POST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == Constant.REQUEST_CODE_ADVANCE_SEARCH_POST) {
            List<Image> images = CustomImagePicker.getImages(data);
            showProgressDialog(getString(R.string.loading));
            postRepository.uploadPostImage(postID, images, new HTTPCallback() {
                @Override
                public void OnSuccess(Object response) {
                    postRepository.getPostDetail(postID, PreferenceManager.getInstance().getUserID(), httpCallback);
                }

                @Override
                public void OnError(String message) {
                    Util.showToast(message);
                    hideProgressDialog();
                }
            });

        }
    }

}
