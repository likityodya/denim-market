package thailand.market.denim.denim.Util;

import com.google.gson.annotations.SerializedName;

/**
 * Created by chawdee on 22/06/2016.
 */
public class MessageEvent {

    /*
     * Result
     */
    public enum ResultType {

        @SerializedName("1")
        BEGIN(1),

        @SerializedName("2")
        SUCCESS(2),

        @SerializedName("3")
        ERROR(3);

        private int value;

        // Constructor
        ResultType(int p) {
            value = p;
        }

        public int getValue() {
            return value;
        }
    }


    /*
     * Activity Type
     */
    public enum ActivityType {

        @SerializedName("1")
        REFRESH_TOKEN(1),

        @SerializedName("2")
        GET_CHANGED_DATA(2),

        @SerializedName("3")
        GET_TEAM_CHAT(3),

        @SerializedName("4")
        GET_USER_CHAT(3);

        private int value;

        // Constructor
        ActivityType(int p) {
            value = p;
        }

        public int getValue() {
            return value;
        }
    }


    public final ResultType result;
    public final ActivityType type;
    public final String message;
    public final Object data;


    public MessageEvent(ResultType result, ActivityType type) {

        this.result = result;
        this.type = type;
        this.message = null;
        this.data = null;
    }

    public MessageEvent(ResultType result, ActivityType type, String message) {
        this.result = result;
        this.type = type;
        this.message = message;
        this.data = null;
    }

    public MessageEvent(ResultType result, ActivityType type, Object data) {
        this.result = result;
        this.type = type;
        this.message = null;
        this.data = data;
    }
}