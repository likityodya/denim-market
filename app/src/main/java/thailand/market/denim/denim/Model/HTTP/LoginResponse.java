package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class LoginResponse extends BaseResponse {

    @SerializedName("data")
    private LoginResultData loginResultData;

    public LoginResultData getLoginResultData() {
        return loginResultData;
    }
}
