package thailand.market.denim.denim.ListViewItem;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.esafirm.imagepicker.model.Image;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItemBinding;

/**
 * Created by Phitakphong on 15/7/2560.
 */

public class DummyImageItemViewHolder extends RecyclerView.ViewHolder {
    private RecyclerPhotoGridItemBinding binding;

    public DummyImageItemViewHolder(RecyclerPhotoGridItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void displayImage(Drawable image) {
        binding.ivImage.setImageDrawable(image);
        binding.progressBar.setVisibility(View.GONE);
    }
}

