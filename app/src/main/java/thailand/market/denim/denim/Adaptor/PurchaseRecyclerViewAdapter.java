package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.PurchaseItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.MyOrderListData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerPurchaseItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class PurchaseRecyclerViewAdapter extends RecyclerView.Adapter<PurchaseItemViewHolder> {

    private List<MyOrderListData> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener itemClickListener;

    public PurchaseRecyclerViewAdapter(List<MyOrderListData> items) {
        this.items = items;
    }


    @Override
    public PurchaseItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerPurchaseItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_purchase_item, parent, false);
        return new PurchaseItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PurchaseItemViewHolder postItemViewHolder, final int position) {
        final MyOrderListData data = items.get(position);
        postItemViewHolder.displayImage(data.getImage());
        postItemViewHolder.setBrand(data.getBrandName());
        postItemViewHolder.setGeneration(data.getGeneration());
        postItemViewHolder.setPrice(data.getPrice());
        postItemViewHolder.setDate(data.getDate());
        postItemViewHolder.setStatus(data.getStatus());
        postItemViewHolder.getLayoutItem().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.OnItemClick(data);
            }
        });
        postItemViewHolder.getLayoutItem().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (data.getStatus().equals("pending") || data.getStatus().equals("completed")) {
                    itemClickListener.OnItemLongClick(v, data);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItemClickListener(IRecyclerViewItemClickListener onClickListener) {
        this.itemClickListener = onClickListener;
    }
}