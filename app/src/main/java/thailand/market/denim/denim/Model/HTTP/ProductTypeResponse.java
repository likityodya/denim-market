package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class ProductTypeResponse extends BaseResponse {

    @SerializedName("data")
    private List<ProductTypeResultData> productTypeResultDataList;

    public List<ProductTypeResultData> getProductTypeResultData() {
        return productTypeResultDataList;
    }
}
