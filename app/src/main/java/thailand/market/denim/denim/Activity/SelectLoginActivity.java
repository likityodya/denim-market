package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.LoginResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.MessageEvent;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivitySelectLoginBinding;

public class SelectLoginActivity extends BaseActivity {

    private CallbackManager callbackManager;
    private AccessToken fbToken;
    private ActivitySelectLoginBinding binding;
    private String firebaseToken;
    public static final int REQUEST_CODE = 101;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_login);

        initInstance();
    }

    private void initInstance() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, LoginResult);

        binding.btnLogin.setOnClickListener(onLoginClickListener);
        binding.tvRegister.setPaintFlags(binding.tvRegister.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.tvRegister.setOnClickListener(onRegisterClickListener);
        binding.btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog(getString(R.string.logging_in));
                LoginManager.getInstance().logInWithReadPermissions(SelectLoginActivity.this, Arrays.asList("public_profile", "email"));
            }
        });
        showDialogForWaitFirebaseToken();
    }

    private View.OnClickListener onRegisterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goRegister();
        }
    };
    private View.OnClickListener onLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            goLogin();
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case LoginActivity.REQUEST_CODE:
                    setResult(Activity.RESULT_OK);
                    finish();
                    break;
                case RegisterActivity.REQUEST_CODE:
                    goLogin();
                    break;
            }
        }
    }

    private FacebookCallback<com.facebook.login.LoginResult> LoginResult = new FacebookCallback<com.facebook.login.LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            String facebookToken = loginResult.getAccessToken().getToken();

            MemberRepository memberRepository = new MemberRepository();
            memberRepository.facebookLogin(facebookToken, firebaseToken, new HTTPCallback() {
                @Override
                public void OnSuccess(Object response) {
                    LoginResultData data = (LoginResultData) response;
                    PreferenceManager.getInstance().setUserID(data.getUserID());
                    PreferenceManager.getInstance().setToken(data.getToken());
                    finish();
                }

                @Override
                public void OnError(String message) {
                    hideProgressDialog();
                    Util.showToast(message);
                }
            });

        }

        @Override
        public void onCancel() {
            finish();
        }

        @Override
        public void onError(FacebookException error) {
            hideProgressDialog();
            Util.showToast(error.getMessage());
        }
    };

    GraphRequest.GraphJSONObjectCallback graphJSONObjectCallback = new GraphRequest.GraphJSONObjectCallback() {
        @Override
        public void onCompleted(JSONObject object, GraphResponse response) {
            try {
                String name = object.getString("name");
                String email = object.getString("email");
                setResult(Activity.RESULT_OK);
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private void goRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivityForResult(intent, RegisterActivity.REQUEST_CODE);
    }

    private void goLogin() {
        Intent intent = new Intent(SelectLoginActivity.this, LoginActivity.class);
        intent.putExtra("FCMToken", firebaseToken);
        startActivityForResult(intent, LoginActivity.REQUEST_CODE);
    }

    private void showDialogForWaitFirebaseToken() {
        firebaseToken = FirebaseInstanceId.getInstance().getToken();
        if (firebaseToken == null) {
            showProgressDialog("Connecting to server...");
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final MessageEvent event) {
        hideProgressDialog();
        firebaseToken = event.message;
    }

}
