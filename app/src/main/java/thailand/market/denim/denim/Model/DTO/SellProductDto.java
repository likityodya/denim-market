package thailand.market.denim.denim.Model.DTO;

import com.esafirm.imagepicker.model.Image;

import java.util.List;

/**
 * Created by Phitakphong on 14/7/2560.
 */

public class SellProductDto {
    private int user_id;
    private int brand_id;
    private int type_id;
    private String generation;
    private String product_type;
    private int price;
    private String description;
    private int province_id;
    private int district_id;
    private String size;
    private String shirts_size;
    private String pants_w;
    private String pants_size;
    private String shoes_size_eu;
    private String shoes_size_uk;
    private String shoes_size_us;
    private String phone;
    private List<Image> images;

    public int getUserId() {
        return user_id;
    }

    public void setUserId(int user_id) {
        this.user_id = user_id;
    }

    public int getBrandId() {
        return brand_id;
    }

    public void setBrandId(int brand_id) {
        this.brand_id = brand_id;
    }

    public int getTypeId() {
        return type_id;
    }

    public void setTypeId(int type_id) {
        this.type_id = type_id;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getProductType() {
        return product_type;
    }

    public void setProductType(String product_type) {
        this.product_type = product_type;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProvinceId() {
        return province_id;
    }

    public void setProvinceId(int province_id) {
        this.province_id = province_id;
    }

    public int getDistrictId() {
        return district_id;
    }

    public void setDistrictId(int district_id) {
        this.district_id = district_id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getShirtsSize() {
        return shirts_size;
    }

    public void setShirtsSize(String shirts_size) {
        this.shirts_size = shirts_size;
    }

    public String getPantsW() {
        return pants_w;
    }

    public void setPantsW(String pants_w) {
        this.pants_w = pants_w;
    }

    public String getPantSize() {
        return pants_size;
    }

    public void setPantsSize(String pants_size) {
        this.pants_size = pants_size;
    }

    public String getShoesSizeUk() {
        return shoes_size_uk;
    }

    public void setShoesSizeUk(String shoes_size_uk) {
        this.shoes_size_uk = shoes_size_uk;
    }

    public String getShoesSizeUS() {
        return shoes_size_us;
    }

    public void setShoesSizeUS(String shoes_size_inter) {
        this.shoes_size_us = shoes_size_inter;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getShoesSizeEU() {
        return shoes_size_eu;
    }

    public void setShoesSizeEU(String shoesSizeEU) {
        this.shoes_size_eu = shoesSizeEU;
    }
}