package thailand.market.denim.denim.Manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PreferenceManager {

    private static PreferenceManager instance;

    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public static PreferenceManager getInstance() {
        if (instance == null)
            instance = new PreferenceManager();
        return instance;
    }

    private Context mContext;

    private PreferenceManager() {
        mContext = Contextor.getInstance().getContext();

        prefs = mContext.getSharedPreferences("preference", Context.MODE_PRIVATE);
        editor = prefs.edit();
    }

    /****************************
     * Preferences
     ****************************/

    public String getToken() {
        return prefs.getString("Token", null);
    }

    public void setToken(String token) {
        editor.putString("Token", token);
        editor.apply();
    }
    public int getUserID() {
        return prefs.getInt("UserID", 0);
    }

    public void setUserID(int userID) {
        editor.putInt("UserID", userID);
        editor.apply();
    }
}
