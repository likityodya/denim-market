package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.ListViewItem.BrandItemViewHolder;
import thailand.market.denim.denim.ListViewItem.NewsItemViewHolder;
import thailand.market.denim.denim.ListViewItem.PromotionItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.NewsResultData;
import thailand.market.denim.denim.Model.HTTP.PromotionResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerBrandItemBinding;
import thailand.market.denim.denim.databinding.RecyclerPromotionItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class PromotionRecyclerViewAdapter extends RecyclerView.Adapter<PromotionItemViewHolder> {

    private List<PromotionResultData> items;
    private LayoutInflater inflater;

    public PromotionRecyclerViewAdapter(List<PromotionResultData> items) {
        this.items = items;
    }

    @Override
    public PromotionItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerPromotionItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_promotion_item, parent, false);
        return new PromotionItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PromotionItemViewHolder holder, final int position) {
        final PromotionResultData item = items.get(position);
        holder.setTitle(item.getName());
        holder.setTitle(item.getDescription());
        holder.displayImage(item.getImage());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}