package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.LoadingViewHolder;
import thailand.market.denim.denim.ListViewItem.SearchPostItemContactViewHolder;
import thailand.market.denim.denim.ListViewItem.SearchPostItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.SearchPostListData;
import thailand.market.denim.denim.Model.SearchPostItem;
import thailand.market.denim.denim.Model.SearchPostItemContact;
import thailand.market.denim.denim.Model.SearchPostItemLoading;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerLoadingItemLayoutBinding;
import thailand.market.denim.denim.databinding.RecyclerSearchPostContactBinding;
import thailand.market.denim.denim.databinding.RecyclerSearchPostItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class SearchPostRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int POST_ITEM_TYPE = 0;
    private static final int POST_CONTACT = 1;
    private static final int POST_LOADING = 2;

    private List<SearchPostItem> items;
    private LayoutInflater inflater;
    private View.OnClickListener contactClickListener;
    private IRecyclerViewItemClickListener itemClickListener;

    public SearchPostRecyclerViewAdapter(List<SearchPostItem> items) {
        this.items = items;
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof SearchPostListData) {
            return POST_ITEM_TYPE;
        } else if (items.get(position) instanceof SearchPostItemContact) {
            return POST_CONTACT;
        } else if (items.get(position) instanceof SearchPostItemLoading) {
            return POST_LOADING;
        }
        return -1;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        if (viewType == POST_ITEM_TYPE) {
            RecyclerSearchPostItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_search_post_item, parent, false);
            return new SearchPostItemViewHolder(binding);
        } else if (viewType == POST_CONTACT) {
            RecyclerSearchPostContactBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_search_post_contact, parent, false);
            return new SearchPostItemContactViewHolder(binding);
        } else if (viewType == POST_LOADING) {
            RecyclerLoadingItemLayoutBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_loading_item_layout, parent, false);
            return new LoadingViewHolder(binding);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof SearchPostItemViewHolder) {
            SearchPostItemViewHolder postItemViewHolder = (SearchPostItemViewHolder) holder;
            final SearchPostListData data = (SearchPostListData) items.get(position);
            postItemViewHolder.displayImage(data.getImages());
            postItemViewHolder.setBrand(data.getBrand());
            postItemViewHolder.setGeneration(data.getGeneration());
            postItemViewHolder.setPrice(data.getPrice());
            postItemViewHolder.setMemberType(data.getUserType());
            postItemViewHolder.setDistrict(data.getDistrict().getNameTh());
            postItemViewHolder.setProvince(data.getProvince().getNameTh());
            postItemViewHolder.setProductType(data.getProductType());
            postItemViewHolder.setSize(data.getSize());
            postItemViewHolder.setType(data.getType());
            postItemViewHolder.isTopAd(data.isPinStatus());
            postItemViewHolder.getLayoutItem().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.OnItemClick(data);
                }
            });
        } else if (holder instanceof SearchPostItemContactViewHolder) {
            if (this.contactClickListener != null) {
                SearchPostItemContactViewHolder itemContactViewHolder = (SearchPostItemContactViewHolder) holder;
                itemContactViewHolder.setOnClickListener(this.contactClickListener);
            }
        } else if (holder instanceof LoadingViewHolder) {
            SearchPostItemLoading data = (SearchPostItemLoading) items.get(position);
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.setLoading(data.isLoading());
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnContactClikListener(View.OnClickListener onClickListener) {
        this.contactClickListener = onClickListener;
    }
    public void setItemClickListener(IRecyclerViewItemClickListener  onClickListener) {
        this.itemClickListener = onClickListener;
    }
}