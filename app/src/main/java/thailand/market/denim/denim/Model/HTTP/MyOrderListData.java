package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 29/7/2560.
 */

public class MyOrderListData {

    @SerializedName("id")
    private int id;

    @SerializedName("price")
    private String price;

    @SerializedName("status")
    private String status;

    @SerializedName("image")
    private String image;

    @SerializedName("date")
    private String date;

    @SerializedName("generation")
    private String generation;

    @SerializedName("brand_name")
    private String brand_name;

    public int getId() {
        return id;
    }

    public String getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public String getImage() {
        return image;
    }

    public String getDate() {
        return date;
    }

    public String getGeneration() {
        return generation;
    }

    public String getBrandName() {
        return brand_name;
    }
}
