package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.esafirm.imagepicker.model.Image;

import java.io.File;
import java.util.List;

import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Model.HTTP.DistrictData;
import thailand.market.denim.denim.Model.HTTP.DistrictResultData;
import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.Model.HTTP.ProvinceResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.CommonRepository;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.ImageResize;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentManageProfileBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ManageProfileFragment extends BaseFragment {

    private FragmentManageProfileBinding binding;
    private WrapperFragment wrapperFragment;
    private ProfileResultData profileResultData;
    private List<ProvinceResultData> provinceResultDataList;
    private CommonRepository commonRepository;
    private ProvinceResultData selectedProvince;
    private DistrictData selectedDistrict;

    private Image selectedImage;

    public ManageProfileFragment() {
        super();
    }

    public static ManageProfileFragment newInstance(ProfileResultData profileResultData) {
        ManageProfileFragment fragment = new ManageProfileFragment();
        Bundle args = new Bundle();
        args.putParcelable("ProfileResultData", profileResultData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentManageProfileBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        profileResultData = getArguments().getParcelable("ProfileResultData");
        commonRepository = new CommonRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        binding.tvDelete.setPaintFlags(binding.tvDelete.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        initData();
        initEvents();
    }

    private void initData() {

        loadImage(profileResultData.getImage());


        binding.userInfo.setProfileResultData(profileResultData);
        int star = Integer.parseInt(profileResultData.getRating());
        binding.userRating.setRating(star);

        binding.etTel.setText(profileResultData.getPhone());
        binding.etFacebook.setText(profileResultData.getFacebook());
        binding.etLine.setText(profileResultData.getLineId());
        binding.etEmail.setText(profileResultData.getEmail());
        binding.tvCreatedAt.setText(profileResultData.getCreatedAt());
        binding.tvIdCard.setText(profileResultData.getNationalityId());
        binding.etFirstName.setText(profileResultData.getFirstName());
        binding.etLastName.setText(profileResultData.getLastName());
        binding.etAddress.setText(profileResultData.getAddress());

        selectedProvince = profileResultData.getProvince();
        selectedDistrict = profileResultData.getDistrict();

        if (selectedProvince != null) {
            binding.selectProvince.setTextValue(selectedProvince.getNameTh());
        } else {
            binding.selectProvince.setTextValue(getString(R.string.province));
        }


        if (selectedDistrict != null) {
            binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());
        } else {
            binding.selectDistrict.setTextValue(getString(R.string.district));
        }

        showProgressDialog(getString(R.string.loading));

        commonRepository.getProvinces(provinceCallback);

    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });

        binding.selectProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.province));

                String[] items = new String[provinceResultDataList.size()];
                for (int i = 0; i < provinceResultDataList.size(); i++) {
                    items[i] = provinceResultDataList.get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedDistrict = null;
                        binding.selectDistrict.setTextValue(getString(R.string.district));
                        selectedProvince = provinceResultDataList.get(which);
                        binding.selectProvince.setTextValue(selectedProvince.getNameTh());
                        if (selectedProvince.getDistrict().size() == 0) {
                            getDistrict(which);
                        }
                    }
                });
                builder.show();
            }
        });

        binding.selectDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedProvince == null){
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.district));

                String[] items = new String[selectedProvince.getDistrict().size()];
                for (int i = 0; i < selectedProvince.getDistrict().size(); i++) {
                    items[i] = selectedProvince.getDistrict().get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedDistrict = selectedProvince.getDistrict().get(which);
                        binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());
                    }
                });
                builder.show();
            }
        });

        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {

                    showProgressDialog(getString(R.string.loading));
                    File file = null;
                    if (selectedImage != null) {
                        try {
                            ImageResize resize = new ImageResize();
                            String resized = resize.resize(selectedImage.getPath());
                            file = new File(resized);
                        } catch (Exception e) {
                            Util.showToast(e.getMessage());
                            return;
                        }
                    }

                    profileResultData.setPhone(binding.etTel.getText().toString());
                    profileResultData.setFacebook(binding.etFacebook.getText().toString());
                    profileResultData.setLineId(binding.etLine.getText().toString());
                    profileResultData.setEmail(binding.etEmail.getText().toString());

                    profileResultData.setFirstName(binding.etFirstName.getText().toString().trim());
                    profileResultData.setLastName(binding.etLastName.getText().toString().trim());
                    profileResultData.setAddress(binding.etAddress.getText().toString().trim());
                    profileResultData.setProvince(selectedProvince);
                    profileResultData.setDistrict(selectedDistrict);

                    MemberRepository memberRepository = new MemberRepository();
                    memberRepository.updateProfile(profileResultData, file, new HTTPCallback() {
                        @Override
                        public void OnSuccess(Object response) {
                            hideProgressDialog();
                            wrapperFragment.onGoBackFragment();
                        }

                        @Override
                        public void OnError(String message) {
                            Util.showToast(message);
                            hideProgressDialog();
                        }
                    });

                }
            }
        });

        binding.profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IFragmentListener listener = (IFragmentListener) getActivity();
                listener.openSelectImage(Constant.REQUEST_CODE_PICKER_PROFILE);
            }
        });
    }

    private HTTPCallback provinceCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            provinceResultDataList = (List<ProvinceResultData>) response;
            if (selectedProvince != null) {
                int index = 0;
                for (ProvinceResultData prov : provinceResultDataList) {
                    if (prov.getId() == selectedProvince.getId()) {
                        break;
                    }
                    index++;
                }
                getDistrict(index);
            } else {
                hideProgressDialog();
            }

        }

        @Override
        public void OnError(String message) {
            Util.showToast(message);
            hideProgressDialog();
        }
    };

    private void getDistrict(final int index) {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getDistrict(selectedProvince.getId(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                DistrictResultData districtResultData = (DistrictResultData) response;
                selectedProvince.setDistrict(districtResultData.getDistricts());
                provinceResultDataList.set(index, selectedProvince);
                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });
    }

    private boolean validate() {
        boolean validateResult = true;

        if (TextUtils.isEmpty(binding.etFirstName.getText())) {
            validateResult = false;
            binding.etFirstName.setError(getString(R.string.require));
        } else {
            binding.etFirstName.setError(null);
        }

        if (TextUtils.isEmpty(binding.etLastName.getText())) {
            validateResult = false;
            binding.etLastName.setError(getString(R.string.require));
        } else {
            binding.etLastName.setError(null);
        }

        if (TextUtils.isEmpty(binding.etAddress.getText())) {
            validateResult = false;
            binding.etAddress.setError(getString(R.string.require));
        } else {
            binding.etAddress.setError(null);
        }

        if (selectedProvince == null) {
            validateResult = false;
            binding.selectProvince.setError(getString(R.string.require));
        } else {
            binding.selectProvince.setError(null);
        }

        if (selectedDistrict == null) {
            validateResult = false;
            binding.selectDistrict.setError(getString(R.string.require));
        } else {
            binding.selectDistrict.setError(null);
        }

        return validateResult;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    public void setSelectedImage(Image image) {
        this.selectedImage = image;
        loadImage(image.getPath());
    }

    private void loadImage(String image) {
        Util.loadImage(getContext(), image, binding.profileImage,binding.imageProgress);
    }
}
