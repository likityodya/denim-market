package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.os.Bundle;
import android.view.View;

import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivityRatingUserBinding;

public class RatingUserActivity extends BaseActivity {

    private ActivityRatingUserBinding binding;
    private MemberRepository repository;
    private int rating;
    private int userID;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rating_user);

        repository = new MemberRepository();

        rating = getIntent().getIntExtra("Rating", 0);
        userID = getIntent().getIntExtra("UserID", 0);

        setRating(rating);

        binding.star1.setOnClickListener(starOnClickListener);
        binding.star2.setOnClickListener(starOnClickListener);
        binding.star3.setOnClickListener(starOnClickListener);
        binding.star4.setOnClickListener(starOnClickListener);
        binding.star5.setOnClickListener(starOnClickListener);

        binding.btnVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressDialog(getString(R.string.loading));
                repository.voteUser(PreferenceManager.getInstance().getUserID(), userID, rating, new HTTPCallback() {
                    @Override
                    public void OnSuccess(Object response) {
                        Util.showToast(response.toString());
                        hideProgressDialog();
                        Intent intent = getIntent();
                        intent.putExtra("Rating", rating);
                        setResult(Activity.RESULT_OK, intent);
                        finish();
                    }

                    @Override
                    public void OnError(String message) {
                        Util.showToast(message);
                        hideProgressDialog();
                    }
                });
            }
        });
    }


    public void setRating(int rating) {
        Drawable ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ico_star, null);
        binding.star1.setImageDrawable(ratingDrawable);
        binding.star2.setImageDrawable(ratingDrawable);
        binding.star3.setImageDrawable(ratingDrawable);
        binding.star4.setImageDrawable(ratingDrawable);
        binding.star5.setImageDrawable(ratingDrawable);

        ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ico_star_yellow, null);
        if (rating >= 1) {
            binding.star1.setImageDrawable(ratingDrawable);
        }
        if (rating >= 2) {
            binding.star2.setImageDrawable(ratingDrawable);
        }
        if (rating >= 3) {
            binding.star3.setImageDrawable(ratingDrawable);
        }
        if (rating >= 4) {
            binding.star4.setImageDrawable(ratingDrawable);
        }
        if (rating == 5) {
            binding.star5.setImageDrawable(ratingDrawable);
        }
    }

    private View.OnClickListener starOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.star_1:
                    rating = 1;
                    break;
                case R.id.star_2:
                    rating = 2;
                    break;
                case R.id.star_3:
                    rating = 3;
                    break;
                case R.id.star_4:
                    rating = 4;
                    break;
                case R.id.star_5:
                    rating = 5;
                    break;
            }
            setRating(rating);
        }
    };
}
