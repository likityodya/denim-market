package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class ForumResponse extends BaseResponse {

    @SerializedName("data")
    private ForumResultData forumResultData;

    public ForumResultData getForumResultData() {
        return forumResultData;
    }
}
