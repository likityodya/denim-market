package thailand.market.denim.denim.Util;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created by com_s on 13-Jan-17.
 */

public class DialogUtil {

    private ProgressDialog progressDialog;
    private Activity activity;

    public DialogUtil(Activity activity) {
        this.activity = activity;
        progressDialog = new ProgressDialog(activity);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
    }

    public void showProgressDialog(final String message) {
        if (progressDialog.isShowing()) {
            return;
        }
        progressDialog.setMessage(message + "...");
        progressDialog.show();
    }

    public void hideProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void cancel() {
        progressDialog.dismiss();
    }
}
