package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class MyOrderListResponse extends BaseResponse {

    @SerializedName("data")
    private List<MyOrderListData> orderListData;

    public List<MyOrderListData> getOrderListData() {
        return orderListData;
    }
}
