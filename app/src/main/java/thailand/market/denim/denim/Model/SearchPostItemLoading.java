package thailand.market.denim.denim.Model;

/**
 * Created by Phitakphong on 18/7/2560.
 */

public class SearchPostItemLoading extends SearchPostItem {
    private boolean loading;

    public boolean isLoading() {
        return loading;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

}
