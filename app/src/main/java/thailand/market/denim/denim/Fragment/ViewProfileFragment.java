package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentViewProfileBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ViewProfileFragment extends BaseFragment {

    private FragmentViewProfileBinding binding;
    private WrapperFragment wrapperFragment;
    private ProfileResultData profileResultData;

    public ViewProfileFragment() {
        super();
    }

    public static ViewProfileFragment newInstance() {
        ViewProfileFragment fragment = new ViewProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentViewProfileBinding.inflate(inflater);
        View rootView = binding.getRoot();
        super.init(rootView, inflater);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {

    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        loadData();
        initEvents();
    }

    private void loadData() {
        if (getContext() == null) {
            return;
        }

        showProgressDialog(getString(R.string.loading));
        MemberRepository memberRepository = new MemberRepository();
        memberRepository.getProfile(PreferenceManager.getInstance().getUserID(), httpCallback);
    }

    private HTTPCallback httpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {

            profileResultData = (ProfileResultData) response;
            Util.loadImage(getContext(), profileResultData.getImage(),binding.profileImage,binding.imageProgress);

            binding.userInfo.setProfileResultData(profileResultData);

            int rating = Integer.parseInt(profileResultData.getRating());
            binding.userRating.setRating(rating);

            if (!TextUtils.isEmpty(profileResultData.getPhone())) {
                binding.layoutPhone.setVisibility(View.VISIBLE);
                binding.tvPhone.setText(profileResultData.getPhone());
            } else {
                binding.layoutPhone.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(profileResultData.getFacebook())) {
                binding.layoutFacebook.setVisibility(View.VISIBLE);
                binding.tvFacebook.setText(profileResultData.getFacebook());
            } else {
                binding.layoutFacebook.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(profileResultData.getLineId())) {
                binding.layoutLine.setVisibility(View.VISIBLE);
                binding.tvLine.setText(profileResultData.getLineId());
            } else {
                binding.layoutLine.setVisibility(View.GONE);
            }

            if (!TextUtils.isEmpty(profileResultData.getEmail())) {
                binding.layoutEmail.setVisibility(View.VISIBLE);
                binding.tvEmail.setText(profileResultData.getEmail());
            } else {
                binding.layoutEmail.setVisibility(View.GONE);
            }

            binding.tvCreatedAt.setText(profileResultData.getCreatedAt());

            binding.tvIdCard.setText(profileResultData.getNationalityId());
            binding.tvFirstName.setText(profileResultData.getFirstName());
            binding.tvLastName.setText(profileResultData.getLastName());
            binding.tvAddress.setText(profileResultData.getAddress());
            if(profileResultData.getProvince() != null){
                binding.tvProvince.setText(profileResultData.getProvince().getNameTh());
            }

            if(profileResultData.getDistrict() != null){
                binding.tvDistrict.setText(profileResultData.getDistrict().getNameTh());
            }


            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };


    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });
        binding.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ManageProfileFragment manageProfileFragment = ManageProfileFragment.newInstance(profileResultData);
                wrapperFragment.onGoNextFragment(manageProfileFragment);
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

}
