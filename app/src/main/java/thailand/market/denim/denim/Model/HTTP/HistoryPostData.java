package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 17/7/2560.
 */

public class HistoryPostData {

    @SerializedName("id")
    private int id;

    @SerializedName("pin_status")
    private boolean pin_status;

    @SerializedName("product_type")
    private String product_type;

    @SerializedName("post_date")
    private String post_date;

    @SerializedName("brand")
    private String brand;

    @SerializedName("type")
    private String type;

    @SerializedName("generation")
    private String generation;

    @SerializedName("price")
    private String price;

    @SerializedName("size")
    private String size;

    @SerializedName("description")
    private String description;

    @SerializedName("images")
    private String images;

    @SerializedName("user_name")
    private String user_name;

    @SerializedName("user_type")
    private String user_type;

    @SerializedName("province")
    private ProvinceResultData province;

    @SerializedName("district")
    private DistrictData district;

    public int getId() {
        return id;
    }

    public boolean isPinStatus() {
        return pin_status;
    }

    public String getProductType() {
        return product_type;
    }

    public String getPost_date() {
        return post_date;
    }

    public String getBrand() {
        return brand;
    }

    public String getType() {
        return type;
    }

    public String getGeneration() {
        return generation;
    }

    public String getPrice() {
        return price;
    }

    public String getSize() {
        return size;
    }

    public String getDescription() {
        return description;
    }

    public String getImages() {
        return images;
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUserType() {
        return user_type;
    }

    public ProvinceResultData getProvince() {
        return province;
    }

    public DistrictData getDistrict() {
        return district;
    }
}
