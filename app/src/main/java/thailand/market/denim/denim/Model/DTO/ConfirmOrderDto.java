package thailand.market.denim.denim.Model.DTO;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 14/7/2560.
 */

public class ConfirmOrderDto implements Parcelable {

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("price")
    private int price;

    @SerializedName("first_name")
    private String first_name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("address")
    private String address;

    @SerializedName("phone")
    private String phone;

    @SerializedName("province_id")
    private int province_id;

    @SerializedName("district_id")
    private int district_id;

    @SerializedName("zipcode")
    private String zipcode;

    @SerializedName("shirt_size")
    private String shirt_size;

    @SerializedName("shirt_chest")
    private String shirt_chest;

    @SerializedName("shoes_us")
    private Double shoes_us;

    @SerializedName("shoes_uk")
    private Double shoes_uk;

    @SerializedName("shoes_eu")
    private String shoes_eu;

    @SerializedName("pants_w")
    private String pants_w;

    @SerializedName("pants_size")
    private String pants_size;

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getFirstName() {
        return first_name;
    }

    public void setFirstName(String firstName) {
        this.first_name = firstName;
    }

    public String getLastName() {
        return last_name;
    }

    public void setLastName(String lastName) {
        this.last_name = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getProvinceId() {
        return province_id;
    }

    public void setProvinceId(int provinceId) {
        this.province_id = provinceId;
    }

    public int getDistrictId() {
        return district_id;
    }

    public void setDistrictId(int districtId) {
        this.district_id = districtId;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }


    public String getShirtSize() {
        return shirt_size;
    }

    public void setShirtSize(String shirtSize) {
        this.shirt_size = shirtSize;
    }

    public String getShirtChest() {
        return shirt_chest;
    }

    public void setShirtChest(String shirtChest) {
        this.shirt_chest = shirtChest;
    }

    public Double getShoesUS() {
        return shoes_us;
    }

    public void setShoesUS(Double shoesInter) {
        this.shoes_us = shoesInter;
    }

    public Double getShoesUk() {
        return shoes_uk;
    }

    public void setShoesUk(Double shoesUk) {
        this.shoes_uk = shoesUk;
    }

    public String getPantsW() {
        return pants_w;
    }

    public void setPantsW(String pantsW) {
        this.pants_w = pantsW;
    }

    public String getPantsSize() {
        return pants_size;
    }

    public void setPantsSize(String pantsSize) {
        this.pants_size = pantsSize;
    }

    public String getShoesEU() {
        return shoes_eu;
    }

    public void setShoesEU(String shoesEU) {
        this.shoes_eu = shoesEU;
    }

    public ConfirmOrderDto() {
    }

    protected ConfirmOrderDto(Parcel in) {
        quantity = in.readInt();
        price = in.readInt();
        first_name = in.readString();
        last_name = in.readString();
        address = in.readString();
        phone = in.readString();
        province_id = in.readInt();
        district_id = in.readInt();
        zipcode = in.readString();
        shirt_size = in.readString();
        shirt_chest = in.readString();
        shoes_us = in.readDouble();
        shoes_uk = in.readDouble();
        shoes_eu = in.readString();
        pants_w = in.readString();
        pants_size = in.readString();
    }

    public static final Parcelable.Creator<ConfirmOrderDto> CREATOR = new Parcelable.Creator<ConfirmOrderDto>() {
        @Override
        public ConfirmOrderDto createFromParcel(Parcel in) {
            return new ConfirmOrderDto(in);
        }

        @Override
        public ConfirmOrderDto[] newArray(int size) {
            return new ConfirmOrderDto[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(quantity);
        dest.writeInt(price);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeInt(province_id);
        dest.writeInt(district_id);
        dest.writeString(zipcode);
        dest.writeString(shirt_size);
        dest.writeString(shirt_chest);
        dest.writeDouble(shoes_us);
        dest.writeDouble(shoes_uk);
        dest.writeString(shoes_eu);
        dest.writeString(pants_w);
        dest.writeString(pants_size);
    }
}