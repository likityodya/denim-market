package thailand.market.denim.denim.Manager.HTTP;

import java.util.Map;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import thailand.market.denim.denim.Model.HTTP.BaseResponse;
import thailand.market.denim.denim.Model.HTTP.LoginResponse;
import thailand.market.denim.denim.Model.HTTP.LookForResponse;
import thailand.market.denim.denim.Model.HTTP.MyOrderListResponse;
import thailand.market.denim.denim.Model.HTTP.OrderDetailResponse;
import thailand.market.denim.denim.Model.HTTP.ProfileResponse;
import thailand.market.denim.denim.Model.HTTP.RegisterResponse;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface MemberService {

    @GET("user/profile/{id}")
    Flowable<Response<ProfileResponse>> getProfile(@Path("id") int userID);

    @GET("order/me/{userID}")
    Flowable<Response<MyOrderListResponse>> getMyOrder(@Path("userID") int userID);

    @GET("user/logout/{id}")
    Flowable<Response<BaseResponse>> logout(@Path("id") int userID);

    @FormUrlEncoded
    @POST("register")
    Flowable<Response<RegisterResponse>> register(
            @Field("first_name") String name
            , @Field("last_name") String surname
            , @Field("email") String email
            , @Field("password") String password
            , @Field("password_confirm") String password_confirm
            , @Field("device_type") String deviceType
    );

    @FormUrlEncoded
    @POST("user/login-form")
    Flowable<Response<LoginResponse>> login(
            @Field("email") String email
            , @Field("password") String password
            , @Field("device_token") String deviceToken
            , @Field("device_type") String deviceType
    );

    @Multipart
    @POST("user/update/{userID}")
    Flowable<Response<BaseResponse>> updateProfile(
            @Path("userID") int userID,
            @PartMap() Map<String, RequestBody> partMap,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("user/forget-password")
    Flowable<Response<BaseResponse>> forgetPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("user/change-password/{userID}")
    Flowable<Response<BaseResponse>> changePassword(
            @Path("userID") int userID,
            @Field("old_password") String oldPassword,
            @Field("new_password") String newPassword,
            @Field("new_password_confirm") String confirmPassword
    );

    @Multipart
    @POST("user/verify/{userID}")
    Flowable<Response<BaseResponse>> registerVerify(
            @Path("userID") int userID,
            @Part MultipartBody.Part image
    );

    @FormUrlEncoded
    @POST("user/login-facebook")
    Flowable<Response<LoginResponse>> facebookLogin(
            @Field("fb_token") String facebookToken,
            @Field("device_token") String deviceToken,
            @Field("device_type") String deviceType
    );

    @FormUrlEncoded
    @POST("user/vote/{fromUserID}/{toUserID}")
    Flowable<Response<BaseResponse>> vote(
            @Path("fromUserID") int fromUserID,
            @Path("toUserID") int toUserID,
            @Field("score") int score
    );

    @FormUrlEncoded
    @POST("deposit/create")
    Flowable<Response<BaseResponse>> lookFor(
            @Field("user_id") int userId,
            @Field("brand_id") int brandId,
            @Field("type_id") int typeId,
            @Field("product_type") String productType,
            @Field("min_price") int minPrice,
            @Field("max_price") int maxPrice,
            @Field("province_id") int provinceId,
            @Field("district_id") int districtId,
            @Field("shirts_size_1") String shirts_size_1,
            @Field("pants_size_1") String pants_size_1,
            @Field("pants_size_2") String pants_size_2,
            @Field("shoes_size_eu") String shoes_size_eu,
            @Field("shoes_size_us") String shoes_size_us,
            @Field("shoes_size_uk") String shoes_size_uk
    );

    @GET("deposit/me/{user_id}")
    Flowable<Response<LookForResponse>> myLookFor(@Path("user_id") int userId);

    @GET("deposit/delete/{deposit_id}/{user_id}")
    Flowable<Response<BaseResponse>> deleteLookFor(@Path("deposit_id") int depositId,@Path("user_id") int userId);

    @GET("order/delete/{order_id}/{user_id}")
    Flowable<Response<BaseResponse>> deleteOrder(@Path("order_id") int orderId,@Path("user_id") int userId);

    @GET("order/detail/{id}")
    Flowable<Response<OrderDetailResponse>> getOrderDetail(@Path("id") int id);

}
