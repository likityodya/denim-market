package thailand.market.denim.denim.Repository;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.Path;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.HTTP.ForumService;
import thailand.market.denim.denim.Manager.HttpManager;
import thailand.market.denim.denim.Model.HTTP.BaseResponse;
import thailand.market.denim.denim.Model.HTTP.ForumResponse;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResponse;
import thailand.market.denim.denim.Model.HTTP.ProductDetailResponse;
import thailand.market.denim.denim.Util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class ForumRepository {
    ForumService forumService;

    public ForumRepository() {
        forumService = HttpManager.getInstance().getForumService();
    }

    public void getForum(final HTTPCallback httpCallback) {
        Flowable<Response<ForumResponse>> register = forumService.forum();
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<ForumResponse>>() {
                    @Override
                    public void onNext(Response<ForumResponse> response) {
                        if (response.isSuccessful()) {
                            ForumResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getForumResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getProduct(int brandID, final HTTPCallback httpCallback) {
        Flowable<Response<ProductBrandResponse>> register = forumService.getProduct(brandID);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<ProductBrandResponse>>() {
                    @Override
                    public void onNext(Response<ProductBrandResponse> response) {
                        if (response.isSuccessful()) {
                            ProductBrandResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getProductBrandResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getProductDetail(int productID, final HTTPCallback httpCallback) {
        Flowable<Response<ProductDetailResponse>> register = forumService.getProductDetail(productID);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<ProductDetailResponse>>() {
                    @Override
                    public void onNext(Response<ProductDetailResponse> response) {
                        if (response.isSuccessful()) {
                            ProductDetailResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getProductDetailResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void confirmOrder( int userID,
                              int productID,
                              int userID1,
                              int quantity,
                              String price,
                              String first_name,
                              String last_name,
                              String address,
                              String phone,
                              int province_id,
                              int district_id,
                              String zipcode,
                              String shirts_size_1,
                              String shirts_size_2,
                              String pants_size_1,
                              String pants_size_2,
                              String shoes_inter,
                              String shoes_uk,
                              String shoes_eu,
                              final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> register = forumService.confirmOrder(
                userID,
                productID,
                userID1,
                quantity,
                price,
                first_name,
                last_name,
                address,
                phone,
                province_id,
                district_id,
                zipcode,
                shirts_size_1,
                shirts_size_2,
                pants_size_1,
                pants_size_2,
                shoes_inter,
                shoes_uk,
                shoes_eu
        );
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
