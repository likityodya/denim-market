package thailand.market.denim.denim.ListViewItem;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.esafirm.imagepicker.model.Image;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItemBinding;

/**
 * Created by Phitakphong on 15/7/2560.
 */

public class ImageItemViewHolder extends RecyclerView.ViewHolder {
    private RecyclerPhotoGridItemBinding binding;

    public ImageItemViewHolder(RecyclerPhotoGridItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void displayImage(Image image) {
        Util.loadImage(this.binding.getRoot().getContext(), image.getPath(), binding.ivImage, binding.progressBar);
    }

    public void displayAddImage() {
        binding.ivImage.setImageResource(R.drawable.ico_add);
        binding.progressBar.setVisibility(View.GONE);
    }
    public void displayEmptyImage(Drawable image) {
        binding.ivImage.setImageDrawable(image);
        binding.progressBar.setVisibility(View.GONE);
    }

    public void setOnLongClickListener(final IRecyclerViewItemClickListener onCardClickListener, final int item) {
        binding.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onCardClickListener.OnItemLongClick(v, item);
                return true;
            }
        });
    }
    public void setOnClickListener(final IRecyclerViewItemClickListener onCardClickListener) {
        binding.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCardClickListener.OnItemClick(null);
            }
        });
    }
}
