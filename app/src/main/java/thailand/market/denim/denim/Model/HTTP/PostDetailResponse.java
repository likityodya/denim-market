package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class PostDetailResponse extends BaseResponse {

    @SerializedName("data")
    private PostDetailResultData postDetailResult;

    public PostDetailResultData getPostDetailResult() {
        return postDetailResult;
    }
}
