package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.List;

import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.DistrictData;
import thailand.market.denim.denim.Model.HTTP.DistrictResultData;
import thailand.market.denim.denim.Model.HTTP.ProductTypeResultData;
import thailand.market.denim.denim.Model.HTTP.ProvinceResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.CommonRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivitySearchPostBinding;

public class SearchPostActivity extends BaseActivity {

    private ActivitySearchPostBinding binding;
    private List<BrandResultData> brands;
    private BrandResultData selectedBrand;
    private CommonRepository commonRepository;
    private List<ProvinceResultData> provinces;
    private ProvinceResultData selectedProvince;
    private DistrictData selectedDistrict;
    private List<ProductTypeResultData> productTypes;
    private ProductTypeResultData selectedProductType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_post);

        commonRepository = new CommonRepository();

        init();
    }

    private void init() {
        loadMaster();

        binding.selectBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SearchPostActivity.this);
                builder.setTitle("แบรนด์");

                String[] items = new String[brands.size()];
                for (int i = 0; i < brands.size(); i++) {
                    items[i] = brands.get(i).getName();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedBrand = brands.get(which);
                        binding.selectBrand.setTextValue("แบรนด์");
                        binding.selectBrand.setTextValue(selectedBrand.getName());
                        showProgressDialog(getString(R.string.loading));
                        commonRepository.getProductType(selectedBrand.getId(), productTypeResponse);
                    }
                });

                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        selectedBrand = null;
                        binding.selectBrand.setTextValue("เลือกแบรนด์");
                        selectedProductType = null;
                        binding.selectType.setTextValue("เลือกประเภท");
                    }
                });

                builder.show();
            }
        });

        binding.selectType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (productTypes == null) {
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(SearchPostActivity.this);
                builder.setTitle("ประเภท");

                String[] items = new String[productTypes.size()];
                for (int i = 0; i < productTypes.size(); i++) {
                    items[i] = productTypes.get(i).getTypeName();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedProductType = productTypes.get(which);
                        binding.selectType.setTextValue(selectedProductType.getTypeName());
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        selectedProductType = null;
                        binding.selectType.setTextValue("เลือกประเภท");
                    }
                });
                builder.show();
            }
        });

        binding.selectProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SearchPostActivity.this);
                builder.setTitle(getString(R.string.province));

                String[] items = new String[provinces.size()];
                for (int i = 0; i < provinces.size(); i++) {
                    items[i] = provinces.get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        setSelectedProvince(which);
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        selectedProvince = null;
                        binding.selectProvince.setTextValue("เลือกจังหวัด");
                        selectedDistrict = null;
                        binding.selectDistrict.setTextValue("เลือกอำเภอ");
                    }
                });
                builder.show();
            }
        });

        binding.selectDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedProvince == null) {
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(SearchPostActivity.this);
                builder.setTitle(getString(R.string.district));

                String[] items = new String[selectedProvince.getDistrict().size()];
                for (int i = 0; i < selectedProvince.getDistrict().size(); i++) {
                    items[i] = selectedProvince.getDistrict().get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedDistrict = selectedProvince.getDistrict().get(which);
                        binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        selectedDistrict = null;
                        binding.selectDistrict.setTextValue("เลือกอำเภอ");
                    }
                });
                builder.show();
            }
        });

        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                intent.putExtra("Brand", selectedBrand);
                intent.putExtra("Type", selectedProductType);
                intent.putExtra("Province", selectedProvince);
                intent.putExtra("District", selectedDistrict);
                if (binding.etMaxPrice.getText().length() > 0) {
                    intent.putExtra("MaxPrice", Integer.parseInt(binding.etMaxPrice.getText().toString()));
                }
                if (binding.etMinPrice.getText().length() > 0) {
                    intent.putExtra("MinPrice", Integer.parseInt(binding.etMinPrice.getText().toString()));
                }
                setResult(Activity.RESULT_OK,intent);
                finish();
            }
        });

    }

    private void setSelectedProvince(int index) {
        selectedProvince = provinces.get(index);
        selectedDistrict = null;
        binding.selectDistrict.setTextValue(getString(R.string.district));
        binding.selectProvince.setTextValue(selectedProvince.getNameTh());
        if (selectedProvince.getDistrict().size() == 0) {
            getDistrict(index);
        }
    }

    private void getDistrict(final int provinceIndex) {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getDistrict(selectedProvince.getId(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                DistrictResultData districtResultData = (DistrictResultData) response;
                selectedProvince.setDistrict(districtResultData.getDistricts());
                provinces.set(provinceIndex, selectedProvince);
                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });
    }

    private void loadMaster() {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getBands(brandHttpCallback);
        commonRepository.getProvinces(provinceHttpCallback);
    }

    private HTTPCallback brandHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            brands = (List<BrandResultData>) response;
            checkSuccessRequest();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private HTTPCallback provinceHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            provinces = (List<ProvinceResultData>) response;
            checkSuccessRequest();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private HTTPCallback productTypeResponse = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            productTypes = (List<ProductTypeResultData>) response;
            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private void checkSuccessRequest() {
        if (brands != null && provinces != null) {
            hideProgressDialog();
        }
    }

}
