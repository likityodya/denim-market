package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class HomeResponse extends BaseResponse {

    @SerializedName("data")
    private HomeResultData homeResultData;

    public HomeResultData gethomeResultData() {
        return homeResultData;
    }
}
