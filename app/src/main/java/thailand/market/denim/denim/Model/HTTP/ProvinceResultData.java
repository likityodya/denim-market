package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Phitakphong on 4/7/2560.
 */

public class ProvinceResultData implements Parcelable {

    @SerializedName("id")
    private int id ;

    @SerializedName("code")
    private String code;

    @SerializedName("name_th")
    private String name_th;

    @SerializedName("name_en")
    private String name_en;

    @SerializedName("geography_id")
    private int geography_id;

    List<DistrictData> district;

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getNameTh() {
        return name_th;
    }

    public String getNameEn() { return name_en; }

    public int getGeographyId() {
        return geography_id;
    }

    public List<DistrictData> getDistrict() {
        if(district == null){
            return new ArrayList<>();
        }
        return district;
    }

    public void setDistrict(List<DistrictData> district) {
        this.district = district;
    }

    protected ProvinceResultData(Parcel in) {
        id = in.readInt();
        code = in.readString();
        name_th = in.readString();
        name_en = in.readString();
        geography_id = in.readInt();
    }

    public static final Parcelable.Creator<ProvinceResultData> CREATOR = new Parcelable.Creator<ProvinceResultData>() {
        @Override
        public ProvinceResultData createFromParcel(Parcel in) {
            return new ProvinceResultData(in);
        }

        @Override
        public ProvinceResultData[] newArray(int size) {
            return new ProvinceResultData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(code);
        dest.writeString(name_th);
        dest.writeString(name_en);
        dest.writeInt(geography_id);
    }
}
