package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.esafirm.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.List;

import thailand.market.denim.denim.Adaptor.DummyImageRecyclerViewAdapter;
import thailand.market.denim.denim.Adaptor.ImageRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentUploadImageBinding;
import thailand.market.denim.denim.databinding.FragmentViewBuyItemBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class UploadImageFragment extends BaseFragment {

    private FragmentUploadImageBinding binding;
    private WrapperFragment wrapperFragment;
    private List<Image> selectedImage;
    private List<Object> images;
    private ImageRecyclerViewAdapter imageAdapter;
    private static final int DEFULT_IMAGE_COUNT = 9;

    public UploadImageFragment() {
        super();
    }

    public static UploadImageFragment newInstance() {
        UploadImageFragment fragment = new UploadImageFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentUploadImageBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
//        if(isFromBackPressed()){
//            return;
//        }
        selectedImage = new ArrayList<>();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        binding.recyclerView.setHasFixedSize(false);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));

        initEvents();

        refreshList();
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });
        binding.vNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedImage.size() < 5) {
                    Util.showToast("กรุณาเลือกรูปอย่างน้อย 5 รูป");
                    return;
                }
                wrapperFragment.onGoNextFragment(SellFragment.newInstance(selectedImage));
            }
        });

    }

    public void addImages(List<Image> images) {
        for (Image image : images) {
            boolean exist = false;
            for (Image img : selectedImage) {
                if (img != null && image.getName().equals(img.getName())) {
                    exist = true;
                    break;
                }
            }
            if (!exist) {
                selectedImage.add(image);
            }
        }
        refreshList();
    }

    public void refreshList() {
        Drawable defaultImg = ContextCompat.getDrawable(getContext(), R.drawable.ico_image);
        images = new ArrayList<>();
        images.add(null);
        for (Image img : selectedImage) {
            images.add(img);
        }

        if (images.size() < DEFULT_IMAGE_COUNT) {
            int c = DEFULT_IMAGE_COUNT - images.size();
            for (int i = 0; i < c ; i++) {
                images.add(defaultImg);
            }
        }

        imageAdapter = new ImageRecyclerViewAdapter(images);
        imageAdapter.setOnLongClickListener(recyclerViewItemClickListener);
        binding.recyclerView.setAdapter(imageAdapter);
    }

    private IRecyclerViewItemClickListener recyclerViewItemClickListener = new IRecyclerViewItemClickListener() {
        @Override
        public void OnItemClick(Object item) {
            IFragmentListener listener = (IFragmentListener) getActivity();
            listener.openSelectImage(Constant.REQUEST_CODE_PICKER_SELL_IMAGE);
        }

        @Override
        public void OnItemLongClick(View view, final Object item) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("ลบรูป");
            builder.setPositiveButton("ยันยัน", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    selectedImage.remove(((int) item) -1);
                    refreshList();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

}
