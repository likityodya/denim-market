package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import thailand.market.denim.denim.databinding.RecyclerLoadingItemLayoutBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class LoadingViewHolder extends RecyclerView.ViewHolder {
    public RecyclerLoadingItemLayoutBinding binding;

    public LoadingViewHolder(RecyclerLoadingItemLayoutBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setLoading(boolean loading) {
        binding.progressBar.setIndeterminate(true);
        if (loading) {
            binding.layoutLoading.setVisibility(View.VISIBLE);
        } else {
            binding.layoutLoading.setVisibility(View.GONE);
        }
    }
}