package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 13/7/2560.
 */

public class ProductDetailResultData implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("price")
    private String price;

    @SerializedName("description")
    private String description;

    @SerializedName("image_path")
    private String image_path;

    @SerializedName("date")
    private String date;

    @SerializedName("type")
    private String type;

    @SerializedName("generation")
    private String generation;

    @SerializedName("brand_name")
    private String brand_name;

    @SerializedName("images")
    private List<String> images;

    @SerializedName("shirts_size_1")
    private List<String> shirt_size;

    @SerializedName("shirts_size_2")
    private List<String> shirt_chest;

    @SerializedName("pants_size_1")
    private List<String> pants_waist;

    @SerializedName("pants_size_2")
    private List<String> pants_length;

    @SerializedName("shoes_uk")
    private List<Double> shoes_uk;

    @SerializedName("shoes_inter")
    private List<Double> shoes_inter;

    @SerializedName("shoes_eu")
    private List<String> shoes_eu;

    public int getId() {
        return id;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getImagePath() {
        return image_path;
    }

    public String getDate() {
        return date;
    }

    public String getType() {
        return type;
    }

    public String getGeneration() {
        return generation;
    }

    public String getBrandName() {
        return brand_name;
    }

    public List<String> getImages() {
        return images;
    }

    public List<String> getShirtSize() {
        return shirt_size;
    }

    public List<String> getShirtChest() {
        return shirt_chest;
    }

    public List<String> getPantsW() {
        return pants_waist;
    }

    public List<String> getPantsL() {
        return pants_length;
    }

    public List<Double> getShoesUk() {
        return shoes_uk;
    }

    public List<Double> getShoesInter() {
        return shoes_inter;
    }

    public List<String> getShoesEU() {
        return shoes_eu;
    }

    public void setShoesEU(List<String> shoesEU) {
        this.shoes_eu = shoesEU;
    }

    public ProductDetailResultData() {
    }

    protected ProductDetailResultData(Parcel in) {
        id = in.readInt();
        price = in.readString();
        description = in.readString();
        image_path = in.readString();
        date = in.readString();
        type = in.readString();
        generation = in.readString();
        brand_name = in.readString();
        in.readStringList(images);
        in.readStringList(shirt_size);
        in.readStringList(shirt_chest);
        in.readList(pants_waist, String.class.getClassLoader());
        in.readList(pants_length, String.class.getClassLoader());
        in.readList(shoes_uk, Double.class.getClassLoader());
        in.readList(shoes_inter, Double.class.getClassLoader());
        in.readList(shoes_eu, String.class.getClassLoader());
    }

    public static final Parcelable.Creator<ProductDetailResultData> CREATOR = new Parcelable.Creator<ProductDetailResultData>() {
        @Override
        public ProductDetailResultData createFromParcel(Parcel in) {
            return new ProductDetailResultData(in);
        }

        @Override
        public ProductDetailResultData[] newArray(int size) {
            return new ProductDetailResultData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(price);
        dest.writeString(description);
        dest.writeString(image_path);
        dest.writeString(date);
        dest.writeString(type);
        dest.writeString(generation);
        dest.writeString(brand_name);
        dest.writeStringList(images);
        dest.writeStringList(shirt_size);
        dest.writeStringList(shirt_chest);
        dest.writeList(pants_waist);
        dest.writeList(pants_length);
        dest.writeList(shoes_uk);
        dest.writeList(shoes_inter);
        dest.writeList(shoes_eu);
    }

}
