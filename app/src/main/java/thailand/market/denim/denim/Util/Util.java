package thailand.market.denim.denim.Util;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inthecheesefactory.thecheeselibrary.manager.Contextor;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import thailand.market.denim.denim.R;

public class Util {

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private static Gson gson;

    public static Gson createGson() {
        if (gson == null) {
            gson = new GsonBuilder()
                    .setDateFormat(Constant.JSON_DATE_TIME_FORMAT)
                    .create();
        }
        return gson;
    }

    public static void showToast(String text) {
        Toast.makeText(Contextor.getInstance().getContext()
                , text
                , Toast.LENGTH_SHORT)
                .show();
    }

    public static String getErrorMessage(ResponseBody errorBody) {
        try {
            JSONObject jsonErrorBody = new JSONObject(errorBody.string());
            return jsonErrorBody.get("OnError").toString();
        } catch (IOException e) {
            return e.getMessage();
        } catch (JSONException e) {
            return e.getMessage();
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }

    public static void loadImage(Context context, String image, ImageView imageView, final ProgressBar progressBar) {
        Glide.with(context)
                .load(image)
                .error(R.drawable.splash)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (progressBar != null) {
                            progressBar.setVisibility(View.GONE);
                        }
                        return false;
                    }
                })
                .into(imageView);
    }

    public static void loadImage(Context context, String image, ImageView imageView) {
        Glide.with(context)
                .load(image)
                .error(R.drawable.splash)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .into(imageView);
    }

    public static void displayMemberType(TextView textView, String userType, Context context) {
        String upperString = "  ";
        if (userType == null) {
            userType = "";
        } else {
            upperString = userType.substring(0, 1).toUpperCase() + userType.substring(1);
        }

        textView.setText(upperString);
        Drawable userTypeDrawable;
        int color;
        switch (userType.toUpperCase()) {
            case "MERCHANT":
                userTypeDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.selector_orange_text_border, null);
                textView.setBackground(userTypeDrawable);
                color = ContextCompat.getColor(context, R.color.orange_text);
                textView.setTextColor(color);
                break;

            case "VERIFIED":
                userTypeDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.selector_green_text_border, null);
                textView.setBackground(userTypeDrawable);
                color = ContextCompat.getColor(context, R.color.green_soft);
                textView.setTextColor(color);
                break;

            default:
                userTypeDrawable = ResourcesCompat.getDrawable(context.getResources(), R.drawable.selector_black_text_border, null);
                textView.setBackground(userTypeDrawable);
                color = ContextCompat.getColor(context, android.R.color.black);
                textView.setTextColor(color);
                break;
        }
    }
}
