package thailand.market.denim.denim.Fragment;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.esafirm.imagepicker.model.Image;

import java.io.File;

import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.ImageResize;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentRegisterMemberBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class RegisterMemberFragment extends BaseFragment {

    private FragmentRegisterMemberBinding binding;
    private WrapperFragment wrapperFragment;
    private Image selectedImage;

    public RegisterMemberFragment() {
        super();
    }

    public static RegisterMemberFragment newInstance() {
        RegisterMemberFragment fragment = new RegisterMemberFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentRegisterMemberBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {

    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        binding.tvPolicy.setPaintFlags(binding.tvPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        initEvents();
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });

        binding.imgUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IFragmentListener listener = (IFragmentListener) getActivity();
                listener.openSelectImage(Constant.REQUEST_CODE_PICKER_REGISTER_MEMBER);
            }
        });

        binding.btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                register();
            }
        });

    }

    private void register() {
        if (selectedImage == null) {
            Util.showToast("กรุณาแนบสำเนาบัตรประชาชนพนร้อมสมุดบัญชีธนาคาร");
            return;
        }

        try {
            showProgressDialog(getString(R.string.loading));
            int userID = PreferenceManager.getInstance().getUserID();
            MemberRepository repository = new MemberRepository();

            ImageResize resize = new ImageResize();
            String resized = resize.resize(selectedImage.getPath());
            File file = new File(resized);
            repository.registerVerify(userID, file, callback);
        } catch (Exception e) {
            Util.showToast(e.getMessage());
        }
    }

    private HTTPCallback callback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            hideProgressDialog();
            Util.showToast(response.toString());
            wrapperFragment.onGoBackFragment();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    public void setSelectedImage(Image image) {
        selectedImage = image;
        Util.loadImage(getContext(), selectedImage.getPath(), binding.imgUpload, null);
        selectedImage = image;
    }

}
