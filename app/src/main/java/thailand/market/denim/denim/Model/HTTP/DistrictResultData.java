package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 3/7/2560.
 */

public class DistrictResultData {


    @SerializedName("amphures")
    private List<DistrictData> districts;

    @SerializedName("province")
    private ProvinceResultData province;

    public List<DistrictData> getDistricts() {
        return districts;
    }

    public ProvinceResultData getProvince() {
        return province;
    }
}