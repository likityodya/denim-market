package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.BrandItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerBrandItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class BrandRecyclerViewAdapter extends RecyclerView.Adapter<BrandItemViewHolder> {

    private IRecyclerViewItemClickListener clickListener;
    private List<BrandResultData> items;
    private LayoutInflater inflater;

    public BrandRecyclerViewAdapter(List<BrandResultData> items) {
        this.items = items;
    }

    @Override
    public BrandItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerBrandItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_brand_item, parent, false);
        return new BrandItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BrandItemViewHolder holder, final int position) {
        final BrandResultData item = items.get(position);
        holder.displayImage(item.getImage());
        if (this.clickListener != null) {
            holder.setOnClickListener(this.clickListener, item);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnClickListener(IRecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }
}