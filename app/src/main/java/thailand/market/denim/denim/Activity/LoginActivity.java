package thailand.market.denim.denim.Activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.LoginResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivityLoginBinding;

public class LoginActivity extends BaseActivity {

    private ActivityLoginBinding binding;
    private MemberRepository memberRepository;
    private String fcmToken;

    public static final int REQUEST_CODE = 103;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        fcmToken = getIntent().getStringExtra("FCMToken");
        initInstances();
    }


    private void initInstances() {
        memberRepository = new MemberRepository();
        //binding.tvRegister.setOnClickListener(onClickListener);
        binding.tvForgotPassword.setPaintFlags(binding.tvForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        binding.btnLogin.setOnClickListener(onLoginClickListener);

        binding.tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }

    private View.OnClickListener onLoginClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (validate()) {

                showProgressDialog(getString(R.string.logging_in));
                memberRepository.login(
                        binding.etUsername.getText().toString().trim()
                        , binding.etPassword.getText().toString()
                        , fcmToken
                        , new HTTPCallback() {
                            @Override
                            public void OnSuccess(Object response) {
                                LoginResultData loginResultData = (LoginResultData) response;
                                PreferenceManager.getInstance().setToken(loginResultData.getToken());
                                PreferenceManager.getInstance().setUserID(loginResultData.getUserID());
                                hideProgressDialog();
                                setResult(Activity.RESULT_OK);
                                finish();
                            }

                            @Override
                            public void OnError(String message) {
                                hideProgressDialog();
                                Util.showToast(message);
                            }
                        });
            }
        }
    };

    private boolean validate() {
        boolean valid = true;
        if (binding.etUsername.getText().toString().trim().length() == 0) {
            binding.etUsername.setError(getString(R.string.email));
            valid = false;
        } else {
            binding.etUsername.setError(null);
        }

        if (binding.etPassword.getText().toString().trim().length() == 0) {
            binding.etPassword.setError(getString(R.string.password));
            valid = false;
        } else {
            binding.etPassword.setError(null);
        }
        return valid;
    }

}
