package thailand.market.denim.denim.ListViewItem;

import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.esafirm.imagepicker.model.Image;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItem2Binding;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItemBinding;

/**
 * Created by Phitakphong on 15/7/2560.
 */

public class ImageItemViewHolder2 extends RecyclerView.ViewHolder {
    private RecyclerPhotoGridItem2Binding binding;

    public ImageItemViewHolder2(RecyclerPhotoGridItem2Binding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void displayImage(String image) {
        Util.loadImage(this.binding.getRoot().getContext(), image, binding.ivImage, binding.progressBar);
    }

    public void setOnLongClickListener(final IRecyclerViewItemClickListener onCardClickListener, final int item) {
        binding.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onCardClickListener.OnItemLongClick(v, item);
                return true;
            }
        });
    }

    public void setOnClickListener(final IRecyclerViewItemClickListener onCardClickListener) {
        binding.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCardClickListener.OnItemClick(null);
            }
        });
    }
}
