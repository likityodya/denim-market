package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class ForumResultData extends BaseResponse {

    @SerializedName("news")
    private List<NewsResultData> news;

    @SerializedName("promotions")
    private List<PromotionResultData> promotions;

    @SerializedName("brands")
    private List<BrandResultData> brands;

    public List<NewsResultData> getNews() {
        return news;
    }

    public List<PromotionResultData> getPromotions() {
        return promotions;
    }

    public List<BrandResultData> getBrands() {
        return brands;
    }
}
