package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 30/5/2560.
 */

public class LoginResultData {
    @SerializedName("token")
    private String token;

    @SerializedName("user_id")
    private int userID;

    public String getToken() {
        return token;
    }

    public int getUserID() {
        return userID;
    }
}
