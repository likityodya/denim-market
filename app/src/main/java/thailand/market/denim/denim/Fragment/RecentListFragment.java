package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Adaptor.FavoritePostRecyclerViewAdapter;
import thailand.market.denim.denim.Adaptor.HistoryRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.FavoritePostData;
import thailand.market.denim.denim.Model.HTTP.HistoryPostData;
import thailand.market.denim.denim.Repository.PostRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentRecentBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class RecentListFragment extends BaseFragment {


    private FragmentRecentBinding binding;
    private PostRepository postRepository;

    public RecentListFragment() {
        super();
    }

    public static RecentListFragment newInstance() {
        RecentListFragment fragment = new RecentListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentRecentBinding.inflate(inflater);
        View rootView = binding.getRoot();
        super.init(rootView, inflater);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        postRepository = new PostRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        showProgress();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        postRepository.getHistory(PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                List<HistoryPostData> dataList = (List<HistoryPostData>) response;
                if (dataList != null) {
                    HistoryRecyclerViewAdapter adapter = new HistoryRecyclerViewAdapter(dataList);
                    adapter.setItemClickListener(new IRecyclerViewItemClickListener() {
                        @Override
                        public void OnItemClick(Object item) {
                            HistoryPostData data = (HistoryPostData) item;
                            IFragmentListener listener = (IFragmentListener) getActivity();
                            listener.openPostItem(data.getId());
                        }

                        @Override
                        public void OnItemLongClick(View view, Object item) {

                        }
                    });
                    binding.recyclerView.setAdapter(adapter);
                }
                hideProgress();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgress();
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }


}
