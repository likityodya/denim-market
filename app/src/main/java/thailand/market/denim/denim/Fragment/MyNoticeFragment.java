package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.design.widget.TabLayout.OnTabSelectedListener;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import thailand.market.denim.denim.Activity.MainActivity;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.PostItemResultData;
import thailand.market.denim.denim.Model.HTTP.PostListResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.PostRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentMyNoticeBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class MyNoticeFragment extends BaseFragment {


    private FragmentMyNoticeBinding binding;
    private FragmentPagerAdapter pagerAdapter;
    private PostRepository postRepository;
    private PostListResultData posts;

    private PostOnlineFragment postOnlineFragment;
    private PostPendingFragment postPendingFragment;
    private PostCloseFragment postCloseFragment;

    int currentTabPosition = -1;

    public MyNoticeFragment() {
        super();
    }

    public static MyNoticeFragment newInstance() {
        MyNoticeFragment fragment = new MyNoticeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentMyNoticeBinding.inflate(inflater);

        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        postRepository = new PostRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {

        binding.layoutSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WrapperFragment wrapperFragment = (WrapperFragment)getParentFragment();
                wrapperFragment.onGoNextFragment(UploadImageFragment.newInstance());
            }
        });

        showProgressDialog(getString(R.string.loading));
        postRepository.getPosts(PreferenceManager.getInstance().getUserID(), httpCallback);
    }


    private HTTPCallback httpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            posts = (PostListResultData) response;

            TabLayout.Tab tab = binding.tabs.newTab();
            tab.setText("ออนไลน์");
            binding.tabs.addTab(tab, 0);

            tab = binding.tabs.newTab();
            tab.setText("รอตรวจสอบ");
            binding.tabs.addTab(tab, 1);

            tab = binding.tabs.newTab();
            tab.setText("ขายเเล้ว");
            binding.tabs.addTab(tab, 2);

            postOnlineFragment = PostOnlineFragment.newInstance(posts.getOnlones());
            postPendingFragment = PostPendingFragment.newInstance(posts.getPendings());
            postCloseFragment = PostCloseFragment.newInstance(posts.getCloses());

            FragmentManager fm = getChildFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.fragment_layout, postOnlineFragment);
            ft.commit();

            binding.tabs.addOnTabSelectedListener(onTabSelectedListener);

            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };
    private HTTPCallback refreshCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            posts = (PostListResultData) response;
            postOnlineFragment.setPosts(posts.getOnlones());
            postPendingFragment.setPosts(posts.getPendings());
            postCloseFragment.setPosts(posts.getCloses());

            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private OnTabSelectedListener onTabSelectedListener = new OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {

            Fragment fragment = null;
            switch (tab.getPosition()) {
                case 0:
                    fragment = postOnlineFragment;
                    break;
                case 1:
                    fragment = postPendingFragment;
                    break;
                case 2:
                    fragment = postCloseFragment;
                    break;
            }

            FragmentManager fm = getChildFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            if (currentTabPosition < tab.getPosition()) {
                ft.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
            } else {
                ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
            }

            ft.replace(R.id.fragment_layout, fragment);
            ft.commit();

            currentTabPosition = tab.getPosition();
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    public void refreshAllList() {
        postRepository.getPosts(PreferenceManager.getInstance().getUserID(), refreshCallback);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    public void markedSell(final PostItemResultData data) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("ขายแล้ว");
        builder.setPositiveButton("ยันยัน", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                showProgressDialog(getString(R.string.loading));
                postRepository.markedSale(data.getId(), PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
                    @Override
                    public void OnSuccess(Object response) {
                        refreshAllList();
                        hideProgressDialog();
                    }

                    @Override
                    public void OnError(String message) {
                        hideProgressDialog();
                        Util.showToast(message);
                    }
                });
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void deletePost(final PostItemResultData data) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("ลบโพสค์");
        builder.setPositiveButton("ยันยัน", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                showProgressDialog(getString(R.string.loading));
                postRepository.delete(data.getId(), PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
                    @Override
                    public void OnSuccess(Object response) {
                        refreshAllList();
                        hideProgressDialog();
                    }

                    @Override
                    public void OnError(String message) {
                        hideProgressDialog();
                        Util.showToast(message);
                    }
                });
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void renewPost(final PostItemResultData data) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("เลื่อนประกาศ");
        builder.setPositiveButton("ยันยัน", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                showProgressDialog(getString(R.string.loading));
                postRepository.renew(data.getId(), PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
                    @Override
                    public void OnSuccess(Object response) {
                        refreshAllList();
                        hideProgressDialog();
                    }

                    @Override
                    public void OnError(String message) {
                        hideProgressDialog();
                        Util.showToast(message);
                    }
                });
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();

    }

    public void editPost(final PostItemResultData data) {
        IFragmentListener listener = (IFragmentListener)getActivity();
        listener.onStartEditSellItem(data.getId());
    }
}
