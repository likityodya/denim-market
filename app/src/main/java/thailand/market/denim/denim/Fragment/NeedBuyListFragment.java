package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import thailand.market.denim.denim.Adaptor.SearchPostRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Interface.IFragmentControlListener;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.DistrictData;
import thailand.market.denim.denim.Model.HTTP.DistrictResultData;
import thailand.market.denim.denim.Model.HTTP.ProductTypeResultData;
import thailand.market.denim.denim.Model.HTTP.ProvinceResultData;
import thailand.market.denim.denim.Model.HTTP.SearchPostListData;
import thailand.market.denim.denim.Model.HTTP.SearchPostListResponse;
import thailand.market.denim.denim.Model.SearchPostItem;
import thailand.market.denim.denim.Model.SearchPostItemContact;
import thailand.market.denim.denim.Model.SearchPostItemLoading;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.CommonRepository;
import thailand.market.denim.denim.Repository.PostRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentNeedBuyListBinding;
import thailand.market.denim.denim.databinding.FragmentViewProfileBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class NeedBuyListFragment extends BaseFragment {

    private FragmentNeedBuyListBinding binding;
    private WrapperFragment wrapperFragment;
    private PostRepository postRepository;
    private SearchPostRecyclerViewAdapter adapter;
    private LinearLayoutManager linearLayoutManager;
    private int pageIndex;
    private int minPrice;
    private int maxPrice;
    private boolean allowNext;
    private boolean isLoading;
    private List<SearchPostItem> items;
    private CommonRepository commonRepository;
    private List<ProvinceResultData> provinceResultDataList;
    private ProvinceResultData selectedProvince;
    private DistrictData selectedDistrict;
    private BrandResultData selectedBrand;
    private ProductTypeResultData selectedType;

    public NeedBuyListFragment() {
        super();
    }

    public static NeedBuyListFragment newInstance() {
        NeedBuyListFragment fragment = new NeedBuyListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentNeedBuyListBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        postRepository = new PostRepository();
        commonRepository = new CommonRepository();
        items = new ArrayList<>();
        clearSearch();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        linearLayoutManager = new LinearLayoutManager(getContext());
        binding.recyclerView.setLayoutManager(linearLayoutManager);

        initEvents();
        showProgressDialog(getString(R.string.loading));
        commonRepository.getProvinces(provinceCallback);

    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });

        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount = linearLayoutManager.getItemCount();
                int lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + 3)) {
                    isLoading = true;
                    loadMore();
                }
            }
        });

        binding.selectProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.province));

                String[] prov = new String[provinceResultDataList.size()];
                for (int i = 0; i < provinceResultDataList.size(); i++) {
                    prov[i] = provinceResultDataList.get(i).getNameTh();
                }

                builder.setItems(prov, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        clearSearch();
                        selectedDistrict = null;
                        binding.selectDistrict.setTextValue(getString(R.string.district));
                        selectedProvince = provinceResultDataList.get(which);
                        binding.selectProvince.setTextValue(selectedProvince.getNameTh());
                        if (selectedProvince.getDistrict().size() == 0) {

                            getDistrict(which);
                        }
                    }
                });
                builder.setNegativeButton("ยกเลิก", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        allowNext = true;
                        pageIndex = 0;
                        items.clear();
                        adapter.notifyDataSetChanged();

                        selectedProvince = null;
                        selectedDistrict = null;
                        selectedBrand = null;
                        selectedType = null;

                        binding.selectProvince.setTextValue(getString(R.string.province));
                        binding.selectDistrict.setTextValue(getString(R.string.district));

                        showProgressDialog(getString(R.string.loading));
                        loadData(refreshCallCallback);
                    }
                });
                builder.show();
            }
        });

        binding.selectDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedProvince == null) {
                    return;
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.district));

                String[] ii = new String[selectedProvince.getDistrict().size()];
                for (int i = 0; i < selectedProvince.getDistrict().size(); i++) {
                    ii[i] = selectedProvince.getDistrict().get(i).getNameTh();
                }

                builder.setItems(ii, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        allowNext = true;
                        pageIndex = 0;
                        items.clear();
                        adapter.notifyDataSetChanged();

                        selectedDistrict = selectedProvince.getDistrict().get(which);
                        binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());

                        showProgressDialog(getString(R.string.loading));
                        loadData(refreshCallCallback);
                    }
                });
                builder.show();
            }
        });
        binding.btnAdvanceSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IFragmentListener listener = (IFragmentListener) getActivity();
                listener.openAdvanceSearchPost();
            }
        });
    }

    private void clearSearch() {
        pageIndex = 0;
        selectedBrand = null;
        selectedType = null;
        selectedProvince = null;
        selectedDistrict = null;
        minPrice = 0;
        maxPrice = 0;
        allowNext = true;
        isLoading = false;
    }

    private void refresh() {
        pageIndex = 0;
        items.clear();
        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
        loadData(refreshCallCallback);
    }

    private void loadMore() {
        if (allowNext) {
            pageIndex++;
            loadData(pagingHttpCallback);
        }
    }

    private void loadData(HTTPCallback callback) {
        postRepository.searchPost(
                pageIndex,
                selectedBrand != null ? selectedBrand.getId() : 0,
                selectedType != null ? selectedType.getTypeId() : 0,
                selectedProvince != null ? selectedProvince.getId() : 0,
                selectedDistrict != null ? selectedDistrict.getId() : 0,
                minPrice,
                maxPrice,
                callback
        );
    }

    private HTTPCallback provinceCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            provinceResultDataList = (List<ProvinceResultData>) response;
            refresh();
        }

        @Override
        public void OnError(String message) {
            Util.showToast(message);
            hideProgressDialog();
        }
    };

    private void getDistrict(final int index) {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getDistrict(selectedProvince.getId(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                DistrictResultData districtResultData = (DistrictResultData) response;
                selectedProvince.setDistrict(districtResultData.getDistricts());
                provinceResultDataList.set(index, selectedProvince);
                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });
    }

    public void advanceSearch(BrandResultData brand, ProductTypeResultData type, ProvinceResultData province, DistrictData district, int maxPrice, int minPrice) {

        items.clear();
        adapter.notifyDataSetChanged();

        binding.selectProvince.setTextValue(getString(R.string.province));
        binding.selectDistrict.setTextValue(getString(R.string.district));

        pageIndex = 0;
        allowNext = true;
        selectedBrand = brand;
        selectedType = type;
        selectedProvince = province;
        selectedDistrict = district;
        this.maxPrice = maxPrice;
        this.minPrice = minPrice;

        showProgressDialog(getString(R.string.loading));
        loadData(refreshCallCallback);
    }

    private HTTPCallback refreshCallCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            SearchPostListResponse resp = (SearchPostListResponse) response;
            keepItem(resp);
            adapter = new SearchPostRecyclerViewAdapter(items);
            adapter.setItemClickListener(new IRecyclerViewItemClickListener() {
                @Override
                public void OnItemClick(Object item) {
                    SearchPostListData data = (SearchPostListData)item;
                    IFragmentListener listener = (IFragmentListener)getActivity();
                    listener.openPostItem(data.getId());
                }

                @Override
                public void OnItemLongClick(View view, Object item) {

                }
            });
            binding.recyclerView.setAdapter(adapter);
            hideProgressDialog();
            isLoading = false;
        }

        @Override
        public void OnError(String message) {
            Util.showToast(message);
            hideProgressDialog();
            isLoading = false;
        }
    };

    private HTTPCallback pagingHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            SearchPostListResponse resp = (SearchPostListResponse) response;
            keepItem(resp);
            adapter.notifyDataSetChanged();
            isLoading = false;
        }

        @Override
        public void OnError(String message) {
            Util.showToast(message);
            isLoading = false;
        }
    };

    private void keepItem(SearchPostListResponse resp) {

        int index = 0;
        if (items == null) {
            items = new ArrayList<>();
        }

        if (items.size() > 1) {
            index = items.size() - 1;
            items.remove(index);
            adapter.notifyItemRemoved(index);
        }

        List<SearchPostListData> pins = resp.getSearchPostData().getItem().getPins();
        List<SearchPostListData> normals = resp.getSearchPostData().getItem().getNormals();
        allowNext = (pins.size() + normals.size()) > 0;

        boolean hasAd = false;
        for (SearchPostListData data : resp.getSearchPostData().getItem().getPins()) {
            items.add(data);
            hasAd = true;
        }
        if (hasAd && resp.getSearchPostData().getItem().getNormals().size() > 0) {
            SearchPostItemContact contact = new SearchPostItemContact();
            items.add(contact);
        }
        for (SearchPostListData data : resp.getSearchPostData().getItem().getNormals()) {
            items.add(data);
        }
        if (allowNext) {
            SearchPostItemLoading loading = new SearchPostItemLoading();
            loading.setLoading(true);
            items.add(loading);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

}
