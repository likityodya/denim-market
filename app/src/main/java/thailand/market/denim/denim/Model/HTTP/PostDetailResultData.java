package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class PostDetailResultData extends BaseResponse {

    @SerializedName("user")
    private ProfileResultData profile;

    @SerializedName("post")
    private PostDetailData post;

    public ProfileResultData getProfile() {
        return profile;
    }

    public PostDetailData getPost() {
        return post;
    }
}
