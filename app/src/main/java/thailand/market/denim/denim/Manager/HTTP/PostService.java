package thailand.market.denim.denim.Manager.HTTP;

import java.util.Map;

import io.reactivex.Flowable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;
import thailand.market.denim.denim.Model.HTTP.BaseResponse;
import thailand.market.denim.denim.Model.HTTP.FavoritePostResponse;
import thailand.market.denim.denim.Model.HTTP.HistoryPostResponse;
import thailand.market.denim.denim.Model.HTTP.LoginResponse;
import thailand.market.denim.denim.Model.HTTP.PostDetailResponse;
import thailand.market.denim.denim.Model.HTTP.PostListResponse;
import thailand.market.denim.denim.Model.HTTP.ProfileResponse;
import thailand.market.denim.denim.Model.HTTP.RegisterResponse;
import thailand.market.denim.denim.Model.HTTP.SearchPostListResponse;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface PostService {

    @Multipart
    @POST("post/create")
    Flowable<Response<BaseResponse>> create(
            @PartMap() Map<String, RequestBody> partMap
            , @Part MultipartBody.Part[] images
    );

    @GET("post/user/{userID}")
    Flowable<Response<PostListResponse>> getPosts(@Path("userID") int userID);

    @GET("post/marked-sale/{postID}/{userID}")
    Flowable<Response<BaseResponse>> markedSale(@Path("postID") int postID, @Path("userID") int userID);

    @GET("post/delete/{postID}/{userID}")
    Flowable<Response<BaseResponse>> deletePost(@Path("postID") int postID, @Path("userID") int userID);

    @GET("favorite/me/{userID}")
    Flowable<Response<FavoritePostResponse>> getFavoritePost(@Path("userID") int userID);

    @GET("post/renew/{postID}/{userID}")
    Flowable<Response<BaseResponse>> renewPost(@Path("postID") int postID, @Path("userID") int userID);

    @GET("post/details/{postID}")
    Flowable<Response<PostDetailResponse>> getPostDetail(@Path("postID") int postID, @Query("visitor_id") int userID);

    @GET("history/me/{userID}")
    Flowable<Response<HistoryPostResponse>> getHistory(@Path("userID") int userID);

    @GET("favorite/remove/{postID}")
    Flowable<Response<BaseResponse>> removeFavorite(@Path("postID") int postID);

    @FormUrlEncoded
    @POST("post/update/{postID}/{userID}")
    Flowable<Response<BaseResponse>> updatePost(
            @Path("postID") int postID,
            @Path("userID") int userID,
            @Field("brand_id") int brandId,
            @Field("type_id") int typeId,
            @Field("generation") String generation,
            @Field("product_type") String productType,
            @Field("price") int price,
            @Field("size") String size,
            @Field("shirts_size_1") String shirts_size_1,
            @Field("pants_size_1") String pants_w,
            @Field("pants_size_2") String pants_l,
            @Field("shoes_size_eu") String shoes_size_eu,
            @Field("shoes_size_uk") String shoes_size_uk,
            @Field("shoes_size_us") String shoes_size_us,
            @Field("description") String description,
            @Field("province_id") int provinceId,
            @Field("district_id") int districtId,
            @Field("phone") String phone
    );

    @FormUrlEncoded
    @POST("post/search/{pageIndex}")
    Flowable<Response<SearchPostListResponse>> searchPost(
            @Path("pageIndex") int pageIndex,
            @Field("brand_id") int brandId,
            @Field("type_id") int typeId,
            @Field("province_id") int provinceId,
            @Field("district_id") int districtId,
            @Field("min_price") int minPrice,
            @Field("max_price") int maxPrice
    );

    @FormUrlEncoded
    @POST("favorite/save")
    Flowable<Response<BaseResponse>> addFavorite(
            @Field("user_id") int userId,
            @Field("post_id") int postId
    );


    @GET("post/image/delete/{postID}/{imageName}")
    Flowable<Response<BaseResponse>> deletePostImage(@Path("postID") int postID, @Path("imageName") String imageName);

    @Multipart
    @POST("post/upload-image/{postID}")
    Flowable<Response<BaseResponse>> uploadPostImage(@Path("postID") int postID, @Part MultipartBody.Part[] images);

}
