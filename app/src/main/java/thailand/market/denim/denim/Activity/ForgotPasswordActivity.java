package thailand.market.denim.denim.Activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;

import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.ActivityForgotPasswordBinding;

public class ForgotPasswordActivity extends BaseActivity {

    ActivityForgotPasswordBinding binding;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_forgot_password);

        initInstance();
        initEvents();
    }
    private void initInstance(){

    }

    private void initEvents(){
        binding.btnForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressDialog(getString(R.string.loading));

                MemberRepository memberRepository = new MemberRepository();
                memberRepository.forgotPassword(binding.etEmail.getText().toString(), new HTTPCallback() {
                    @Override
                    public void OnSuccess(Object response) {
                        Util.showToast(response.toString());
                        hideProgressDialog();
                        onBackPressed();
                    }

                    @Override
                    public void OnError(String message) {
                        hideProgressDialog();
                        Util.showToast(message);
                    }
                });
            }
        });

        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
