package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 13/7/2560.
 */

public class PromotionResultData {

    @SerializedName("description")
    private String description;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }
}
