package thailand.market.denim.denim.Manager.HTTP;

import io.reactivex.Flowable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import thailand.market.denim.denim.Model.HTTP.BaseResponse;
import thailand.market.denim.denim.Model.HTTP.DistrictResponse;
import thailand.market.denim.denim.Model.HTTP.ForumResponse;
import thailand.market.denim.denim.Model.HTTP.HomeResponse;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResponse;
import thailand.market.denim.denim.Model.HTTP.ProductDetailResponse;
import thailand.market.denim.denim.Model.HTTP.ProvinceResponse;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface ForumService {

    @GET("forum/home")
    Flowable<Response<ForumResponse>> forum();

    @GET("forum/brand/{brandID}/product")
    Flowable<Response<ProductBrandResponse>> getProduct(@Path("brandID") int brandID);

    @GET("forum/product/{productID}")
    Flowable<Response<ProductDetailResponse>> getProductDetail(@Path("productID") int productID);

    @FormUrlEncoded
    @POST("order/send/user/{userID}/product/{productID}")
    Flowable<Response<BaseResponse>> confirmOrder(
            @Path("userID") int userID,
            @Path("productID") int productID,
            @Field("user_id") int userID1,
            @Field("quantity") int quantity,
            @Field("price") String price,
            @Field("first_name") String first_name,
            @Field("last_name") String last_name,
            @Field("address") String address,
            @Field("phone") String phone,
            @Field("province_id") int province_id,
            @Field("district_id") int district_id,
            @Field("zipcode") String zipcode,
            @Field("shirts_size_1") String shirts_size_1,
            @Field("shirts_size_2") String shirts_size_2,
            @Field("pants_l") String pants_size_1,
            @Field("pants_w") String pants_size_2,
            @Field("shoes_inter") String shoes_inter,
            @Field("shoes_uk") String shoes_uk,
            @Field("shoes_eu") String shoes_eu
    );
}
