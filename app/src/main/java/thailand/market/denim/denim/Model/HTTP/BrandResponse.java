package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class BrandResponse extends BaseResponse {

    @SerializedName("data")
    private List<BrandResultData> brandResultDataList;

    public List<BrandResultData> getBrandResultData() {
        return brandResultDataList;
    }
}
