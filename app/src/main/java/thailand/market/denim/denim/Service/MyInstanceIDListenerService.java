package thailand.market.denim.denim.Service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.greenrobot.eventbus.EventBus;

import thailand.market.denim.denim.Util.MessageEvent;


/**
 * Created by chawdee on 18/06/2016.
 */
public class MyInstanceIDListenerService extends FirebaseInstanceIdService {

	/**
	 * Called if InstanceID token is updated. This may occur if the security of
	 * the previous token had been compromised. Note that this is also called
	 * when the InstanceID token is initially generated, so this is where
	 * you retrieve the token.
	 */
	// [START refresh_token]
	@Override
	public void onTokenRefresh() {
		// Get updated InstanceID token.
		String refreshedToken = FirebaseInstanceId.getInstance().getToken();

		// Post get new token to LoginFragment
		EventBus.getDefault().post(new MessageEvent(MessageEvent.ResultType.SUCCESS,
				MessageEvent.ActivityType.REFRESH_TOKEN,
				refreshedToken));

	}

}
