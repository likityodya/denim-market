package thailand.market.denim.denim.Manager.HTTP;

import io.reactivex.Flowable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import thailand.market.denim.denim.Model.HTTP.BrandResponse;
import thailand.market.denim.denim.Model.HTTP.DistrictResponse;
import thailand.market.denim.denim.Model.HTTP.DistrictResultData;
import thailand.market.denim.denim.Model.HTTP.HomeResponse;
import thailand.market.denim.denim.Model.HTTP.LoginResponse;
import thailand.market.denim.denim.Model.HTTP.ProductTypeResponse;
import thailand.market.denim.denim.Model.HTTP.ProvinceResponse;
import thailand.market.denim.denim.Model.HTTP.ProvinceResultData;
import thailand.market.denim.denim.Model.HTTP.RegisterResponse;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public interface CommonService {

    @GET("home")
    Flowable<Response<HomeResponse>> home();

    @GET("provinces")
    Flowable<Response<ProvinceResponse>> getProvinces();

    @GET("amphures/{provinceID}")
    Flowable<Response<DistrictResponse>> getDistricts(@Path("provinceID") int provinceID);

    @GET("brand")
    Flowable<Response<BrandResponse>> getBrands();

    @GET("type-of-brand/{brandID}")
    Flowable<Response<ProductTypeResponse>> getProductType(@Path("brandID") int brandID);


}
