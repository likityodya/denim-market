package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.PostClosedItemViewHolder;
import thailand.market.denim.denim.ListViewItem.PostItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.PostItemResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerPostClosedItemBinding;
import thailand.market.denim.denim.databinding.RecyclerPostItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class PostClosedRecyclerViewAdapter extends RecyclerView.Adapter<PostClosedItemViewHolder> {

    private List<PostItemResultData> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener editClickListener;
    private IRecyclerViewItemClickListener deleteClickListener;

    public PostClosedRecyclerViewAdapter(List<PostItemResultData> items) {
        this.items = items;
    }

    @Override
    public PostClosedItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerPostClosedItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_post_closed_item, parent, false);
        return new PostClosedItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PostClosedItemViewHolder holder, final int position) {
        final PostItemResultData item = items.get(position);
        holder.displayImage(item.getImage());

        if (editClickListener != null) {
            holder.setEditClickListener(editClickListener, item);
        }
        if (deleteClickListener != null) {
            holder.setDeleteClickListener(deleteClickListener, item);
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setEditClickListener(IRecyclerViewItemClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public void setDeleteClickListener(IRecyclerViewItemClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }
}