package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerSearchPostContactBinding;
import thailand.market.denim.denim.databinding.RecyclerSearchPostItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class SearchPostItemContactViewHolder extends RecyclerView.ViewHolder {
    public RecyclerSearchPostContactBinding binding;

    public SearchPostItemContactViewHolder(RecyclerSearchPostContactBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        binding.btnContact.setOnClickListener(onClickListener);
    }
}
