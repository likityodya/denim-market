package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.DTO.ConfirmOrderDto;
import thailand.market.denim.denim.Model.HTTP.DistrictData;
import thailand.market.denim.denim.Model.HTTP.DistrictResultData;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResultData;
import thailand.market.denim.denim.Model.HTTP.ProductDetailResultData;
import thailand.market.denim.denim.Model.HTTP.ProvinceResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.CommonRepository;
import thailand.market.denim.denim.Repository.ForumRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentConfirmBuyBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ConfirmBuyItemFragment extends BaseFragment {

    private FragmentConfirmBuyBinding binding;
    private WrapperFragment wrapperFragment;
    private ConfirmOrderDto orderDto;
    private ProductDetailResultData productDetail;
    private ProductBrandResultData product;
    private CommonRepository commonRepository;
    private ForumRepository forumRepository;
    private List<ProvinceResultData> provinces;
    private ProvinceResultData selectedProvince;
    private DistrictData selectedDistrict;


    public ConfirmBuyItemFragment() {
        super();
    }

    public static ConfirmBuyItemFragment newInstance(ProductBrandResultData product, ConfirmOrderDto orderDto, ProductDetailResultData productDetail) {
        ConfirmBuyItemFragment fragment = new ConfirmBuyItemFragment();
        Bundle args = new Bundle();
        args.putParcelable("ProductBrandResultData", product);
        args.putParcelable("ConfirmOrderDto", orderDto);
        args.putParcelable("ProductDetailResultData", productDetail);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentConfirmBuyBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        orderDto = getArguments().getParcelable("ConfirmOrderDto");
        productDetail = getArguments().getParcelable("ProductDetailResultData");
        product = getArguments().getParcelable("ProductBrandResultData");

        commonRepository = new CommonRepository();
        forumRepository = new ForumRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        commonRepository.getProvinces(provinceHttpCallback);

        wrapperFragment = (WrapperFragment) getParentFragment();
        Util.loadImage(getContext(), product.getImage(), binding.imageView, binding.progressBar);
        binding.tvBrand.setText(productDetail.getBrandName());
        binding.tvGeneration.setText(productDetail.getGeneration());
        binding.tvPrice.setText(productDetail.getPrice());
        String strSize = "";
        switch (productDetail.getType()) {
            case "pants":
                strSize = orderDto.getPantsW();
                strSize += "/" + orderDto.getPantsSize();
                break;
            case "shirt":
                strSize = orderDto.getShirtSize();
                break;
            case "shoes":
                strSize = "UK - " + orderDto.getShoesUk();
                strSize += " US - " + orderDto.getShoesUS();
                strSize += " EU - " + orderDto.getShoesEU();

                break;
        }
        binding.tvSize.setText(strSize);
        binding.etQty.setText(String.valueOf(orderDto.getQuantity()));

        binding.etFirstName.setText(orderDto.getFirstName());
        binding.etLastName.setText(orderDto.getLastName());
        binding.etAddress.setText(orderDto.getAddress());
        binding.etTel.setText(orderDto.getPhone());
        binding.etZipcode.setText(orderDto.getZipcode());
        initEvents();
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    showProgressDialog(getString(R.string.loading));
                    int userID = PreferenceManager.getInstance().getUserID();
                    forumRepository.confirmOrder(
                            userID
                            , productDetail.getId()
                            , userID
                            , Integer.parseInt(binding.etQty.getText().toString())
                            , productDetail.getPrice().replace(",", "")
                            , binding.etFirstName.getText().toString()
                            , binding.etLastName.getText().toString()
                            , binding.etAddress.getText().toString()
                            , binding.etTel.getText().toString()
                            , selectedProvince.getId()
                            , selectedDistrict.getId()
                            , binding.etZipcode.getText().toString()
                            , orderDto.getShirtSize()
                            , orderDto.getShirtChest()
                            , orderDto.getPantsSize()
                            , String.valueOf(orderDto.getPantsW())
                            , String.valueOf(orderDto.getShoesUS())
                            , String.valueOf(orderDto.getShoesUk())
                            , orderDto.getShoesEU()
                            , saveHttpCallback
                    );
                }
            }
        });
        binding.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = Integer.parseInt(binding.etQty.getText().toString());
                current++;
                binding.etQty.setText(String.valueOf(current));
            }
        });
        binding.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int current = Integer.parseInt(binding.etQty.getText().toString());
                current--;
                if (current < 1) {
                    current = 1;
                }
                binding.etQty.setText(String.valueOf(current));
            }
        });
        binding.selectProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.province));

                String[] items = new String[provinces.size()];
                for (int i = 0; i < provinces.size(); i++) {
                    items[i] = provinces.get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        setSelectedProvince(which);
                    }
                });
                builder.show();
            }
        });

        binding.selectDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedProvince == null) {
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.district));

                String[] items = new String[selectedProvince.getDistrict().size()];
                for (int i = 0; i < selectedProvince.getDistrict().size(); i++) {
                    items[i] = selectedProvince.getDistrict().get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedDistrict = selectedProvince.getDistrict().get(which);
                        binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());
                    }
                });
                builder.show();
            }
        });
    }

    private void getDistrict(final int provinceIndex) {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getDistrict(selectedProvince.getId(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                DistrictResultData districtResultData = (DistrictResultData) response;
                selectedProvince.setDistrict(districtResultData.getDistricts());
                provinces.set(provinceIndex, selectedProvince);
                if (selectedDistrict == null && orderDto.getDistrictId() != 0) {
                    for (DistrictData district : districtResultData.getDistricts()) {
                        if (district.getId() == orderDto.getDistrictId()) {
                            selectedDistrict = district;
                            binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());
                            break;
                        }
                    }
                }
                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    private boolean validate() {
        boolean valid = true;

        binding.etQty.setError(null);
        binding.etFirstName.setError(null);
        binding.etLastName.setError(null);
        binding.etAddress.setError(null);
        binding.etTel.setError(null);
        binding.selectProvince.setError(null);
        binding.selectDistrict.setError(null);
        binding.etZipcode.setError(null);

        if (binding.etQty.getText().length() == 0) {
            binding.etQty.setError("จำนวน");
            valid = false;
        }
        if (binding.etFirstName.getText().length() == 0) {
            binding.etFirstName.setError("ชื่อ");
            valid = false;
        }
        if (binding.etLastName.getText().length() == 0) {
            binding.etLastName.setError("นามสกุล");
            valid = false;
        }
        if (binding.etAddress.getText().length() == 0) {
            binding.etAddress.setError("ที่อยุ่");
            valid = false;
        }
        if (binding.etTel.getText().length() == 0) {
            binding.etTel.setError("เบอร์โทร");
            valid = false;
        }
        if (binding.etAddress.getText().length() == 0) {
            binding.etAddress.setError("ที่อยุ่");
            valid = false;
        }
        if (binding.etTel.getText().length() == 0) {
            binding.etTel.setError("เบอร์โทร");
            valid = false;
        }
        if (selectedProvince == null) {
            binding.selectProvince.setError("จังหวัด");
            valid = false;
        }
        if (selectedDistrict == null) {
            binding.selectDistrict.setError("อำเภอ");
            valid = false;
        }
        if (binding.etZipcode.getText().length() == 0) {
            binding.etZipcode.setError("รหัสไปรษณีย์");
            valid = false;
        }
        return valid;
    }

    private HTTPCallback provinceHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            provinces = (List<ProvinceResultData>) response;
            int index = 0;
            if (orderDto.getProvinceId() != 0) {
                for (ProvinceResultData province : provinces) {
                    if (province.getId() == orderDto.getProvinceId()) {
                        setSelectedProvince(index);
                        break;
                    }
                    index++;
                }
            }
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };
    private HTTPCallback saveHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            hideProgressDialog();
            Util.showToast(response.toString());
            wrapperFragment.onGoBackFragment();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private void setSelectedProvince(int index) {
        selectedProvince = provinces.get(index);
        selectedDistrict = null;
        binding.selectDistrict.setTextValue(getString(R.string.district));
        binding.selectProvince.setTextValue(selectedProvince.getNameTh());
        if (selectedProvince.getDistrict().size() == 0) {
            getDistrict(index);
        }
    }
}
