package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import thailand.market.denim.denim.Adaptor.ProductRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.ForumRepository;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentProductBrandBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ProductBrandFragment extends BaseFragment {

    private FragmentProductBrandBinding binding;
    private WrapperFragment wrapperFragment;
    private BrandResultData brand;
    private ForumRepository forumRepository;
    private List<ProductBrandResultData> products;
    private ProductRecyclerViewAdapter adapter;

    public ProductBrandFragment() {
        super();
    }

    public static ProductBrandFragment newInstance(BrandResultData brand) {
        ProductBrandFragment fragment = new ProductBrandFragment();
        Bundle args = new Bundle();
        args.putParcelable("BrandResultData", brand);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProductBrandBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        brand = getArguments().getParcelable("BrandResultData");
        forumRepository = new ForumRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        showProgressDialog(getString(R.string.loading));
        binding.recyclerProduct.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.tvTitle.setText(brand.getName());
        forumRepository.getProduct(brand.getId(), productCallback);

        initEvents();
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    private HTTPCallback productCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            if(response == null){
                products = new ArrayList<>();
            }else {
                products = (List<ProductBrandResultData>)response;
            }
            adapter = new ProductRecyclerViewAdapter(products);
            adapter.setOnClickListener(clickListener);
            binding.recyclerProduct.setAdapter(adapter);
            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };
    private IRecyclerViewItemClickListener clickListener = new IRecyclerViewItemClickListener() {
        @Override
        public void OnItemClick(Object item) {
            ProductBrandResultData data = (ProductBrandResultData)item;
            wrapperFragment.onGoNextFragment(ProductBrandDetailFragment.newInstance(data));
        }

        @Override
        public void OnItemLongClick(View view, Object item) {

        }
    };
}
