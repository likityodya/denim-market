package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.esafirm.imagepicker.model.Image;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.ImageItemViewHolder;
import thailand.market.denim.denim.ListViewItem.ImageItemViewHolder2;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItem2Binding;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class ImageRecyclerViewAdapter2 extends RecyclerView.Adapter<ImageItemViewHolder2> {

    private List<String> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener onCardClickListener;

    public ImageRecyclerViewAdapter2(List<String> items) {
        this.items = items;
    }

    @Override
    public ImageItemViewHolder2 onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerPhotoGridItem2Binding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_photo_grid_item_2, parent, false);
        return new ImageItemViewHolder2(binding);
    }

    @Override
    public void onBindViewHolder(ImageItemViewHolder2 holder, final int position) {
        String item = items.get(position);
        holder.displayImage(item);
        if (onCardClickListener != null) {
            holder.setOnClickListener(onCardClickListener);
            holder.setOnLongClickListener(onCardClickListener, position);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setClickListener(IRecyclerViewItemClickListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

}