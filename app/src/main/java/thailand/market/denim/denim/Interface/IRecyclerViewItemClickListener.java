package thailand.market.denim.denim.Interface;

import android.view.View;

import com.esafirm.imagepicker.model.Image;

/**
 * Created by Phitakphong on 13/7/2560.
 */

public interface IRecyclerViewItemClickListener {
    void OnItemClick(Object item);
    void OnItemLongClick(View view, Object item);
}
