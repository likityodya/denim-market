package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Interface.IFragmentControlListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.DistrictData;
import thailand.market.denim.denim.Model.HTTP.DistrictResultData;
import thailand.market.denim.denim.Model.HTTP.ProductTypeResultData;
import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.Model.HTTP.ProvinceResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.CommonRepository;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Constant;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentWantedBinding;
import thailand.market.denim.denim.databinding.FragmentWantedRequestBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class WantedRequestFragment extends BaseFragment {


    private FragmentWantedRequestBinding binding;
    private CommonRepository commonRepository;
    private List<BrandResultData> brands;
    private BrandResultData selectedBrand;
    private List<ProvinceResultData> provinces;
    private ProvinceResultData selectedProvince;
    private DistrictData selectedDistrict;
    private List<ProductTypeResultData> productTypes;
    private ProductTypeResultData selectedProductType;
    private ProfileResultData profile;
    private MemberRepository memberRepository;

    private String selectedShirtSize;
    private String selectedShoesEU;
    private String selectedShoesUS;
    private String selectedShoesUK;
    private String selectedPantsW;
    private String selectedPantsSize;

    public WantedRequestFragment() {
        super();
    }

    public static WantedRequestFragment newInstance() {
        WantedRequestFragment fragment = new WantedRequestFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentWantedRequestBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        commonRepository = new CommonRepository();
        memberRepository = new MemberRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        loadMaster();
        initEvents();
    }

    private void loadMaster() {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getBands(brandHttpCallback);
        memberRepository.getProfile(PreferenceManager.getInstance().getUserID(), userCallback);

    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    showProgressDialog(getString(R.string.loading));
                    memberRepository.lookForItem(
                            PreferenceManager.getInstance().getUserID()
                            , selectedBrand.getId()
                            , selectedProductType.getTypeId()
                            , binding.rdoNew.isChecked() ? "new" : "old"
                            , Integer.parseInt(binding.etMinPrice.getText().toString())
                            , Integer.parseInt(binding.etMaxPrice.getText().toString())
                            , selectedProvince.getId()
                            , selectedDistrict.getId()
                            , selectedShirtSize
                            , selectedPantsW
                            , selectedPantsSize
                            , selectedShoesEU
                            , selectedShoesUS
                            , selectedShoesUK
                            , new HTTPCallback() {
                                @Override
                                public void OnSuccess(Object response) {
                                    Util.showToast(response.toString());
                                    hideProgressDialog();
                                    goBack();
                                }

                                @Override
                                public void OnError(String message) {
                                    hideProgressDialog();
                                    Util.showToast(message);
                                }
                            }
                    );


                }
            }
        });

        binding.selectBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("แบรนด์");

                String[] items = new String[brands.size()];
                for (int i = 0; i < brands.size(); i++) {
                    items[i] = brands.get(i).getName();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedBrand = brands.get(which);
                        binding.selectBrand.setTextValue("แบรนด์");
                        binding.selectBrand.setTextValue(selectedBrand.getName());
                        loadType(selectedBrand.getId());
                    }
                });
                builder.show();
            }
        });
        binding.selectType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (productTypes == null) {
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ประเภท");

                String[] items = new String[productTypes.size()];
                for (int i = 0; i < productTypes.size(); i++) {
                    items[i] = productTypes.get(i).getTypeName();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedProductType = productTypes.get(which);
                        binding.selectType.setTextValue(selectedProductType.getTypeName());

                        clearType();

                        switch (selectedProductType.getSizeOption()) {
                            case "pants":
                                binding.layoutPants.setVisibility(View.VISIBLE);
                                break;
                            case "shirts":
                                binding.layoutShirts.setVisibility(View.VISIBLE);
                                break;
                            case "shoes":
                                binding.layoutShoes.setVisibility(View.VISIBLE);
                                break;
                            default:
                                binding.layoutOther.setVisibility(View.VISIBLE);
                                break;
                        }

                    }
                });
                builder.show();
            }
        });
        binding.selectProvince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.province));

                String[] items = new String[provinces.size()];
                for (int i = 0; i < provinces.size(); i++) {
                    items[i] = provinces.get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        setSelectedProvince(which);
                    }
                });
                builder.show();
            }
        });

        binding.selectDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (selectedProvince == null) {
                    return;
                }

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(getString(R.string.district));

                String[] items = new String[selectedProvince.getDistrict().size()];
                for (int i = 0; i < selectedProvince.getDistrict().size(); i++) {
                    items[i] = selectedProvince.getDistrict().get(i).getNameTh();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedDistrict = selectedProvince.getDistrict().get(which);
                        binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());
                    }
                });
                builder.show();
            }
        });

        binding.selectSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("รอบอก CHEST");

                final String[] items = Constant.SHIRT_SIZE;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShirtSize = items[which];
                        binding.selectSize.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });


        binding.selectUkSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ไซค์ UK");

                final String[] items = Constant.SHOES_UK;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShoesUK = items[which];
                        binding.selectUkSize.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });

        binding.selectSizeUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ไซค์ US");

                final String[] items = Constant.SHOES_US;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShoesUS = items[which];
                        binding.selectSizeUs.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });

        binding.selectSizeEu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ไซค์ EU");

                final String[] items = Constant.SHOES_EU;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedShoesEU = items[which];
                        binding.selectSizeEu.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });

        binding.selectWaist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("รอบเอว WAIST");

                final String[] items = Constant.PANT_WAIST;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedPantsW = items[which];
                        binding.selectWaist.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });
        binding.selectLength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ความยาว LENGTH");

                final String[] items = Constant.PANT_LENGTH;

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        selectedPantsSize = items[which];
                        binding.selectLength.setTextValue(items[which]);
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    private void goBack() {
        WrapperFragment wrapperFragment = (WrapperFragment) getParentFragment();
        wrapperFragment.onGoBackFragment();
    }

    private boolean validate() {
        boolean valid = true;

        if (selectedBrand == null) {
            binding.selectBrand.setError("แบรนด์");
            valid = false;
        } else {
            binding.selectBrand.setError(null);
        }

        if (selectedProductType == null) {
            binding.selectType.setError("ประเภท");
            valid = false;
        } else {
            binding.selectType.setError(null);
        }

        if (!binding.rdoNew.isChecked() && !binding.rdoOld.isChecked()) {
            binding.rdoNew.setError("ประเภท");
            binding.rdoOld.setError("ประเภท");
            valid = false;
        } else {
            binding.rdoNew.setError(null);
            binding.rdoOld.setError(null);
        }


        if (binding.etMinPrice.getText().length() == 0) {
            binding.etMinPrice.setError("ราคาต่ำสุด");
            valid = false;
        } else {
            binding.etMinPrice.setError(null);
        }

        if (binding.etMaxPrice.getText().length() == 0) {
            binding.etMaxPrice.setError("ราคาสูงสุด");
            valid = false;
        } else {
            binding.etMaxPrice.setError(null);
        }

        if (selectedProvince == null) {
            binding.selectProvince.setError("เลือกจังหวัด");
            valid = false;
        } else {
            binding.selectProvince.setError(null);
        }

        if (selectedDistrict == null) {
            binding.selectDistrict.setError("เลือกอำเภอ");
            valid = false;
        } else {
            binding.selectDistrict.setError(null);
        }

        if (selectedProductType == null) {
            binding.selectType.setError("ประเภท");
            valid = false;
        } else {
            binding.selectType.setError(null);
        }

        if (selectedProductType != null) {

            binding.selectWaist.setError(null);
            binding.selectLength.setError(null);
            binding.selectSize.setError(null);
            binding.selectUkSize.setError(null);
            binding.selectSizeUs.setError(null);
            binding.etOtherSize.setError(null);

            switch (selectedProductType.getSizeOption()) {
                case "pants":
                    if (selectedPantsSize == null) {
                        binding.selectLength.setError("เลือกรอบเอว WAIST");
                        valid = false;
                    } else {
                        binding.selectLength.setError(null);
                    }
                    if (selectedPantsW == null) {
                        binding.selectWaist.setError("เลือกความยาว LENGTH");
                        valid = false;
                    } else {
                        binding.selectWaist.setError(null);
                    }
                    break;
                case "shirts":
                    if (selectedShirtSize == null) {
                        binding.selectSize.setError("เลือกรอบอก CHEST");
                        valid = false;
                    } else {
                        binding.selectSize.setError(null);
                    }
                    break;
                case "shoes":
                    if (selectedShoesUK == null) {
                        binding.selectUkSize.setError("เลือกไซต์ UK");
                        valid = false;
                    } else {
                        binding.selectUkSize.setError(null);
                    }
                    if (selectedShoesUS == null) {
                        binding.selectSizeUs.setError("เลือกไซต์ US");
                        valid = false;
                    } else {
                        binding.selectSizeUs.setError(null);
                    }
                    if (selectedShoesEU == null) {
                        binding.selectSizeEu.setError("เลือกไซต์ EU");
                        valid = false;
                    } else {
                        binding.selectSizeEu.setError(null);
                    }
                    break;
                default:
                    if (binding.etOtherSize.getText().length() == 0) {
                        binding.etOtherSize.setError("Size");
                        valid = false;
                    }
                    break;
            }
        }

        return valid;
    }

    private HTTPCallback brandHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            brands = (List<BrandResultData>) response;
            checkSuccessRequest();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private HTTPCallback provinceHttpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            provinces = (List<ProvinceResultData>) response;
            int index = 0;
            if (profile.getProvince() != null) {
                for (ProvinceResultData province : provinces) {
                    if (province.getId() == profile.getProvince().getId()) {
                        setSelectedProvince(index);
                        break;
                    }
                    index++;
                }
            }
            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private HTTPCallback userCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            profile = (ProfileResultData) response;
            checkSuccessRequest();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };
    private HTTPCallback productTypeResponse = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            productTypes = (List<ProductTypeResultData>) response;
            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private void checkSuccessRequest() {
        if (brands != null && profile != null) {
            commonRepository.getProvinces(provinceHttpCallback);
        }
    }

    private void setSelectedProvince(int index) {
        selectedProvince = provinces.get(index);
        selectedDistrict = null;
        binding.selectDistrict.setTextValue(getString(R.string.district));
        binding.selectProvince.setTextValue(selectedProvince.getNameTh());
        if (selectedProvince.getDistrict().size() == 0) {
            getDistrict(index);
        }
    }

    private void getDistrict(final int provinceIndex) {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getDistrict(selectedProvince.getId(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                DistrictResultData districtResultData = (DistrictResultData) response;
                selectedProvince.setDistrict(districtResultData.getDistricts());
                provinces.set(provinceIndex, selectedProvince);
                if (selectedDistrict == null && profile.getDistrict() != null) {
                    for (DistrictData district : districtResultData.getDistricts()) {
                        if (district.getId() == profile.getDistrict().getId()) {
                            selectedDistrict = district;
                            binding.selectDistrict.setTextValue(selectedDistrict.getNameTh());
                            break;
                        }
                    }
                }
                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });
    }

    private void loadType(int brandUD) {
        showProgressDialog(getString(R.string.loading));
        commonRepository.getProductType(brandUD, productTypeResponse);
    }

    private void clearType() {
        binding.layoutPants.setVisibility(View.GONE);
        binding.layoutShirts.setVisibility(View.GONE);
        binding.layoutShoes.setVisibility(View.GONE);
        binding.layoutOther.setVisibility(View.GONE);

        binding.selectSize.setTextValue("เลือกรอบอก CHEST");

        binding.selectSizeEu.setTextValue("เลือกไซต์ EU");
        binding.selectUkSize.setTextValue("เลือกไซต์ UK");
        binding.selectSizeUs.setTextValue("เลือกไซต์ US");

        binding.selectWaist.setTextValue("เลือกรอบเอว WAIST");
        binding.selectLength.setTextValue("เลือกความยาว LENGTH");

        binding.etOtherSize.setText(null);

        selectedShirtSize = null;

        selectedShoesEU = null;
        selectedShoesUS = null;
        selectedShoesUK = null;

        selectedPantsW = null;
        selectedPantsSize = null;
    }

}
