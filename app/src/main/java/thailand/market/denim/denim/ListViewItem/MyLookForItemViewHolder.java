package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerLookForItemBinding;
import thailand.market.denim.denim.databinding.RecyclerPurchaseItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class MyLookForItemViewHolder extends RecyclerView.ViewHolder {
    public RecyclerLookForItemBinding binding;

    public MyLookForItemViewHolder(RecyclerLookForItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setLine1(String line1){
        binding.tvLine1.setText(line1);
    }
    public void setLine2(String line2){
        binding.tvLine2.setText(line2);
    }
    public void setLine3(String line3){
        binding.tvLine3.setText(line3);
    }
    public View getLayoutItem() {
        return binding.layout;
    }
}
