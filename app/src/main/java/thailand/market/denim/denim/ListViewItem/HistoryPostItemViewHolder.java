package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerFavoritePostItemBinding;
import thailand.market.denim.denim.databinding.RecyclerHistoryPostItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class HistoryPostItemViewHolder extends RecyclerView.ViewHolder {
    public RecyclerHistoryPostItemBinding binding;

    public HistoryPostItemViewHolder(RecyclerHistoryPostItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void displayImage(String imageUrl) {
        Util.loadImage(binding.getRoot().getContext(), imageUrl, binding.imageView, binding.progressBar);
    }

    public void setProductType(String productType) {
        if (productType.equals("new")) {
            binding.tvProductType.setText("สินค้าใหม่");
        } else {
            binding.tvProductType.setText("สินค้ามือสอง");
        }
    }

    public void setBrand(String brand) {
        binding.tvBrand.setText(brand);
    }

    public void setGeneration(String generation) {
        binding.tvGeneration.setText(generation);
    }

    public void setSize(String size) {
        binding.tvSize.setText(size);
    }

    public void setType(String type) {
        binding.tvType.setText(type);
    }

    public void setPrice(String price) {
        binding.tvPrice.setText(price);
    }

    public void setDistrict(String district) {
        binding.tvDistrict.setText(district);
    }

    public void setProvince(String province) {
        binding.tvProvince.setText(province);
    }

    public void setMemberType(String memberType) {
        Util.displayMemberType(binding.tvMemberType, memberType, binding.getRoot().getContext());
    }

    public View getLayoutItem() {
        return binding.layout;
    }
}
