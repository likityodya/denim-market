package thailand.market.denim.denim.CustomImagePicker;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.Build.VERSION;
import android.provider.MediaStore.Images.Media;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.esafirm.imagepicker.R.dimen;
import com.esafirm.imagepicker.R.drawable;
import com.esafirm.imagepicker.R.id;
import com.esafirm.imagepicker.R.layout;
import com.esafirm.imagepicker.R.string;
import com.esafirm.imagepicker.adapter.FolderPickerAdapter;
import com.esafirm.imagepicker.adapter.ImagePickerAdapter;
import com.esafirm.imagepicker.features.ImageLoader;
import com.esafirm.imagepicker.features.ImagePickerConfig;
import com.esafirm.imagepicker.features.ImagePickerPresenter;
import com.esafirm.imagepicker.features.ImagePickerView;
import com.esafirm.imagepicker.features.camera.CameraHelper;
import com.esafirm.imagepicker.helper.ImagePickerPreferences;
import com.esafirm.imagepicker.helper.IntentHelper;
import com.esafirm.imagepicker.listeners.OnFolderClickListener;
import com.esafirm.imagepicker.listeners.OnImageClickListener;
import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;
import com.esafirm.imagepicker.view.GridSpacingItemDecoration;
import com.esafirm.imagepicker.view.ProgressWheel;
import java.util.ArrayList;
import java.util.List;

import thailand.market.denim.denim.R;

public class CustomImagePickerActivity extends AppCompatActivity implements ImagePickerView, OnImageClickListener {
    private static final int RC_CAPTURE = 2000;
    private static final String TAG = "CustomImagePickerActivity";
    public static final int RC_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 23;
    public static final int RC_PERMISSION_REQUEST_CAMERA = 24;
    private ActionBar actionBar;
    private RelativeLayout mainLayout;
    private ProgressWheel progressBar;
    private TextView emptyTextView;
    private RecyclerView recyclerView;
    private GridLayoutManager layoutManager;
    private GridSpacingItemDecoration itemOffsetDecoration;
    private ImagePickerPresenter presenter;
    private ImagePickerPreferences preferences;
    private ImagePickerAdapter imageAdapter;
    private ImagePickerConfig config;
    private FolderPickerAdapter folderAdapter;
    private Handler handler;
    private ContentObserver observer;
    private Parcelable foldersState;
    private int imageColumns;
    private int folderColumns;

    public CustomImagePickerActivity() {
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(layout.ef_activity_image_picker);
        Intent intent = this.getIntent();
        if(intent != null && intent.getExtras() != null) {
            this.preferences = new ImagePickerPreferences(this);
            this.presenter = new ImagePickerPresenter(new ImageLoader(this));
            this.presenter.attachView(this);
            this.setupExtras();
            this.setupView();
            this.orientationBasedUI(this.getResources().getConfiguration().orientation);
        } else {
            this.finish();
        }
    }

    @SuppressLint("WrongViewCast")
    private void setupView() {
        this.mainLayout = (RelativeLayout)this.findViewById(id.main);
        this.progressBar = (ProgressWheel)this.findViewById(id.progress_bar);
        this.emptyTextView = (TextView)this.findViewById(id.tv_empty_images);
        this.recyclerView = (RecyclerView)this.findViewById(id.recyclerView);
        Toolbar toolbar = (Toolbar)this.findViewById(id.toolbar);
        this.setSupportActionBar(toolbar);
        this.actionBar = this.getSupportActionBar();
        if(this.actionBar != null) {
            this.actionBar.setTitle(this.config.isFolderMode()?this.config.getFolderTitle():this.config.getImageTitle());
            this.actionBar.setDisplayHomeAsUpEnabled(true);
            this.actionBar.setHomeAsUpIndicator(drawable.ic_arrow_back);
            this.actionBar.setDisplayShowTitleEnabled(true);
        }

    }

    private void setupExtras() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        this.config = (ImagePickerConfig)bundle.getParcelable(ImagePickerConfig.class.getSimpleName());
        if(this.config == null) {
            this.config = IntentHelper.makeConfigFromIntent(this, intent);
        }

        ArrayList selectedImages = null;
        if(this.config.getMode() == 2 && !this.config.getSelectedImages().isEmpty()) {
            selectedImages = this.config.getSelectedImages();
        }

        if(selectedImages == null) {
            selectedImages = new ArrayList();
        }

        this.imageAdapter = new ImagePickerAdapter(this, selectedImages, this);
        this.folderAdapter = new FolderPickerAdapter(this, new OnFolderClickListener() {
            public void onFolderClick(Folder bucket) {
                CustomImagePickerActivity.this.foldersState = CustomImagePickerActivity.this.recyclerView.getLayoutManager().onSaveInstanceState();
                CustomImagePickerActivity.this.setImageAdapter(bucket.getImages());
            }
        });
    }

    protected void onResume() {
        super.onResume();
        this.getDataWithPermission();
    }

    private void setImageAdapter(List<Image> images) {
        this.imageAdapter.setData(images);
        this.setItemDecoration(this.imageColumns);
        this.recyclerView.setAdapter(this.imageAdapter);
        this.updateTitle();
    }

    private void setFolderAdapter(List<Folder> folders) {
        if(folders != null) {
            this.folderAdapter.setData(folders);
        }

        this.setItemDecoration(this.folderColumns);
        this.recyclerView.setAdapter(this.folderAdapter);
        if(this.foldersState != null) {
            this.layoutManager.setSpanCount(this.folderColumns);
            this.recyclerView.getLayoutManager().onRestoreInstanceState(this.foldersState);
        }

        this.updateTitle();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        this.getMenuInflater().inflate(R.menu.image_picker_menu_main, menu);
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuCamera = menu.findItem(id.menu_camera);
        if(menuCamera != null) {
            menuCamera.setVisible(this.config.isShowCamera());
        }

        MenuItem menuDone = menu.findItem(id.menu_done);
        if(menuDone != null) {
            menuDone.setVisible(!this.isDisplayingFolderView() && !this.imageAdapter.getSelectedImages().isEmpty());
            if(this.config.getMode() == 1 && this.config.isReturnAfterFirst()) {
                menuDone.setVisible(false);
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == 16908332) {
            this.onBackPressed();
            return true;
        } else if(id == R.id.menu_done) {
            this.onDone();
            return true;
        } else if(id == R.id.menu_camera) {
            this.captureImageWithPermission();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void onDone() {
        List selectedImages = this.imageAdapter.getSelectedImages();
        this.presenter.onDoneSelectImages(selectedImages);
    }

    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        this.orientationBasedUI(newConfig.orientation);
    }

    private void orientationBasedUI(int orientation) {
        this.imageColumns = orientation == 1?3:5;
        this.folderColumns = orientation == 1?2:4;
        int columns = this.isDisplayingFolderView()?this.folderColumns:this.imageColumns;
        this.layoutManager = new GridLayoutManager(this, columns);
        this.recyclerView.setLayoutManager(this.layoutManager);
        this.recyclerView.setHasFixedSize(true);
        this.setItemDecoration(columns);
    }

    private void setItemDecoration(int columns) {
        this.layoutManager.setSpanCount(columns);
        if(this.itemOffsetDecoration != null) {
            this.recyclerView.removeItemDecoration(this.itemOffsetDecoration);
        }

        this.itemOffsetDecoration = new GridSpacingItemDecoration(columns, this.getResources().getDimensionPixelSize(dimen.ef_item_padding), false);
        this.recyclerView.addItemDecoration(this.itemOffsetDecoration);
    }

    private void getDataWithPermission() {
        int rc = ActivityCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE");
        if(rc == 0) {
            this.getData();
        } else {
            this.requestWriteExternalPermission();
        }

    }

    private void getData() {
        this.presenter.abortLoad();
        this.presenter.loadImages(this.config.isFolderMode());
    }

    private void requestWriteExternalPermission() {
        Log.w("CustomImagePickerActivity", "Write External permission is not granted. Requesting permission");
        String[] permissions = new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"};
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.WRITE_EXTERNAL_STORAGE")) {
            ActivityCompat.requestPermissions(this, permissions, 23);
        } else {
            String permission = "writeExternalRequested";
            if(!this.preferences.isPermissionRequested("writeExternalRequested")) {
                this.preferences.setPermissionRequested("writeExternalRequested");
                ActivityCompat.requestPermissions(this, permissions, 23);
            } else {
                Snackbar snackbar = Snackbar.make(this.mainLayout, string.ef_msg_no_write_external_permission, -2);
                snackbar.setAction(string.ef_ok, new OnClickListener() {
                    public void onClick(View view) {
                        CustomImagePickerActivity.this.openAppSettings();
                    }
                });
                snackbar.show();
            }
        }

    }

    private void requestCameraPermission() {
        Log.w("CustomImagePickerActivity", "Write External permission is not granted. Requesting permission");
        String[] permissions = new String[]{"android.permission.CAMERA"};
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, "android.permission.CAMERA")) {
            ActivityCompat.requestPermissions(this, permissions, 24);
        } else {
            String permission = "cameraRequested";
            if(!this.preferences.isPermissionRequested("cameraRequested")) {
                this.preferences.setPermissionRequested("cameraRequested");
                ActivityCompat.requestPermissions(this, permissions, 24);
            } else {
                Snackbar snackbar = Snackbar.make(this.mainLayout, string.ef_msg_no_camera_permission, -2);
                snackbar.setAction(string.ef_ok, new OnClickListener() {
                    public void onClick(View view) {
                        CustomImagePickerActivity.this.openAppSettings();
                    }
                });
                snackbar.show();
            }
        }

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case 23:
                if(grantResults.length != 0 && grantResults[0] == 0) {
                    Log.d("CustomImagePickerActivity", "Write External permission granted");
                    this.getData();
                    return;
                }

                Log.e("CustomImagePickerActivity", "Permission not granted: results len = " + grantResults.length + " Result code = " + (grantResults.length > 0?Integer.valueOf(grantResults[0]):"(empty)"));
                this.finish();
                break;
            case 24:
                if(grantResults.length != 0 && grantResults[0] == 0) {
                    Log.d("CustomImagePickerActivity", "Camera permission granted");
                    this.captureImage();
                    return;
                }

                Log.e("CustomImagePickerActivity", "Permission not granted: results len = " + grantResults.length + " Result code = " + (grantResults.length > 0?Integer.valueOf(grantResults[0]):"(empty)"));
                break;
            default:
                Log.d("CustomImagePickerActivity", "Got unexpected permission result: " + requestCode);
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }

    }

    private void openAppSettings() {
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS", Uri.fromParts("package", this.getPackageName(), (String)null));
        intent.addFlags(268435456);
        this.startActivity(intent);
    }

    public void onClick(View view, int position) {
        this.clickImage(position);
    }

    private void clickImage(int position) {
        Image image = this.imageAdapter.getItem(position);
        int selectedItemPosition = this.selectedImagePosition(image);
        if(this.config.getMode() == 2) {
            if(selectedItemPosition == -1) {
                if(this.imageAdapter.getSelectedImages().size() < this.config.getLimit()) {
                    this.imageAdapter.addSelected(image);
                } else {
                    Toast.makeText(this, string.ef_msg_limit_images, 0).show();
                }
            } else {
                this.imageAdapter.removeSelectedPosition(selectedItemPosition, position);
            }
        } else if(selectedItemPosition != -1) {
            this.imageAdapter.removeSelectedPosition(selectedItemPosition, position);
        } else {
            if(this.imageAdapter.getSelectedImages().size() > 0) {
                this.imageAdapter.removeAllSelectedSingleClick();
            }

            this.imageAdapter.addSelected(image);
            if(this.config.isReturnAfterFirst()) {
                this.onDone();
            }
        }

        this.updateTitle();
    }

    private int selectedImagePosition(Image image) {
        List selectedImages = this.imageAdapter.getSelectedImages();

        for(int i = 0; i < selectedImages.size(); ++i) {
            if(((Image)selectedImages.get(i)).getPath().equals(image.getPath())) {
                return i;
            }
        }

        return -1;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 2000 && resultCode == -1) {
            this.presenter.finishCaptureImage(this, data, this.config);
        }

    }

    private void captureImageWithPermission() {
        if(VERSION.SDK_INT >= 23) {
            int rc = ActivityCompat.checkSelfPermission(this, "android.permission.CAMERA");
            if(rc == 0) {
                this.captureImage();
            } else {
                Log.w("CustomImagePickerActivity", "Camera permission is not granted. Requesting permission");
                this.requestCameraPermission();
            }
        } else {
            this.captureImage();
        }

    }

    private void captureImage() {
        if(CameraHelper.checkCameraAvailability(this)) {
            this.presenter.captureImage(this, this.config, 2000);
        }
    }

    protected void onStart() {
        super.onStart();
        if(this.handler == null) {
            this.handler = new Handler();
        }

        this.observer = new ContentObserver(new Handler()) {
            public void onChange(boolean selfChange) {
                CustomImagePickerActivity.this.getData();
            }
        };
        this.getContentResolver().registerContentObserver(Media.EXTERNAL_CONTENT_URI, false, this.observer);
    }

    private void updateTitle() {
        this.supportInvalidateOptionsMenu();
        if(this.isDisplayingFolderView()) {
            this.actionBar.setTitle(this.config.getFolderTitle());
        } else {
            if(this.imageAdapter.getSelectedImages().isEmpty()) {
                this.actionBar.setTitle(this.config.getImageTitle());
            } else if(this.config.getMode() == 2) {
                int imageSize = this.imageAdapter.getSelectedImages().size();
                this.actionBar.setTitle(this.config.getLimit() == 99?String.format(this.getString(string.ef_selected), new Object[]{Integer.valueOf(imageSize)}):String.format(this.getString(string.ef_selected_with_limit), new Object[]{Integer.valueOf(imageSize), Integer.valueOf(this.config.getLimit())}));
            }

        }
    }

    protected void onDestroy() {
        super.onDestroy();
        if(this.presenter != null) {
            this.presenter.abortLoad();
            this.presenter.detachView();
        }

        if(this.observer != null) {
            this.getContentResolver().unregisterContentObserver(this.observer);
            this.observer = null;
        }

        if(this.handler != null) {
            this.handler.removeCallbacksAndMessages((Object)null);
            this.handler = null;
        }

    }

    private boolean isDisplayingFolderView() {
        return this.config.isFolderMode() && (this.recyclerView.getAdapter() == null || this.recyclerView.getAdapter() instanceof FolderPickerAdapter);
    }

    public void onBackPressed() {
        if(this.config.isFolderMode() && !this.isDisplayingFolderView()) {
            this.setFolderAdapter((List)null);
        } else {
            this.setResult(0);
            super.onBackPressed();
        }
    }

    public void finishPickImages(List<Image> images) {
        Intent data = new Intent();
        data.putParcelableArrayListExtra("selectedImages", (ArrayList)images);
        this.setResult(-1, data);
        this.finish();
    }

    public void showCapturedImage() {
        this.getDataWithPermission();
    }

    public void showFetchCompleted(List<Image> images, List<Folder> folders) {
        if(this.config.isFolderMode()) {
            this.setFolderAdapter(folders);
        } else {
            this.setImageAdapter(images);
        }

    }

    public void showError(Throwable throwable) {
        String message = "Unknown Error";
        if(throwable != null && throwable instanceof NullPointerException) {
            message = "Images not exist";
        }

        Toast.makeText(this, message, 0).show();
    }

    public void showLoading(boolean isLoading) {
        this.progressBar.setVisibility(isLoading?0:8);
        this.recyclerView.setVisibility(isLoading?8:0);
        this.emptyTextView.setVisibility(8);
    }

    public void showEmpty() {
        this.progressBar.setVisibility(8);
        this.recyclerView.setVisibility(8);
        this.emptyTextView.setVisibility(0);
    }
}
