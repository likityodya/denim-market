package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class DistrictResponse extends BaseResponse {

    @SerializedName("data")
    private DistrictResultData districtResultData;

    public DistrictResultData getDistrictResultData() {
        return districtResultData;
    }
}
