package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 13/7/2560.
 */

public class ProductBrandResultData implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("price")
    private String price;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("generation")
    private String generation;

    public int getId() {
        return id;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public String getGeneration() {
        return generation;
    }

    public ProductBrandResultData(){}

    protected ProductBrandResultData(Parcel in) {
        id = in.readInt();
        price = in.readString();
        description = in.readString();
        image = in.readString();
        generation = in.readString();
    }

    public static final Parcelable.Creator<ProductBrandResultData> CREATOR = new Parcelable.Creator<ProductBrandResultData>() {
        @Override
        public ProductBrandResultData createFromParcel(Parcel in) {
            return new ProductBrandResultData(in);
        }

        @Override
        public ProductBrandResultData[] newArray(int size) {
            return new ProductBrandResultData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(price);
        dest.writeString(description);
        dest.writeString(image);
        dest.writeString(generation);
    }

}
