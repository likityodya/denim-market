package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 30/7/2560.
 */

public class OrderDetailResultData {


    @SerializedName("id")
    private int id;

    @SerializedName("brand_name")
    private String brand_name;

    @SerializedName("generation")
    private String generation;

    @SerializedName("date")
    private String date;

    @SerializedName("price")
    private String price;

    @SerializedName("status")
    private String status;

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("image")
    private String image;

    @SerializedName("shipping_data")
    private ShippingData shipping;

    @SerializedName("district")
    private DistrictData district;

    @SerializedName("province")
    private ProvinceResultData province;

    public int getId() {
        return id;
    }

    public String getBrandName() {
        return brand_name;
    }

    public String getGeneration() {
        return generation;
    }

    public String getDate() {
        return date;
    }

    public String getPrice() {
        return price;
    }

    public String getStatus() {
        return status;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getImage() {
        return image;
    }

    public ShippingData getShipping() {
        return shipping;
    }

    public DistrictData getDistrict() {
        return district;
    }

    public ProvinceResultData getProvince() {
        return province;
    }
}
