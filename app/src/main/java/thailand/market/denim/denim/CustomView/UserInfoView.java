package thailand.market.denim.denim.CustomView;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.inthecheesefactory.thecheeselibrary.view.BaseCustomViewGroup;

import org.w3c.dom.Text;

import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.LayoutUserInfoBinding;

/**
 * Created by Phitakphong on 16/6/2560.
 */

public class UserInfoView extends BaseCustomViewGroup {

    private TextView tvMemberType;
    private TextView tvFullName;
    private TextView tvMemberId;

    public UserInfoView(Context context) {
        super(context);
    }

    public UserInfoView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public UserInfoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public UserInfoView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_user_info, this);

        tvMemberType = (TextView) findViewById(R.id.tv_member_type);
        tvFullName = (TextView) findViewById(R.id.tv_full_name);
        tvMemberId = (TextView) findViewById(R.id.tv_member_id);
    }

    public void setProfileResultData(ProfileResultData resultData) {
        tvFullName.setText(resultData.getFirstName() + " " + resultData.getLastName());
        tvMemberId.setText(String.valueOf(resultData.getId()));
        String memberType = resultData.getUserType().toUpperCase();
        tvMemberType.setText(resultData.getUserType());
        Util.displayMemberType(tvMemberType, resultData.getUserType(), getContext());
    }
}
