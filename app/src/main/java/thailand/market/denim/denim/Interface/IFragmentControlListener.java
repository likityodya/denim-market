package thailand.market.denim.denim.Interface;

import android.support.v4.app.Fragment;

import thailand.market.denim.denim.Fragment.BaseFragment;

/**
 * Created by Phitakphong on 16/6/2560.
 */

public interface IFragmentControlListener {
    void onGoNextFragment(BaseFragment fragment);
    boolean onGoBackFragment();
}

