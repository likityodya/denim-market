package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.OrderDetailResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentViewBuyItemBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ViewBuyItemFragment extends BaseFragment {

    private FragmentViewBuyItemBinding binding;
    private WrapperFragment wrapperFragment;
    private MemberRepository memberRepository;
    private int id;

    public ViewBuyItemFragment() {
        super();
    }

    public void setID(int id){
        this.id = id;
    }

    public static ViewBuyItemFragment newInstance(int id) {
        ViewBuyItemFragment fragment = new ViewBuyItemFragment();
        fragment.setID(id);
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentViewBuyItemBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        memberRepository = new MemberRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();

        showProgressDialog(getString(R.string.loading));
        memberRepository.getOrderDetail(id, new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {

                OrderDetailResultData data = (OrderDetailResultData) response;
                switch (data.getStatus()) {
                    case "pending":
                    case "completed":
                        binding.btnDelete.setVisibility(View.VISIBLE);
                        break;
                    default:
                        binding.btnDelete.setVisibility(View.GONE);
                        break;
                }

                Util.loadImage(getContext(), data.getImage(), binding.imageView, binding.progressBar);

                binding.tvBrand.setText(data.getBrandName());
                binding.tvGeneration.setText(data.getGeneration());
                binding.tvPrice.setText(data.getPrice());
                binding.tvQty.setText(data.getQuantity());
                binding.tvFirstName.setText(data.getShipping().getFirstName());
                binding.tvLastName.setText(data.getShipping().getLastName());
                binding.tvAddress.setText(data.getShipping().getAddress());
                binding.tvPhone.setText(data.getShipping().getPhone());
                binding.tvPostCode.setText(data.getShipping().getZipcode());
                binding.tvProvince.setText(data.getProvince().getNameTh());
                binding.tvDistrict.setText(data.getDistrict().getNameTh());

                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgressDialog();
            }
        });

        initEvents();
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });
        binding.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext())
                        .setMessage("ลบรายการสั่งซื้อ")
                        .setCancelable(true)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                showProgressDialog(getString(R.string.loading));
                                memberRepository.deleteOrder(id, PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
                                    @Override
                                    public void OnSuccess(Object response) {
                                        Util.showToast(response.toString());
                                        hideProgressDialog();
                                        wrapperFragment.onGoBackFragment();
                                    }

                                    @Override
                                    public void OnError(String message) {
                                        Util.showToast(message);
                                        hideProgressDialog();
                                    }
                                });
                            }
                        });
                alertDialog.show();

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

}
