package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 3/7/2560.
 */

public class DistrictData implements Parcelable {

    @SerializedName("id")
    private int id ;

    @SerializedName("code")
    private String code;

    @SerializedName("name_th")
    private String name_th;

    @SerializedName("name_en")
    private String name_en;

    @SerializedName("province_id")
    private int province_id;


    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getNameTh() {
        return name_th;
    }

    public String getNameEn() {
        return name_en;
    }

    public int getProvinceId() {
        return province_id;
    }

    protected DistrictData(Parcel in) {
        id = in.readInt();
        code = in.readString();
        name_th = in.readString();
        name_en = in.readString();
        province_id = in.readInt();
    }

    public static final Parcelable.Creator<DistrictData> CREATOR = new Parcelable.Creator<DistrictData>() {
        @Override
        public DistrictData createFromParcel(Parcel in) {
            return new DistrictData(in);
        }

        @Override
        public DistrictData[] newArray(int size) {
            return new DistrictData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(code);
        dest.writeString(name_th);
        dest.writeString(name_en);
        dest.writeInt(province_id);
    }

}
