package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 18/7/2560.
 */

public class PostDataItem {

    @SerializedName("pin")
    private List<SearchPostListData> pins;

    @SerializedName("normal")
    private List<SearchPostListData> normals;

    public List<SearchPostListData> getPins() {
        return pins;
    }

    public List<SearchPostListData> getNormals() {
        return normals;
    }
}
