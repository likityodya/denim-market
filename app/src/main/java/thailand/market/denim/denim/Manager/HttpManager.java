package thailand.market.denim.denim.Manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import thailand.market.denim.denim.Manager.HTTP.CommonService;
import thailand.market.denim.denim.Manager.HTTP.ForumService;
import thailand.market.denim.denim.Manager.HTTP.MemberService;
import thailand.market.denim.denim.Manager.HTTP.PostService;
import thailand.market.denim.denim.Util.Constant;


public class HttpManager {

    private static HttpManager instance;

    public static HttpManager getInstance() {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }

    private HttpManager() {
        createMemberService();
        createCommonService();
        createForumService();
        createPostService();
    }

    private Retrofit createRequest() {
        Gson gson = new GsonBuilder()
                .setDateFormat(Constant.JSON_DATE_TIME_FORMAT)
                .create();
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.connectTimeout(60, TimeUnit.SECONDS);
//        httpClient.readTimeout(60, TimeUnit.SECONDS);
//        httpClient.writeTimeout(60, TimeUnit.SECONDS);
//        httpClient.retryOnConnectionFailure(false);
        httpClient.addInterceptor(logging);
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request request = original.newBuilder()
                        .method(original.method(), original.body())
                        .build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(client)
                .build();
        return retrofit;
    }

    private MemberService memberService;
    private CommonService commonService;
    private ForumService forumService;
    private PostService postService;

    private void createMemberService() {
        memberService = this.createRequest().create(MemberService.class);
    }

    private void createCommonService() {
        commonService = this.createRequest().create(CommonService.class);
    }

    private void createForumService() {
        forumService = this.createRequest().create(ForumService.class);
    }

    private void createPostService() {
        postService = this.createRequest().create(PostService.class);
    }

    public MemberService getMemberService() {
        return memberService;
    }

    public CommonService getCommonService() {
        return commonService;
    }

    public ForumService getForumService() {
        return forumService;
    }

    public PostService getPostService() {
        return postService;
    }
}


