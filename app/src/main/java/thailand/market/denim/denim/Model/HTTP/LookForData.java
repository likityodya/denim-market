package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 17/7/2560.
 */

public class LookForData {

    @SerializedName("id")
    private int id;

    @SerializedName("line_1")
    private String line_1;

    @SerializedName("line_2")
    private String line_2;

    @SerializedName("line_3")
    private String line_3;

    public int getId() {
        return id;
    }

    public String getLine1() {
        return line_1;
    }

    public String getLine2() {
        return line_2;
    }

    public String getLine3() {
        return line_3;
    }
}
