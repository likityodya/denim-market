package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.FavoritePostItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.FavoritePostData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerFavoritePostItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class FavoritePostRecyclerViewAdapter extends RecyclerView.Adapter<FavoritePostItemViewHolder> {

    private List<FavoritePostData> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener itemClickListener;

    public FavoritePostRecyclerViewAdapter(List<FavoritePostData> items) {
        this.items = items;
    }


    @Override
    public FavoritePostItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerFavoritePostItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_favorite_post_item, parent, false);
        return new FavoritePostItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(FavoritePostItemViewHolder postItemViewHolder, final int position) {
        final FavoritePostData data = items.get(position);
        postItemViewHolder.displayImage(data.getImages());
        postItemViewHolder.setBrand(data.getBrand());
        postItemViewHolder.setGeneration(data.getGeneration());
        postItemViewHolder.setPrice(data.getPrice());
        postItemViewHolder.setMemberType(data.getUserType());
        postItemViewHolder.setDistrict(data.getDistrict().getNameTh());
        postItemViewHolder.setProvince(data.getProvince().getNameTh());
        postItemViewHolder.setProductType(data.getProductType());
        postItemViewHolder.setSize(data.getSize());
        postItemViewHolder.setType(data.getType());
        postItemViewHolder.getLayoutItem().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.OnItemClick(data);
            }
        });
        postItemViewHolder.getLayoutItem().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                itemClickListener.OnItemLongClick(v, data);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItemClickListener(IRecyclerViewItemClickListener onClickListener) {
        this.itemClickListener = onClickListener;
    }
}