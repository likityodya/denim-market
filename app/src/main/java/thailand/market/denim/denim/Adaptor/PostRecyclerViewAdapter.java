package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.PostItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.PostItemResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerPostItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class PostRecyclerViewAdapter extends RecyclerView.Adapter<PostItemViewHolder> {

    private List<PostItemResultData> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener markedSellClickListener;
    private IRecyclerViewItemClickListener editClickListener;
    private IRecyclerViewItemClickListener deleteClickListener;
    private IRecyclerViewItemClickListener renewClickListener;

    public PostRecyclerViewAdapter(List<PostItemResultData> items) {
        this.items = items;
    }

    @Override
    public PostItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerPostItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_post_item, parent, false);
        return new PostItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(PostItemViewHolder holder, final int position) {
        final PostItemResultData item = items.get(position);
        holder.displayImage(item.getImage());
        holder.setPinPostNumber(item.getPostponedNumber());
        if (markedSellClickListener != null) {
            holder.setSellClickListener(markedSellClickListener, item);
        }
        if (editClickListener != null) {
            holder.setEditClickListener(editClickListener, item);
        }
        if (deleteClickListener != null) {
            holder.setDeleteClickListener(deleteClickListener, item);
        }
        if (renewClickListener != null) {
            holder.setRenewClickListener(renewClickListener, item);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setMarkedSellClickListener(IRecyclerViewItemClickListener markedSellClickListener) {
        this.markedSellClickListener = markedSellClickListener;
    }

    public void setEditClickListener(IRecyclerViewItemClickListener editClickListener) {
        this.editClickListener = editClickListener;
    }

    public void setDeleteClickListener(IRecyclerViewItemClickListener deleteClickListener) {
        this.deleteClickListener = deleteClickListener;
    }
    public void setRenewClickListener(IRecyclerViewItemClickListener renewClickListener) {
        this.renewClickListener = renewClickListener;
    }
}