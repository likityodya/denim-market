package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.esafirm.imagepicker.model.Image;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.DummyImageItemViewHolder;
import thailand.market.denim.denim.ListViewItem.ImageItemViewHolder;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class DummyImageRecyclerViewAdapter extends RecyclerView.Adapter<DummyImageItemViewHolder> {

    private List<Drawable> items;
    private LayoutInflater inflater;

    public DummyImageRecyclerViewAdapter(List<Drawable> items) {
        this.items = items;
    }

    @Override
    public DummyImageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerPhotoGridItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_photo_grid_item, parent, false);
        return new DummyImageItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(DummyImageItemViewHolder holder, final int position) {
        Drawable item = items.get(position);
        holder.displayImage(item);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


}