package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerNewsItemBinding;
import thailand.market.denim.denim.databinding.RecyclerPromotionItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class PromotionItemViewHolder extends RecyclerView.ViewHolder {
    public RecyclerPromotionItemBinding binding;

    public PromotionItemViewHolder(RecyclerPromotionItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setTitle(String title){
        binding.tvTitle.setText(title);
    }
    public void setDesc(String desc){
        binding.tvDesc.setText(desc);
    }
    public void displayImage(String imageUrl) {
        Util.loadImage(binding.getRoot().getContext(), imageUrl, binding.imageView, binding.progressBar);
    }
}
