package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 18/7/2560.
 */

public class SearchPostData {

    @SerializedName("posts")
    private PostDataItem item;

    public PostDataItem getItem() {
        return item;
    }
}
