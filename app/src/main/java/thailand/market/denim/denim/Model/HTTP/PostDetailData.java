package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 16/7/2560.
 */

public class PostDetailData {

    @SerializedName("id")
    private int id;

    @SerializedName("product_type")
    private String product_type;

    @SerializedName("post_date")
    private String post_date;

    @SerializedName("brand_id")
    private int brand_id;

    @SerializedName("brand")
    private String brand;

    @SerializedName("type_id")
    private int type_id;

    @SerializedName("type")
    private String type;

    @SerializedName("generation")
    private String generation;

    @SerializedName("price")
    private String price;

    @SerializedName("size_option")
    private String size_option;

    @SerializedName("shirts_size_1")
    private String shirts_size;

    @SerializedName("pants_size_1")
    private String pants_size_1;

    @SerializedName("pants_size_2")
    private String pants_size_2;

    @SerializedName("shoes_size_eu")
    private String shoes_size_eu;

    @SerializedName("shoes_size_uk")
    private String shoes_size_uk;

    @SerializedName("shoes_size_us")
    private String shoes_size_us;

    @SerializedName("size")
    private String size;

    @SerializedName("description")
    private String description;

    @SerializedName("images")
    private List<String> images;

    @SerializedName("image_path")
    private String image_path;

    @SerializedName("province_id")
    private int province_id;

    @SerializedName("district_id")
    private int district_id;

    @SerializedName("phone")
    private String phone;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductType() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getPostDate() {
        return post_date;
    }

    public void setPost_date(String post_date) {
        this.post_date = post_date;
    }

    public int getBrandId() {
        return brand_id;
    }

    public void setBrand_id(int brand_id) {
        this.brand_id = brand_id;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getTypeId() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSizeOption() {
        return size_option;
    }

    public void setSize_option(String size_option) {
        this.size_option = size_option;
    }

    public String getShirtsSize() {
        return shirts_size;
    }

    public void setShirts_size(String shirts_size) {
        this.shirts_size = shirts_size;
    }

    public String getPants_size_1() {
        return pants_size_1;
    }

    public void setPants_size_1(String pants_size_1) {
        this.pants_size_1 = pants_size_1;
    }

    public String getPants_size_2() {
        return pants_size_2;
    }

    public void setPants_size_2(String pants_size_2) {
        this.pants_size_2 = pants_size_2;
    }

    public String getShoesSizeEu() {
        return shoes_size_eu;
    }

    public void setShoes_size_eu(String shoes_size_eu) {
        this.shoes_size_eu = shoes_size_eu;
    }

    public String getShoesSizeUk() {
        return shoes_size_uk;
    }

    public void setShoes_size_uk(String shoes_size_uk) {
        this.shoes_size_uk = shoes_size_uk;
    }

    public String getShoesSizeUS() {
        return shoes_size_us;
    }

    public void setShoes_size_us(String shoes_size_us) {
        this.shoes_size_us = shoes_size_us;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getImagePath() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public int getProvinceId() {
        return province_id;
    }

    public void setProvince_id(int province_id) {
        this.province_id = province_id;
    }

    public int getDistrictId() {
        return district_id;
    }

    public void setDistrict_id(int district_id) {
        this.district_id = district_id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

}
