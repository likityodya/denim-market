package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import thailand.market.denim.denim.databinding.FragmentPurchaseListBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PurchaseListFragment extends BaseFragment {

    private FragmentPurchaseListBinding binding;
    private WrapperFragment wrapperFragment;

    private FragmentPagerAdapter pagerAdapter;

    public PurchaseListFragment() {
        super();
    }

    public static PurchaseListFragment newInstance() {
        PurchaseListFragment fragment = new PurchaseListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentPurchaseListBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {

        pagerAdapter = getFragmentPagerAdapter();
        binding.viewpager.setAdapter(pagerAdapter);
        binding.tabs.setupWithViewPager(binding.viewpager);
        binding.tabs.getTabAt(0).setText("ประวัติการสั่งซื้อ");
        binding.tabs.getTabAt(1).setText("รายการฝากหา");
        initEvents();
    }

    public void goNext(BaseFragment fragment) {
        wrapperFragment.onGoNextFragment(fragment);
    }

    public void goBack() {
        wrapperFragment.onGoBackFragment();
    }

    @NonNull
    private FragmentPagerAdapter getFragmentPagerAdapter() {

        return new FragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                switch (position) {
                    case 0:
                        return PurchaseFragment.newInstance();
                    case 1:
                        return WantedFragment.newInstance();
                    default:
                        return null;
                }
            }

            @Override
            public int getCount() {
                return 2;
            }
        };
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });

    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
        String sss = "";
    }

}
