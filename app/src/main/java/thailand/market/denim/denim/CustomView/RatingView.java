package thailand.market.denim.denim.CustomView;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import com.inthecheesefactory.thecheeselibrary.view.BaseCustomViewGroup;

import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.R;

/**
 * Created by Phitakphong on 16/6/2560.
 */

public class RatingView extends BaseCustomViewGroup {

    private ImageView ivStar1;
    private ImageView ivStar2;
    private ImageView ivStar3;
    private ImageView ivStar4;
    private ImageView ivStar5;

    public RatingView(Context context) {
        super(context);
    }

    public RatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RatingView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(21)
    public RatingView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_rating, this);

        ivStar1 = (ImageView) findViewById(R.id.star_1);
        ivStar2 = (ImageView) findViewById(R.id.star_2);
        ivStar3 = (ImageView) findViewById(R.id.star_3);
        ivStar4 = (ImageView) findViewById(R.id.star_4);
        ivStar5 = (ImageView) findViewById(R.id.star_5);
    }

    public void setRating(int rating) {

        Drawable ratingDrawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ico_star_yellow, null);
        if (rating >= 1) {
            ivStar1.setImageDrawable(ratingDrawable);
        }
        if (rating >= 2) {
            ivStar2.setImageDrawable(ratingDrawable);
        }
        if (rating >= 3) {
            ivStar3.setImageDrawable(ratingDrawable);
        }
        if (rating >= 4) {
            ivStar4.setImageDrawable(ratingDrawable);
        }
        if (rating == 5) {
            ivStar5.setImageDrawable(ratingDrawable);
        }
    }
}
