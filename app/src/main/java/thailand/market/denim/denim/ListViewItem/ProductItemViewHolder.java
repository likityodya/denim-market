package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerBrandItemBinding;
import thailand.market.denim.denim.databinding.RecyclerProductItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class ProductItemViewHolder extends RecyclerView.ViewHolder {
    public RecyclerProductItemBinding binding;

    public ProductItemViewHolder(RecyclerProductItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void setGeneration(String generation) {
        binding.tvGeneration.setText(generation);
    }

    public void setPrice(String price) {
        if (!TextUtils.isEmpty(price)) {
            binding.tvPrice.setText(price);
        }
    }

    public void setDescription(String description) {
        binding.tvDetail.setText(binding.tvDetail.getText() + description);
    }

    public void displayImage(String imageUrl) {
        Util.loadImage(binding.getRoot().getContext(), imageUrl, binding.imageView, binding.progressBar);
    }

    public void setOnClickListener(final IRecyclerViewItemClickListener clickListener, final ProductBrandResultData data) {
        binding.listItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.OnItemClick(data);
            }
        });
    }

}
