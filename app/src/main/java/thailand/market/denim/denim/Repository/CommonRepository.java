package thailand.market.denim.denim.Repository;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import retrofit2.Response;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.HTTP.CommonService;
import thailand.market.denim.denim.Manager.HttpManager;
import thailand.market.denim.denim.Model.HTTP.BrandResponse;
import thailand.market.denim.denim.Model.HTTP.DistrictResponse;
import thailand.market.denim.denim.Model.HTTP.HomeResponse;
import thailand.market.denim.denim.Model.HTTP.ProductTypeResponse;
import thailand.market.denim.denim.Model.HTTP.ProvinceResponse;
import thailand.market.denim.denim.Util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class CommonRepository {
    CommonService commonService;

    public CommonRepository() {
        commonService = HttpManager.getInstance().getCommonService();
    }

    public void home(final HTTPCallback httpCallback) {
        Flowable<Response<HomeResponse>> register = commonService.home();
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<HomeResponse>>() {
                    @Override
                    public void onNext(Response<HomeResponse> response) {
                        if (response.isSuccessful()) {
                            HomeResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.gethomeResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getProvinces(final HTTPCallback httpCallback) {
        Flowable<Response<ProvinceResponse>> register = commonService.getProvinces();
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<ProvinceResponse>>() {
                    @Override
                    public void onNext(Response<ProvinceResponse> response) {
                        if (response.isSuccessful()) {
                            ProvinceResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getProvinceResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getDistrict(int province, final HTTPCallback httpCallback) {
        Flowable<Response<DistrictResponse>> register = commonService.getDistricts(province);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<DistrictResponse>>() {
                    @Override
                    public void onNext(Response<DistrictResponse> response) {
                        if (response.isSuccessful()) {
                            DistrictResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getDistrictResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getBands(final HTTPCallback httpCallback) {
        Flowable<Response<BrandResponse>> register = commonService.getBrands();
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BrandResponse>>() {
                    @Override
                    public void onNext(Response<BrandResponse> response) {
                        if (response.isSuccessful()) {
                            BrandResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getBrandResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getProductType(int brandID, final HTTPCallback httpCallback) {
        Flowable<Response<ProductTypeResponse>> register = commonService.getProductType(brandID);
        register.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<ProductTypeResponse>>() {
                    @Override
                    public void onNext(Response<ProductTypeResponse> response) {
                        if (response.isSuccessful()) {
                            ProductTypeResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getProductTypeResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        t.printStackTrace();
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
