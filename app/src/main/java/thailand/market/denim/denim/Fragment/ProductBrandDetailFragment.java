package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.synnapps.carouselview.ImageListener;

import java.util.ArrayList;
import java.util.List;

import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.DTO.ConfirmOrderDto;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResultData;
import thailand.market.denim.denim.Model.HTTP.ProductDetailResultData;
import thailand.market.denim.denim.Model.HTTP.ProfileResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.ForumRepository;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentProductBrandDetailBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ProductBrandDetailFragment extends BaseFragment {

    private FragmentProductBrandDetailBinding binding;
    private WrapperFragment wrapperFragment;
    private String[] images;
    private ProductBrandResultData product;
    private ForumRepository forumRepository;
    private ProductDetailResultData data;
    private ConfirmOrderDto orderDto;

    public ProductBrandDetailFragment() {
        super();
    }

    public static ProductBrandDetailFragment newInstance(ProductBrandResultData product) {
        ProductBrandDetailFragment fragment = new ProductBrandDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable("ProductBrandResultData", product);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProductBrandDetailBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        product = getArguments().getParcelable("ProductBrandResultData");
        forumRepository = new ForumRepository();
        orderDto = new ConfirmOrderDto();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        showProgressDialog(getString(R.string.loading));
        forumRepository.getProductDetail(product.getId(), httpCallback);
        initEvents();
    }

    private void initEvents() {

        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()) {
                    confirm();
                }
            }
        });

        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });

        binding.selectWaist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("รอบเอว WAIST");

                final List<String> w = data.getPantsW();
                String[] items = new String[w.size()];
                for (int i = 0; i < w.size(); i++) {
                    items[i] = w.get(i);
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        String selected = w.get(which);
                        orderDto.setPantsW(selected);
                        binding.selectWaist.setTextValue(String.valueOf(selected));
                    }
                });
                builder.show();
            }
        });
        binding.selectLength.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ความยาว LENGTH");

                final List<String> l = data.getPantsL();
                String[] items = new String[l.size()];
                for (int i = 0; i < l.size(); i++) {
                    items[i] = l.get(i).toString();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        String selected = l.get(which);
                        orderDto.setPantsSize(selected);
                        binding.selectLength.setTextValue(String.valueOf(selected));
                    }
                });
                builder.show();
            }
        });

        binding.selectSizeUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ไซต์ US");

                final List<Double> inter = data.getShoesInter();
                String[] items = new String[inter.size()];
                for (int i = 0; i < inter.size(); i++) {
                    items[i] = inter.get(i).toString();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        double selected = inter.get(which);
                        orderDto.setShoesUS(selected);
                        binding.selectSizeUs.setTextValue(String.valueOf(selected));
                    }
                });
                builder.show();
            }
        });
        binding.selectUkSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ไซต์ UK");

                final List<Double> uk = data.getShoesUk();
                String[] items = new String[uk.size()];
                for (int i = 0; i < uk.size(); i++) {
                    items[i] = uk.get(i).toString();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Double selected = uk.get(which);
                        orderDto.setShoesUk(selected);
                        binding.selectUkSize.setTextValue(String.valueOf(selected));
                    }
                });
                builder.show();
            }
        });

        binding.selectSizeEu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("ไซต์ EU");

                final List<String> eu = data.getShoesEU();
                String[] items = new String[eu.size()];
                for (int i = 0; i < eu.size(); i++) {
                    items[i] = eu.get(i).toString();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        String selected = eu.get(which);
                        orderDto.setShoesEU(selected);
                        binding.selectSizeEu.setTextValue(selected);
                    }
                });
                builder.show();
            }
        });

        binding.selectSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("รอบอก CHEST");

                final List<String> size = data.getShirtSize();
                String[] items = new String[size.size()];
                for (int i = 0; i < size.size(); i++) {
                    items[i] = size.get(i).toString();
                }

                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        String selected = size.get(which);
                        orderDto.setShirtSize(selected);
                        binding.selectSize.setTextValue(selected);
                    }
                });
                builder.show();
            }
        });


    }

    private void confirm() {
        showProgressDialog(getString(R.string.loading));
        MemberRepository memberRepository = new MemberRepository();
        memberRepository.getProfile(PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                ProfileResultData profileResultData = (ProfileResultData) response;
                orderDto.setQuantity(1);
                orderDto.setFirstName(profileResultData.getFirstName());
                orderDto.setLastName(profileResultData.getLastName());
                orderDto.setAddress(profileResultData.getAddress());
                orderDto.setPhone(profileResultData.getPhone());
                orderDto.setProvinceId(profileResultData.getProvince().getId());
                orderDto.setDistrictId(profileResultData.getDistrict().getId());
                hideProgressDialog();
                wrapperFragment.onGoNextFragment(ConfirmBuyItemFragment.newInstance(product, orderDto, data));
            }

            @Override
            public void OnError(String message) {
                hideProgressDialog();
                Util.showToast(message);
            }
        });
    }

    private boolean validate() {
        boolean valid = true;
        switch (data.getType()) {
            case "pants":
                if (orderDto.getPantsSize() == null) {
                    binding.selectLength.setError("เลือกความยาว LENGTH");
                    valid = false;
                } else {
                    binding.selectLength.setError(null);
                }
                if (orderDto.getPantsW() == null) {
                    binding.selectWaist.setError("เลือกรอบเอว WAIST");
                    valid = false;
                } else {
                    binding.selectWaist.setError(null);
                }
                break;
            case "shirt":
                if (orderDto.getShirtSize() == null) {
                    binding.selectSize.setError("เลือกรอบอก CHEST");
                    valid = false;
                } else {
                    binding.selectSize.setError(null);
                }
                break;
            case "shoes":
                if (orderDto.getShoesUS() == null) {
                    binding.selectSizeUs.setError("เลือกไซต์ US");
                    valid = false;
                } else {
                    binding.selectSizeUs.setError(null);
                }
                if (orderDto.getShoesUk() == null) {
                    binding.selectUkSize.setError("เลือกไซต์ UK");
                    valid = false;
                } else {
                    binding.selectUkSize.setError(null);
                }
                if (orderDto.getShoesEU() == null) {
                    binding.selectSizeEu.setError("เลือกไซต์ EU");
                    valid = false;
                } else {
                    binding.selectSizeEu.setError(null);
                }
                break;
        }
        return valid;
    }

    private ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Util.loadImage(getContext(), images[position], imageView);
        }
    };

    private HTTPCallback httpCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            data = (ProductDetailResultData) response;
            List<String> tempImage = new ArrayList<>();
            for (String img : data.getImages()) {
                tempImage.add(data.getImagePath() + img);
            }
            images = new String[tempImage.size()];
            images = tempImage.toArray(images);
            binding.carouselView.setImageListener(imageListener);
            binding.carouselView.setPageCount(images.length);

            binding.tvBrand.setText(data.getBrandName());
            binding.tvDate.setText(data.getDate());
            binding.tvDesc.setText(binding.tvDesc.getText() + data.getDescription());
            binding.tvGeneration.setText(data.getGeneration());
            if (!TextUtils.isEmpty(data.getPrice())) {
                binding.tvPrice.setText(data.getPrice());
            }

            switch (data.getType()) {
                case "pants":
                    binding.layoutPants.setVisibility(View.VISIBLE);
                    break;
                case "shirt":
                    binding.layoutShirts.setVisibility(View.VISIBLE);
                    break;
                case "shoes":
                    binding.layoutShoes.setVisibility(View.VISIBLE);
                    break;
            }

            String strPrice = data.getPrice().replace(",", "");
            orderDto.setPrice(Integer.parseInt(strPrice));

            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

}
