package thailand.market.denim.denim.Repository;

import com.esafirm.imagepicker.model.Image;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DefaultSubscriber;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Manager.HTTP.PostService;
import thailand.market.denim.denim.Manager.HttpManager;
import thailand.market.denim.denim.Model.DTO.SellProductDto;
import thailand.market.denim.denim.Model.HTTP.BaseResponse;
import thailand.market.denim.denim.Model.HTTP.FavoritePostResponse;
import thailand.market.denim.denim.Model.HTTP.HistoryPostResponse;
import thailand.market.denim.denim.Model.HTTP.PostDetailResponse;
import thailand.market.denim.denim.Model.HTTP.PostListResponse;
import thailand.market.denim.denim.Model.HTTP.SearchPostListResponse;
import thailand.market.denim.denim.Util.ImageResize;
import thailand.market.denim.denim.Util.Util;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class PostRepository {
    PostService postService;

    public PostRepository() {
        postService = HttpManager.getInstance().getPostService();
    }

    public void create(SellProductDto sellProductDto, final HTTPCallback httpCallback) {

        RequestBody user_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getUserId()));
        RequestBody brand_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getBrandId()));
        RequestBody type_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getTypeId()));
        RequestBody generation = RequestBody.create(MediaType.parse("text/plain"), sellProductDto.getGeneration());
        RequestBody product_type = RequestBody.create(MediaType.parse("text/plain"), sellProductDto.getProductType());
        RequestBody price = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getPrice()));
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), sellProductDto.getDescription());
        RequestBody province_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getProvinceId()));
        RequestBody district_id = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getDistrictId()));
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getPhone()));

        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("user_id", user_id);
        map.put("brand_id", brand_id);
        map.put("type_id", type_id);
        map.put("generation", generation);
        map.put("product_type", product_type);
        map.put("price", price);
        map.put("description", description);
        map.put("province_id", province_id);
        map.put("district_id", district_id);
        map.put("phone", phone);


        if (sellProductDto.getSize() != null) {
            RequestBody size = RequestBody.create(MediaType.parse("text/plain"), sellProductDto.getSize());
            map.put("size", size);
        }

        if (sellProductDto.getShirtsSize() != null) {
            RequestBody shirts_size = RequestBody.create(MediaType.parse("text/plain"), sellProductDto.getShirtsSize());
            map.put("shirts_size_1", shirts_size);
        }

        if (sellProductDto.getPantsW() != null) {
            RequestBody pants_w = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getPantsW()));
            map.put("pants_size_2", pants_w);
        }
        if (sellProductDto.getPantSize() != null) {
            RequestBody pants_l = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getPantSize()));
            map.put("pants_size_1", pants_l);
        }

        if (sellProductDto.getShoesSizeEU() != null) {
            RequestBody shoes_size_eu = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getShoesSizeEU()));
            map.put("shoes_size_eu", shoes_size_eu);
        }
        if (sellProductDto.getShoesSizeUk() != null) {
            RequestBody shoes_size_uk = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getShoesSizeUk()));
            map.put("shoes_size_uk", shoes_size_uk);
        }
        if (sellProductDto.getShoesSizeUS() != null) {
            RequestBody shoes_size_us = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(sellProductDto.getShoesSizeUS()));
            map.put("shoes_size_us", shoes_size_us);
        }

        MultipartBody.Part[] parts = new MultipartBody.Part[sellProductDto.getImages().size()];
        for (int i = 0; i < parts.length; i++) {
            try {
                ImageResize resize = new ImageResize();
                String resized = resize.resize(sellProductDto.getImages().get(i).getPath());
                File file = new File(resized);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("image[]", file.getName(), requestFile);
                parts[i] = body;
            } catch (Exception e) {
                httpCallback.OnError(e.getMessage());
            }
        }
        Flowable<Response<BaseResponse>> createPost = postService.create(
                map
                , parts
        );
        createPost.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

    public void getPosts(int userID, final HTTPCallback httpCallback) {
        Flowable<Response<PostListResponse>> profile = postService.getPosts(userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<PostListResponse>>() {
                    @Override
                    public void onNext(Response<PostListResponse> response) {
                        if (response.isSuccessful()) {
                            PostListResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getPostListResultData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void markedSale(int postID, int userID, final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> profile = postService.markedSale(postID, userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void delete(int postID, int userID, final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> profile = postService.deletePost(postID, userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void renew(int postID, int userID, final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> profile = postService.renewPost(postID, userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getPostDetail(int postID, int userID, final HTTPCallback httpCallback) {
        Flowable<Response<PostDetailResponse>> profile = postService.getPostDetail(postID, userID);
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<PostDetailResponse>>() {
                    @Override
                    public void onNext(Response<PostDetailResponse> response) {
                        if (response.isSuccessful()) {
                            PostDetailResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getPostDetailResult());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void updatePost(int postID,
                           int userID,
                           int brandId,
                           int typeId,
                           String generation,
                           String productType,
                           int price,
                           String size,
                           String shirts_size,
                           String pants_1,
                           String pants_2,
                           String shoes_size_eu,
                           String shoes_size_uk,
                           String shoes_size_us,
                           String description,
                           int provinceId,
                           int districtId,
                           String phone,
                           final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> profile = postService.updatePost(
                postID,
                userID,
                brandId,
                typeId,
                generation,
                productType,
                price,
                size,
                shirts_size,
                pants_1,
                pants_2,
                shoes_size_eu,
                shoes_size_uk,
                shoes_size_us,
                description,
                provinceId,
                districtId,
                phone
        );
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void searchPost(int pageIndex,
                           int brandId,
                           int typeId,
                           int provinceId,
                           int districtId,
                           int minPrice,
                           int maxPrice,
                           final HTTPCallback httpCallback) {
        Flowable<Response<SearchPostListResponse>> profile = postService.searchPost(
                pageIndex,
                brandId,
                typeId,
                provinceId,
                districtId,
                minPrice,
                maxPrice
        );
        profile.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<SearchPostListResponse>>() {
                    @Override
                    public void onNext(Response<SearchPostListResponse> response) {
                        if (response.isSuccessful()) {
                            SearchPostListResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp);
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void addFavorite(int userID,
                            int postID,
                            final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> favorite = postService.addFavorite(userID, postID);
        favorite.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getFavorite(int userID,
                            final HTTPCallback httpCallback) {
        Flowable<Response<FavoritePostResponse>> favorite = postService.getFavoritePost(userID);
        favorite.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<FavoritePostResponse>>() {
                    @Override
                    public void onNext(Response<FavoritePostResponse> response) {
                        if (response.isSuccessful()) {
                            FavoritePostResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getFavoritePostData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void getHistory(int userID,
                           final HTTPCallback httpCallback) {
        Flowable<Response<HistoryPostResponse>> favorite = postService.getHistory(userID);
        favorite.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<HistoryPostResponse>>() {
                    @Override
                    public void onNext(Response<HistoryPostResponse> response) {
                        if (response.isSuccessful()) {
                            HistoryPostResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getHistoryPostData());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void removeFavorite(int postID,
                               final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> favorite = postService.removeFavorite(postID);
        favorite.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void deletePostImage(int postID,
                                  String imageName,
                                  final HTTPCallback httpCallback) {
        Flowable<Response<BaseResponse>> favorite = postService.deletePostImage(postID, imageName);
        favorite.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void uploadPostImage(int postID,
                                List<Image> images,
                                final HTTPCallback httpCallback) {

        MultipartBody.Part[] parts = new MultipartBody.Part[images.size()];
        for (int i = 0; i < parts.length; i++) {
            try {
                ImageResize resize = new ImageResize();
                String resized = resize.resize(images.get(i).getPath());
                File file = new File(resized);
                RequestBody requestFile = RequestBody.create(MediaType.parse("image/*"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("image[]", file.getName(), requestFile);
                parts[i] = body;
            } catch (Exception e) {
                httpCallback.OnError(e.getMessage());
            }
        }

        Flowable<Response<BaseResponse>> favorite = postService.uploadPostImage(postID, parts);
        favorite.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new DefaultSubscriber<Response<BaseResponse>>() {
                    @Override
                    public void onNext(Response<BaseResponse> response) {
                        if (response.isSuccessful()) {
                            BaseResponse resp = response.body();
                            if (resp.isSuccessStatus()) {
                                httpCallback.OnSuccess(resp.getMessage());
                            } else {
                                httpCallback.OnError(resp.getMessage());
                            }
                        } else {
                            httpCallback.OnError(Util.getErrorMessage(response.errorBody()));
                        }
                    }

                    @Override
                    public void onError(Throwable t) {
                        httpCallback.OnError(t.getMessage());
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
