package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.esafirm.imagepicker.model.Image;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.ImageItemViewHolder;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerPhotoGridItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class ImageRecyclerViewAdapter extends RecyclerView.Adapter<ImageItemViewHolder> {

    private List<Object> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener onCardClickListener;

    public ImageRecyclerViewAdapter(List<Object> items) {
        this.items = items;
    }

    @Override
    public ImageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerPhotoGridItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_photo_grid_item, parent, false);
        return new ImageItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ImageItemViewHolder holder, final int position) {
        Object item = items.get(position);
        if (position == 0) {
            holder.displayAddImage();
            holder.setOnClickListener(onCardClickListener);
        } else if(item instanceof Image) {
            holder.displayImage((Image)item);
            if (onCardClickListener != null) {
                holder.setOnLongClickListener(onCardClickListener, position);
            }
        } else if(item instanceof Drawable) {
            holder.displayEmptyImage((Drawable)item);
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnLongClickListener(IRecyclerViewItemClickListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

}