package thailand.market.denim.denim.ListViewItem;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.PostItemResultData;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.RecyclerBrandItemBinding;
import thailand.market.denim.denim.databinding.RecyclerPostItemBinding;

/**
 * Created by com_s on 05-Feb-17.
 */

public class PostItemViewHolder extends RecyclerView.ViewHolder {
    public RecyclerPostItemBinding binding;

    public PostItemViewHolder(RecyclerPostItemBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void displayImage(String imageUrl) {
        Util.loadImage(binding.getRoot().getContext(), imageUrl, binding.imageView, binding.progressBar);
    }

    public void setPinPostNumber(int number) {
        binding.tvPostponedNumber.setText(String.valueOf(number));
    }

    public void setSellClickListener(final IRecyclerViewItemClickListener listener,final PostItemResultData data){
        binding.layoutSell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnItemClick(data);
            }
        });
    }
    public void setEditClickListener(final IRecyclerViewItemClickListener listener,final PostItemResultData data){
        binding.layoutEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnItemClick(data);
            }
        });
    }
    public void setDeleteClickListener(final IRecyclerViewItemClickListener listener,final PostItemResultData data){
        binding.layoutDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnItemClick(data);
            }
        });
    }
    public void setRenewClickListener(final IRecyclerViewItemClickListener listener,final PostItemResultData data){
        binding.layoutPinPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnItemClick(data);
            }
        });
    }
}
