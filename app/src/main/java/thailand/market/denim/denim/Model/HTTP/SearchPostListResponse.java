package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Phitakphong on 17/7/2560.
 */

public class SearchPostListResponse extends BaseResponse {

    @SerializedName("data")
    private SearchPostData postData;

    @SerializedName("prev_status")
    private boolean prev_status;

    @SerializedName("next_status")
    private boolean next_status;

    @SerializedName("prev_page")
    private int prev_page;

    @SerializedName("curent_page")
    private int curent_page;

    @SerializedName("next_page")
    private int next_page;

    public SearchPostData getSearchPostData() {
        return  postData;
    }

    public boolean isPrevStatus() {
        return prev_status;
    }

    public boolean isNextStatus() {
        return next_status;
    }

    public int getPrevPage() {
        return prev_page;
    }

    public int getCurentPage() {
        return curent_page;
    }

    public int getNextPage() {
        return next_page;
    }
}
