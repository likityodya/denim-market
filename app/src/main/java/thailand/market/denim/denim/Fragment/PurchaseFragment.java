package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Adaptor.PurchaseRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Interface.IFragmentControlListener;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.MyOrderListData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentPurchaseBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class PurchaseFragment extends BaseFragment {


    private FragmentPurchaseBinding binding;
    private PurchaseListFragment fragment;
    private MemberRepository memberRepository;

    public PurchaseFragment() {
        super();
    }

    public static PurchaseFragment newInstance() {
        PurchaseFragment fragment = new PurchaseFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = FragmentPurchaseBinding.inflate(inflater);

        View rootView = binding.getRoot();
        super.init(rootView,inflater);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        memberRepository = new MemberRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {

        fragment = (PurchaseListFragment) getParentFragment();
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        loadData();

    }

    private void loadData() {
        showProgress();
        memberRepository.getMyOrder(PreferenceManager.getInstance().getUserID(), myOrderCallback);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    private HTTPCallback myOrderCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            List<MyOrderListData> data = (List<MyOrderListData>) response;
            if (data != null) {
                PurchaseRecyclerViewAdapter adapter = new PurchaseRecyclerViewAdapter(data);
                adapter.setItemClickListener(new IRecyclerViewItemClickListener() {
                    @Override
                    public void OnItemClick(Object item) {
                        MyOrderListData d = (MyOrderListData) item;
                        fragment.goNext(ViewBuyItemFragment.newInstance(d.getId()));
                    }

                    @Override
                    public void OnItemLongClick(View view, Object item) {

                        final MyOrderListData d = (MyOrderListData) item;

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext())
                                .setMessage("ลบรายการสั่งซื้อ")
                                .setCancelable(true)
                                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        delete(d);
                                    }
                                });
                        alertDialog.show();
                    }
                });
                binding.recyclerView.setAdapter(adapter);
            }
            hideProgress();
        }

        @Override
        public void OnError(String message) {
            hideProgress();
            Util.showToast(message);
        }
    };

    private void delete(MyOrderListData data) {
        showProgressDialog(getString(R.string.loading));
        memberRepository.deleteOrder(data.getId(), PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                loadData();
                Util.showToast(response.toString());
                hideProgressDialog();
            }

            @Override
            public void OnError(String message) {
                hideProgressDialog();
                Util.showToast(message);
            }
        });
    }
}
