package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.BrandItemViewHolder;
import thailand.market.denim.denim.ListViewItem.ProductItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.ProductBrandResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerBrandItemBinding;
import thailand.market.denim.denim.databinding.RecyclerProductItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductItemViewHolder> {

    private IRecyclerViewItemClickListener clickListener;
    private List<ProductBrandResultData> items;
    private LayoutInflater inflater;

    public ProductRecyclerViewAdapter(List<ProductBrandResultData> items) {
        this.items = items;
    }

    @Override
    public ProductItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerProductItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_product_item, parent, false);
        return new ProductItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ProductItemViewHolder holder, final int position) {
        final ProductBrandResultData item = items.get(position);
        holder.displayImage(item.getImage());
        holder.setPrice(item.getPrice());
        holder.setGeneration(item.getGeneration());
        holder.setDescription(item.getDescription());
        if (this.clickListener != null) {
            holder.setOnClickListener(this.clickListener, item);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setOnClickListener(IRecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }
}