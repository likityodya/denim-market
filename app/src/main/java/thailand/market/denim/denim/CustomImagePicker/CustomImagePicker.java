//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package thailand.market.denim.denim.CustomImagePicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.esafirm.imagepicker.features.ImagePickerConfig;
import com.esafirm.imagepicker.model.Image;

import java.util.ArrayList;
import java.util.List;

public abstract class CustomImagePicker {
    public static final String EXTRA_SELECTED_IMAGES = "selectedImages";
    public static final String EXTRA_LIMIT = "limit";
    public static final String EXTRA_SHOW_CAMERA = "showCamera";
    public static final String EXTRA_MODE = "mode";
    public static final String EXTRA_FOLDER_MODE = "folderMode";
    public static final String EXTRA_FOLDER_TITLE = "folderTitle";
    public static final String EXTRA_IMAGE_TITLE = "imageTitle";
    public static final String EXTRA_IMAGE_DIRECTORY = "imageDirectory";
    public static final String EXTRA_RETURN_AFTER_FIRST = "returnAfterFirst";
    public static final int MAX_LIMIT = 99;
    public static final int MODE_SINGLE = 1;
    public static final int MODE_MULTIPLE = 2;
    private ImagePickerConfig config;

    public CustomImagePicker() {
    }

    public abstract void start(int var1);

    public void init(Context context) {
        this.config = new ImagePickerConfig(context);
    }

    public static CustomImagePicker.ImagePickerWithActivity create(Activity activity) {
        return new CustomImagePicker.ImagePickerWithActivity(activity);
    }

    public static CustomImagePicker.ImagePickerWithFragment create(Fragment fragment) {
        return new CustomImagePicker.ImagePickerWithFragment(fragment);
    }

    public CustomImagePicker single() {
        this.config.setMode(1);
        return this;
    }

    public CustomImagePicker multi() {
        this.config.setMode(2);
        return this;
    }

    public CustomImagePicker returnAfterFirst(boolean returnAfterFirst) {
        this.config.setReturnAfterFirst(returnAfterFirst);
        return this;
    }

    public CustomImagePicker limit(int count) {
        this.config.setLimit(count);
        return this;
    }

    public CustomImagePicker showCamera(boolean show) {
        this.config.setShowCamera(show);
        return this;
    }

    public CustomImagePicker folderTitle(String title) {
        this.config.setFolderTitle(title);
        return this;
    }

    public CustomImagePicker imageTitle(String title) {
        this.config.setImageTitle(title);
        return this;
    }

    public CustomImagePicker origin(ArrayList<Image> images) {
        this.config.setSelectedImages(images);
        return this;
    }

    public CustomImagePicker folderMode(boolean folderMode) {
        this.config.setFolderMode(folderMode);
        return this;
    }

    public CustomImagePicker imageDirectory(String directory) {
        this.config.setImageDirectory(directory);
        return this;
    }

    public Intent getIntent(Context context) {
        Intent intent = new Intent(context, CustomImagePickerActivity.class);
        intent.putExtra(ImagePickerConfig.class.getSimpleName(), this.config);
        return intent;
    }

    public static List<Image> getImages(Intent intent) {
        if (intent == null) {
            return null;
        } else {
            return intent.getParcelableArrayListExtra("selectedImages");
        }
    }

    public static class ImagePickerWithFragment extends CustomImagePicker {
        private Fragment fragment;

        public ImagePickerWithFragment(Fragment fragment) {
            this.fragment = fragment;
            this.init(fragment.getActivity());
        }

        public void start(int requestCode) {
            Intent intent = this.getIntent(this.fragment.getActivity());
            this.fragment.startActivityForResult(intent, requestCode);
        }
    }

    public static class ImagePickerWithActivity extends CustomImagePicker {
        private Activity activity;

        public ImagePickerWithActivity(Activity activity) {
            this.activity = activity;
            this.init(activity);
        }

        public void start(int requestCode) {
            Intent intent = this.getIntent(this.activity);
            this.activity.startActivityForResult(intent, requestCode);
        }
    }
}
