package thailand.market.denim.denim.CustomView;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inthecheesefactory.thecheeselibrary.view.BaseCustomViewGroup;

import thailand.market.denim.denim.R;

/**
 * Created by Phitakphong on 16/6/2560.
 */

public class CustomSelector extends BaseCustomViewGroup {

    private RelativeLayout selectorLayout;
    private TextView textViewValue;
    private TextAwesome textAwesome;
    private String textValue;

    public CustomSelector(Context context) {
        super(context);
    }

    public CustomSelector(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        initWithAttrs(attrs, 0, 0);
    }

    public CustomSelector(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
        initWithAttrs(attrs, defStyleAttr, 0);
    }

    @TargetApi(21)
    public CustomSelector(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
        initWithAttrs(attrs, defStyleAttr, defStyleRes);
    }

    private void init() {
        inflate(getContext(), R.layout.layout_custom_selector, this);
        textViewValue = (TextView) findViewById(R.id.tvTextValue);
        selectorLayout = (RelativeLayout) findViewById(R.id.selector);
        textAwesome = (TextAwesome) findViewById(R.id.tvTextAwesome);
        textValue = "";
    }

    private void initWithAttrs(AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        TypedArray typedArray = getContext().getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.CustomSelector,
                defStyleAttr,
                defStyleRes);

        try {
            String str = typedArray.getString(R.styleable.CustomSelector_text_value);
            setTextValue(str);
        } finally {
            typedArray.recycle();
        }
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
        textViewValue.setText(this.textValue);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        selectorLayout.setOnClickListener(l);
    }

    public void setError(String error) {
        textAwesome.setError(error);
        if (error != null) {
            textAwesome.setSelected(true);
        }
    }
}
