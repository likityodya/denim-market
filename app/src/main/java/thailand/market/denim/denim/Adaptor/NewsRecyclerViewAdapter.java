package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.ListViewItem.BrandItemViewHolder;
import thailand.market.denim.denim.ListViewItem.NewsItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.BrandResultData;
import thailand.market.denim.denim.Model.HTTP.NewsResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerBrandItemBinding;
import thailand.market.denim.denim.databinding.RecyclerNewsItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsItemViewHolder> {

    private List<NewsResultData> items;
    private LayoutInflater inflater;

    public NewsRecyclerViewAdapter(List<NewsResultData> items) {
        this.items = items;
    }

    @Override
    public NewsItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerNewsItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_news_item, parent, false);
        return new NewsItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(NewsItemViewHolder holder, final int position) {
        final NewsResultData item = items.get(position);
        holder.setTitle(item.getName());
        holder.setTitle(item.getDescription());
        holder.displayImage(item.getImage());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}