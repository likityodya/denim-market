package thailand.market.denim.denim.Model.HTTP;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 13/7/2560.
 */

public class BrandResultData implements Parcelable {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("image")
    private String image;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public BrandResultData() {
    }

    protected BrandResultData(Parcel in) {
        id = in.readInt();
        name = in.readString();
        image = in.readString();
    }

    public static final Parcelable.Creator<BrandResultData> CREATOR = new Parcelable.Creator<BrandResultData>() {
        @Override
        public BrandResultData createFromParcel(Parcel in) {
            return new BrandResultData(in);
        }

        @Override
        public BrandResultData[] newArray(int size) {
            return new BrandResultData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(image);
    }
}
