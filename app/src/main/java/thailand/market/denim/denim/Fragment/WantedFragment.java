package thailand.market.denim.denim.Fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Adaptor.LookForRecyclerViewAdapter;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.HTTP.LookForData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentWantedBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class WantedFragment extends BaseFragment {


    private FragmentWantedBinding binding;
    private MemberRepository memberRepository;

    public WantedFragment() {
        super();
    }

    public static WantedFragment newInstance() {
        WantedFragment fragment = new WantedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentWantedBinding.inflate(inflater);
        View rootView = binding.getRoot();
        super.init(rootView, inflater);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        memberRepository = new MemberRepository();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        loadData();
        initEvents();
    }

    private void loadData() {

        showProgress();
        memberRepository.myLookFor(PreferenceManager.getInstance().getUserID(), new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                List<LookForData> dataList = (List<LookForData>) response;
                if (dataList != null) {
                    LookForRecyclerViewAdapter adapter = new LookForRecyclerViewAdapter(dataList);
                    adapter.setItemClickListener(new IRecyclerViewItemClickListener() {
                        @Override
                        public void OnItemClick(Object item) {

                        }

                        @Override
                        public void OnItemLongClick(View view, Object item) {
                            LookForData data = (LookForData) item;
                            delete(data);
                        }
                    });
                    binding.recyclerView.setAdapter(adapter);
                }
                hideProgress();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
                hideProgress();
            }
        });
    }

    private void delete(final LookForData data) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext())
                .setMessage("ลบรายการฝากหา")
                .setCancelable(true)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        showProgressDialog(getString(R.string.loading));
                        memberRepository.deleteLookFor(
                                data.getId()
                                , PreferenceManager.getInstance().getUserID()
                                , new HTTPCallback() {
                                    @Override
                                    public void OnSuccess(Object response) {
                                        loadData();
                                        Util.showToast(response.toString());
                                        hideProgressDialog();
                                    }

                                    @Override
                                    public void OnError(String message) {
                                        Util.showToast(message);
                                        hideProgressDialog();
                                    }
                                }
                        );
                    }
                });
        alertDialog.show();

    }

    private void initEvents() {
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PurchaseListFragment parentFragment = (PurchaseListFragment) getParentFragment();
                parentFragment.goNext(WantedRequestFragment.newInstance());
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }


}
