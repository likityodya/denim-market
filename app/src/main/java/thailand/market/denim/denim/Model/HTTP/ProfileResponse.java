package thailand.market.denim.denim.Model.HTTP;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Phitakphong on 23/5/2560.
 */

public class ProfileResponse extends BaseResponse {

    @SerializedName("data")
    private ProfileResultData profileResultData;

    public ProfileResultData getProfileResultData() {
        return profileResultData;
    }
}
