package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import thailand.market.denim.denim.Manager.PreferenceManager;
import thailand.market.denim.denim.Model.DTO.ForgotPasswordDto;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Repository.MemberRepository;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentChangePasswordBinding;

/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ChangePasswordFragment extends BaseFragment {

    private FragmentChangePasswordBinding binding;
    private WrapperFragment wrapperFragment;

    public ChangePasswordFragment() {
        super();
    }

    public static ChangePasswordFragment newInstance() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentChangePasswordBinding.inflate(inflater);
        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {

    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
        initEvents();
    }

    private void initEvents() {
        binding.vBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wrapperFragment.onGoBackFragment();
            }
        });
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {
                    changePassword();

                }
            }
        });
    }

    private void changePassword() {

        int userID = PreferenceManager.getInstance().getUserID();
        ForgotPasswordDto forgotPasswordDto = new ForgotPasswordDto();
        forgotPasswordDto.setUserID(userID);
        forgotPasswordDto.setPassword(binding.etPassword.getText().toString());
        forgotPasswordDto.setNewPassword(binding.etNewPassword.getText().toString());
        forgotPasswordDto.setConfirmPassword(binding.etConfirmPassword.getText().toString());

        MemberRepository memberRepository = new MemberRepository();
        memberRepository.changePassword(forgotPasswordDto, new HTTPCallback() {
            @Override
            public void OnSuccess(Object response) {
                Util.showToast(response.toString());
                wrapperFragment.onGoBackFragment();
            }

            @Override
            public void OnError(String message) {
                Util.showToast(message);
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    private boolean validate() {
        boolean valid = true;
        if (binding.etPassword.getText().toString().trim().length() == 0) {
            binding.etPassword.setError(getString(R.string.password));
            valid = false;
        } else {
            binding.etPassword.setError(null);
        }
        if (binding.etNewPassword.getText().toString().trim().length() == 0) {
            binding.etNewPassword.setError(getString(R.string.new_password));
            valid = false;
        } else {
            binding.etNewPassword.setError(null);
        }
        if (binding.etConfirmPassword.getText().toString().trim().length() == 0) {
            binding.etConfirmPassword.setError(getString(R.string.confirm_pass));
            valid = false;
        } else {
            binding.etConfirmPassword.setError(null);
        }
        if (valid) {
            valid = binding.etNewPassword.getText().toString().equals(binding.etConfirmPassword.getText().toString());
            if (!valid) {
                binding.etNewPassword.setError(getString(R.string.password_not_match));
                binding.etConfirmPassword.setError(getString(R.string.password_not_match));
            } else {
                binding.etNewPassword.setError(null);
                binding.etConfirmPassword.setError(null);
            }
        }

        return valid;
    }

}
