package thailand.market.denim.denim.Adaptor;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import thailand.market.denim.denim.Interface.IRecyclerViewItemClickListener;
import thailand.market.denim.denim.ListViewItem.MyLookForItemViewHolder;
import thailand.market.denim.denim.ListViewItem.PurchaseItemViewHolder;
import thailand.market.denim.denim.Model.HTTP.LookForData;
import thailand.market.denim.denim.Model.HTTP.MyOrderListData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.RecyclerLookForItemBinding;
import thailand.market.denim.denim.databinding.RecyclerPurchaseItemBinding;

/**
 * Created by phitakphong on 17/05/2559.
 */
public class LookForRecyclerViewAdapter extends RecyclerView.Adapter<MyLookForItemViewHolder> {

    private List<LookForData> items;
    private LayoutInflater inflater;
    private IRecyclerViewItemClickListener itemClickListener;

    public LookForRecyclerViewAdapter(List<LookForData> items) {
        this.items = items;
    }


    @Override
    public MyLookForItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (inflater == null) {
            inflater = LayoutInflater.from(parent.getContext());
        }
        RecyclerLookForItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.recycler_look_for_item, parent, false);
        return new MyLookForItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyLookForItemViewHolder holder, final int position) {
        final LookForData data = items.get(position);
        holder.setLine1(data.getLine1());
        holder.setLine2(data.getLine2());
        holder.setLine3(data.getLine3());
        holder.getLayoutItem().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                itemClickListener.OnItemLongClick(v, data);
                return true;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setItemClickListener(IRecyclerViewItemClickListener onClickListener) {
        this.itemClickListener = onClickListener;
    }
}