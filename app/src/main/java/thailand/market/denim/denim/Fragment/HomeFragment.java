package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.synnapps.carouselview.ImageListener;

import thailand.market.denim.denim.Interface.IFragmentControlListener;
import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.Model.HTTP.HomeResultData;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.Repository.CommonRepository;
import thailand.market.denim.denim.Interface.HTTPCallback;
import thailand.market.denim.denim.Util.Util;
import thailand.market.denim.denim.databinding.FragmentHomeBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class HomeFragment extends BaseFragment {

    private FragmentHomeBinding binding;
    private WrapperFragment wrapperFragment;
    private String[] images;

    public HomeFragment() {
        super();
    }

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater);
        View rootView = binding.getRoot();
        super.init(rootView, inflater);
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        wrapperFragment = (WrapperFragment) getParentFragment();
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {
        binding.layoutBuy.setOnClickListener(onBuyClickListener);
        binding.layoutSell.setOnClickListener(onSellClickListener);
        binding.layoutSearch.setOnClickListener(onSearchClickListener);
        binding.layoutLookFor.setOnClickListener(onLookForClickListener);

        loadImageSlider();
        loadHome();
    }

    private void loadHome() {
        showProgressDialog(getString(R.string.loading));
        CommonRepository commonRepository = new CommonRepository();
        commonRepository.home(homeCallback);
    }

    private void loadImageSlider() {

        binding.layoutLookFor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IFragmentControlListener controlListener = (IFragmentControlListener)getParentFragment();
                controlListener.onGoNextFragment(WantedRequestFragment.newInstance());
            }
        });
    }

    private ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            Util.loadImage(getContext(), images[position], imageView);
        }
    };


    private HTTPCallback homeCallback = new HTTPCallback() {
        @Override
        public void OnSuccess(Object response) {
            HomeResultData homeResultData = (HomeResultData) response;
            binding.tvNew.setText(String.valueOf(homeResultData.getPostNewTotal()));
            binding.tvTotal.setText(String.valueOf(homeResultData.getPostAllTotal()));
            binding.tvOnline.setText(String.valueOf(homeResultData.getUserOnline()));
            binding.tvOneDay.setText(String.valueOf(homeResultData.getUserLoginTodayTotal()));
            binding.tvTotalMember.setText(String.valueOf(homeResultData.getUserTotal()));
            images = homeResultData.getSlider();

            binding.carouselView.setImageListener(imageListener);
            binding.carouselView.setPageCount(homeResultData.getSlider().length);

            hideProgressDialog();
        }

        @Override
        public void OnError(String message) {
            hideProgressDialog();
            Util.showToast(message);
        }
    };

    private View.OnClickListener onBuyClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            IFragmentControlListener controlListener = (IFragmentControlListener)getParentFragment();
            controlListener.onGoNextFragment(NeedBuyListFragment.newInstance());
        }
    };

    private View.OnClickListener onSellClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isLogedIn()) {
                wrapperFragment.onGoNextFragment(UploadImageFragment.newInstance());
            }else {
                IFragmentListener loginChecker = (IFragmentListener) getActivity();
                loginChecker.onLoginRequire();
            }
        }
    };
    private View.OnClickListener onLookForClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };
    private View.OnClickListener onSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }


}
