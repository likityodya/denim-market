package thailand.market.denim.denim.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import thailand.market.denim.denim.Interface.IFragmentListener;
import thailand.market.denim.denim.R;
import thailand.market.denim.denim.databinding.FragmentProfileBinding;
import thailand.market.denim.denim.databinding.FragmentPurchaseListBinding;


/**
 * Created by nuuneoi on 11/16/2014.
 */
public class ProfileFragment extends BaseFragment {

    private FragmentProfileBinding binding;
    private WrapperFragment wrapperFragment;

    public ProfileFragment() {
        super();
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);

        if (savedInstanceState != null)
            onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentProfileBinding.inflate(inflater);

        View rootView = binding.getRoot();
        initInstances(rootView, savedInstanceState);
        return rootView;
    }

    @SuppressWarnings("UnusedParameters")
    private void init(Bundle savedInstanceState) {
        if (!this.isLogedIn()) {

        }
    }

    @SuppressWarnings("UnusedParameters")
    private void initInstances(View rootView, Bundle savedInstanceState) {

        wrapperFragment = (WrapperFragment)getParentFragment();

        binding.itemProfile.setOnClickListener(onMenuItemClick);
        binding.itemRegisterMember.setOnClickListener(onMenuItemClick);
        binding.itemBuyAndSellList.setOnClickListener(onMenuItemClick);
        binding.itemBookmarkList.setOnClickListener(onMenuItemClick);
        binding.itemContactStaff.setOnClickListener(onMenuItemClick);
        binding.itemResetPassword.setOnClickListener(onMenuItemClick);
        binding.itemTerm.setOnClickListener(onMenuItemClick);
        binding.itemLogout.setOnClickListener(onMenuItemClick);


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        // Save Instance (Fragment level's variables) State here
    }

    @SuppressWarnings("UnusedParameters")
    private void onRestoreInstanceState(Bundle savedInstanceState) {
        // Restore Instance (Fragment level's variables) State here
    }

    private View.OnClickListener onMenuItemClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            IFragmentListener fragmentListener = (IFragmentListener) getActivity();
            switch (v.getId()) {
                case R.id.itemProfile:

                    wrapperFragment.onGoNextFragment(ViewProfileFragment.newInstance());

                    break;
                case R.id.itemRegisterMember:

                    wrapperFragment.onGoNextFragment(RegisterMemberFragment.newInstance());

                    break;
                case R.id.itemBuyAndSellList:

                    wrapperFragment.onGoNextFragment(PurchaseListFragment.newInstance());

                    break;
                case R.id.itemBookmarkList:

                    wrapperFragment.onGoNextFragment(FavoriteListFragment.newInstance());

                    break;
                case R.id.itemTerm:

                    fragmentListener.openTerm();

                    break;
                case R.id.itemContactStaff:

                    fragmentListener.openContactStaff();

                    break;
                case R.id.itemResetPassword:

                    wrapperFragment.onGoNextFragment(ChangePasswordFragment.newInstance());

                    break;
                case R.id.itemLogout:

                    fragmentListener.onLogout();

                    break;

            }
        }
    };

}
