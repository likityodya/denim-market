package thailand.market.denim.denim.Interface;

import android.content.Intent;

import thailand.market.denim.denim.Activity.EditSellItemActivity;

/**
 * Created by Phitakphong on 5/6/2560.
 */

public interface IFragmentListener {
    void onLoginRequire();

    void onLogout();

    void onStartEditSellItem(int postID);

    void openSelectImage(int requestCode);

    void openAdvanceSearchPost();

    void openPostItem(int postID);

    void openTerm();

    void openContactStaff();
}